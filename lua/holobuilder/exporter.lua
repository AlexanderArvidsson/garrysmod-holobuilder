HoloBuilder.Exporter = HoloBuilder.Exporter or {
    formats = {}
}


function HoloBuilder.Exporter:AddFormat(name, format)
    self.formats[name] = format
end

function HoloBuilder.Exporter:GetFormats()
    return self.formats
end

function HoloBuilder.Exporter:GetFormat(name)
    return self.formats[name]
end



local files, _ = file.Find("holobuilder/export_formats/*", "LUA")

for k, fName in ipairs(files) do
    include("holobuilder/export_formats/" .. fName)
end