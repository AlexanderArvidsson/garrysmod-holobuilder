HoloBuilder.SpawnMenu = HoloBuilder.SpawnMenu or {}


function HoloBuilder.SpawnMenu:Integrate()
    if not IsValid(g_SpawnMenu) then return end

    local world = vgui.GetWorldPanel()

    local function GetTab(pnl, name)
        for k, v in pairs(pnl.Items) do
            if v.Name == name then
                return v
            end
        end
    end

    local spawnMenu = g_SpawnMenu
    local creationMenu = spawnMenu.CreateMenu

    local contentTab = GetTab(creationMenu, "#spawnmenu.content_tab")
    local contentPanel = contentTab.Panel:GetChildren()[1]

    if not IsValid(contentPanel) then return end

    contentPanel.oldSwitchPanel = contentPanel.oldSwitchPanel or contentPanel.SwitchPanel

    contentPanel.SwitchPanel = function(pnl, panel)
        contentPanel:oldSwitchPanel(panel)
        
        self:IntegrateContentPanel(contentPanel.SelectedPanel)
    end

    self:IntegrateContentPanel(contentPanel.SelectedPanel)
end

function HoloBuilder.SpawnMenu:IntegrateContentPanel(panel)
    local world = vgui.GetWorldPanel()
    local spawnIcons = panel.IconList:GetChildren()

    for k, v in pairs(spawnIcons) do
        v.oldOpenMenu = v.oldOpenMenu or v.OpenMenu
        v.OpenMenu = function(icon)
            v:oldOpenMenu()
            
            for k, v in pairs(world:GetChildren()) do
                if IsValid(v) and v:GetName() == "DMenu" then
                    local x, y = v:GetPos()
                    
                    if x == gui.MouseX() and y == gui.MouseY() then
                        hook.Run("HoloBuilderSpawnMenuIconMenu", v, icon)
                    end
                end
            end
            
        end
    end
end