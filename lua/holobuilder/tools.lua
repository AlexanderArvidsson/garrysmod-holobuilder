HoloBuilder.Tools = HoloBuilder.Tools or {
    table = {},
    meta = {}
}
HoloBuilder.Tools.meta.__index = HoloBuilder.Tools.meta

local toolMeta = HoloBuilder.Tools.meta

function HoloBuilder.Tools.New(name)
    if not name then return end
    local tool = setmetatable({}, toolMeta)

    tool.id       = 0
    tool.position = 0
    tool.name     = name

    return tool
end


function toolMeta:Copy()
    local newTbl = table.Copy(self)

    return newTbl
end

function toolMeta:SetName(name) self.name = name end
function toolMeta:SetPanel(pnl) self.panel = pnl end

function toolMeta:GetName() return self.name end
function toolMeta:GetPanel() return self.panel end

function toolMeta:CanSelectParts() return true end

function toolMeta:OnMousePressed(mouseCode, mPos) end
function toolMeta:OnMouseDragged(mouseCode, mPos, xDelta, yDelta) end
function toolMeta:OnMouseReleased(mouseCode, mPos) end

function toolMeta:Think() end

function toolMeta:GetSelectedPart()
    local part = HoloBuilder:GetSelectedPart()

    if part and not part:CanToolManipulate(self:GetName()) then return end

    return part
end

function toolMeta:IsHovered() return false end

function toolMeta:Draw(clientModel)
end

function toolMeta:Draw2D()
end