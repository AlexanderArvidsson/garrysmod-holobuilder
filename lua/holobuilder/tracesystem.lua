
HoloBuilder.TraceSystem = HoloBuilder.TraceSystem or {}
local v = debug.getregistry().Vector
local Dot = v.Dot
local Distance = v.Distance


function HoloBuilder.TraceSystem.RayPlaneIntersection( Start, Dir, Pos, Normal )
    local A = Dot(Normal, Dir)
    
    --Check if the ray is aiming towards the plane (fail if it origin behind the plane, but that is checked later)
    if A < 0 then
        
        local B = Dot(Normal, Pos-Start)
        
        --Check if the ray origin in front of plane
        if B < 0 then
            return (Start + Dir * (B/A))
        end
        
    --Check if the ray is parallel to the plane
    elseif A == 0 then
        
        --Check if the ray origin inside the plane
        if Dot(Normal, Pos-Start) == 0 then
            return Start
        end
        
    end
    
    return false
end

function HoloBuilder.TraceSystem.RayFaceIntersection(Start, Dir, Pos, Normal, Size, Rotation)
    local hitPos = HoloBuilder.TraceSystem.RayPlaneIntersection(Start, Dir, Pos, Normal)
    
    if hitPos then
        
        faceAngle = Normal:Angle() + Angle(0, 0, Rotation)
        
        local localHitPos = WorldToLocal(hitPos, Angle(0, 0, 0), Pos, faceAngle)
        
        local min = Size/-2
        local max = Size/2
        
        --Because using Normal:Angle() for WorldToLocal() makes it think that axis is x, we need to use the local hitpos z as x.
        if localHitPos.z >= min.x and localHitPos.z <= max.x then
            if localHitPos.y >= min.y and localHitPos.y <= max.y then
                
                return hitPos
                
            end
        end
        
    end
    
    return false
    
end


local boxNormals = {
    Vector( 1,  0,  0),
    Vector( 0,  1,  0),
    Vector( 0,  0,  1),
    Vector(-1,  0,  0),
    Vector( 0, -1,  0),
    Vector( 0,  0, -1)
}

function HoloBuilder.TraceSystem.RayAABBoxIntersection( Start, Dir, Pos, Size )
    --Getting the sizes for the faces let us scale the box a lil.
    local boxSizes = {
        Vector(Size.z, Size.y, 0),
        Vector(Size.z, Size.x, 0),
        Vector(Size.x, Size.y, 0),
        Vector(Size.z, Size.y, 0),
        Vector(Size.z, Size.x, 0),
        Vector(Size.x, Size.y, 0)
    }
    
    local closestDist
    local closestHitPos
    local closestNormal
    
    for i = 1, 6 do
        local normal = boxNormals[i]
        local faceSize = boxSizes[i]
        
        if Dot(normal, Dir) < 0 then
            
            local planePos = Pos + normal * (Size / 2)
            local HitPos = HoloBuilder.TraceSystem.RayFaceIntersection(Start, Dir, planePos, normal, faceSize, 0)
            
            if HitPos then
                local dist = Distance(HitPos, Start) 
                if not closestDist or dist < closestDist then
                    closestDist = dist
                    closestHitPos = HitPos
                    closestNormal = normal
                end
            end
            
        end
        
    end
    
    if closestHitPos then return closestHitPos, closestNormal end
    
    return false
end

function HoloBuilder.TraceSystem.RayOBBoxIntersection(Start, Dir, Pos, Size, Ang)
    --To use an oriented-bounding-box we make the ray local (so we can use the AABB code)
    local localRayStart = WorldToLocal(Start, Angle(0,0,0), Pos, Ang)
    
    --The direction need to be local to 0,0,0 though
    local localRayDir = WorldToLocal(Dir, Angle(0,0,0), Vector(0,0,0), Ang)
    
    --Use AABB code as that is easyer than calculating the normals of the faces and their angle aroudn that axis
    local localHitPos, localHitNormal = HoloBuilder.TraceSystem.RayAABBoxIntersection( localRayStart, localRayDir, Vector(), Size)
    
    --But we want the returned hitpos to be a world coord
    if localHitPos then
        local hitPos = LocalToWorld(localHitPos, Angle(0,0,0), Pos, Ang)
        local hitNormal = LocalToWorld(localHitNormal, Angle(0,0,0), Vector(0,0,0), Ang)
        
        return hitPos, hitNormal
    end
    
    return false
end

