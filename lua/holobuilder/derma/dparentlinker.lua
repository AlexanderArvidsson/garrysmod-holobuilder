local PANEL = {}

function PANEL:Init()
    self:SetParent(nil)

    HoloBuilder.Skin.PaintParentLinker(self)
end

function PANEL:SetParent(part)
    self.linking = false
    self.error = nil
    self.parent = part

    if not part then
        self:SetText("Parent")
        self:SetTextColor(Color(255, 255, 255))
    else
        self:SetText("Parent: " .. part:GetName())
        self:SetTextColor(Color(255, 150, 0))
    end
end

function PANEL:SetError(err)
    self.linking = false
    self.error = err
    self:SetText(err)
    self:SetTextColor(Color(255, 0, 0))
end

function PANEL:IsLinking()
    return self.linking
end

function PANEL:DoClick()
    if not HoloBuilder:GetSelectedPart() then return end

    self.linking = not self.linking
end

function PANEL:SelectPart(part)
    local selectedPart = HoloBuilder:GetSelectedPart()

    if self:IsLinking() and selectedPart then
        if part == selectedPart then
            self:SetError("Can't parent to itself!")
        elseif part and part:GetParent() == selectedPart then
            self:SetError("Can't crosslink!")
        elseif part and part.type == HB_PART_TYPE_CLIP and selectedPart.type == HB_PART_TYPE_NORMAL then
            self:SetError("Can't parent to clip plane!")
        else
            self:SetParent(part)
            selectedPart:SetParent(part)

            return 2
        end

        return 1
    end

    return 0
end

function PANEL:Think()
    if input.IsMouseDown(MOUSE_RIGHT) and self.linking then
        self.linking = false
    end
end

vgui.Register("DHoloBuilderParentLinker", PANEL, "DButton")