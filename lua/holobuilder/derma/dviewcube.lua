local PANEL = {}

local axes = {
    FORWARD = 1,
    BACK    = 2,
    RIGHT   = 3,
    LEFT    = 4,
    UP      = 5,
    DOWN    = 6
}

local matFaceTop    = Material("holobuilder/viewcube/face_top.png")
local matFaceBottom = Material("holobuilder/viewcube/face_bottom.png")
local matFaceLeft   = Material("holobuilder/viewcube/face_left.png")
local matFaceRight  = Material("holobuilder/viewcube/face_right.png")
local matFaceFront  = Material("holobuilder/viewcube/face_front.png")
local matFaceBack   = Material("holobuilder/viewcube/face_back.png")

function PANEL:Init()
    self.pos = Vector()
    self.angle = Angle()

    self.fov = 90

    self.min = -Vector(10, 10, 10)
    self.max = Vector(10, 10, 10)
    self.size = self.max - self.min

    self.dist = 30
end

function PANEL:OnMousePressedFace(mCode, axis)

end

function PANEL:OnMousePressed(mCode)
    if self.hoverAxis then
        self:OnMousePressedFace(mCode, self.hoverAxis, self.normal)
    end
end

function PANEL:Think()
    local mx, my = self:CursorPos()
    local dir = self:GetScreenDir(mx, my)

    local trace, norm = HoloBuilder.TraceSystem.RayOBBoxIntersection(self.pos, dir, Vector(), self.size, Angle())
    if trace then
        if norm ==      Vector(1, 0, 0) then self.hoverAxis = axes.FORWARD
        elseif norm == -Vector(1, 0, 0) then self.hoverAxis = axes.BACK
        elseif norm ==  Vector(0, 1, 0) then self.hoverAxis = axes.RIGHT
        elseif norm == -Vector(0, 1, 0) then self.hoverAxis = axes.LEFT
        elseif norm ==  Vector(0, 0, 1) then self.hoverAxis = axes.UP
        elseif norm == -Vector(0, 0, 1) then self.hoverAxis = axes.DOWN end

        self.normal = norm
    else
        self.hoverAxis = nil
        self.normal = nil
    end
end

function PANEL:GetScreenDir(x, y)
    local w = self:GetWide()
    local h = self:GetTall()

    --local ratio = w / h
    --local fovFix = (math.atan(math.tan((self.Camera.fov * math.pi) / 360) * (ratio / (4/3) )) * 360) / math.pi

    --This code works by basically treating the camera like a frustrum of a pyramid.
    --We slice this frustrum at a distance "d" from the camera, where the slice will be a rectangle whose width equals the "4:3" width corresponding to the given screen height.
    local d = (0.5 * w) / math.tan(math.rad(self.fov) * 0.5)
 
    --Forward, right, and up vectors (need these to convert from local to world coordinates
    local angCamRot = self.angle
    local vForward = angCamRot:Forward()
    local vRight   = angCamRot:Right()
    local vUp      = angCamRot:Up()
 
    --Then convert vec to proper world coordinates and return it 
    local v = (d * vForward + (x - 0.5 * w) * vRight + (0.5 * h - y) * vUp)
    v:Normalize()
    return v
end

function PANEL:Paint(w, h)
    local x, y = self:LocalToScreen()

    local min = self.min
    local max = self.max

    local color = Color(255, 255, 255)
    local normalColor = Color(200, 200, 200)
    local hoverColor = Color(255, 200, 100)

    local axis = self.hoverAxis

    self.pos = -self.angle:Forward() * self.dist

    cam.Start3D(self.pos, self.angle, self.fov, x, y, w, h, 1, 100)
        --render.Util_Render3DOBBBoxWireframe(self.min, Angle(), self.size.x, self.size.y, self.size.z, Color(180, 180, 180), true)
        
        color = normalColor

        if axis == axes.UP then color = hoverColor end
        render.SetMaterial(matFaceTop)
        render.DrawQuad(
            Vector(min.x, min.y, max.z),
            Vector(min.x, max.y, max.z),
            Vector(max.x, max.y, max.z),
            Vector(max.x, min.y, max.z),
            color
        )
        color = normalColor

        if axis == axes.DOWN then color = hoverColor end
        render.SetMaterial(matFaceBottom)
        render.DrawQuad(
            Vector(max.x, min.y, min.z),
            Vector(max.x, max.y, min.z),
            Vector(min.x, max.y, min.z),
            Vector(min.x, min.y, min.z),
            color
        )
        color = normalColor

        if axis == axes.BACK then color = hoverColor end
        render.SetMaterial(matFaceBack)
        render.DrawQuad(
            Vector(min.x, max.y, max.z),
            Vector(min.x, min.y, max.z),
            Vector(min.x, min.y, min.z),
            Vector(min.x, max.y, min.z),
            color
        )
        color = normalColor

        if axis == axes.FORWARD then color = hoverColor end
        render.SetMaterial(matFaceFront)
        render.DrawQuad(
            Vector(max.x, min.y, max.z),
            Vector(max.x, max.y, max.z),
            Vector(max.x, max.y, min.z),
            Vector(max.x, min.y, min.z),
            color
        )
        color = normalColor

        if axis == axes.LEFT then color = hoverColor end
        render.SetMaterial(matFaceLeft)
        render.DrawQuad(
            Vector(min.x, min.y, max.z),
            Vector(max.x, min.y, max.z),
            Vector(max.x, min.y, min.z),
            Vector(min.x, min.y, min.z),
            color
        )
        color = normalColor

        if axis == axes.RIGHT then color = hoverColor end
        render.SetMaterial(matFaceRight)
        render.DrawQuad(
            Vector(max.x, max.y, max.z),
            Vector(min.x, max.y, max.z),
            Vector(min.x, max.y, min.z),
            Vector(max.x, max.y, min.z),
            color
        )

        
    cam.End3D()
end


vgui.Register("DHoloBuilderViewCube", PANEL, "Panel")