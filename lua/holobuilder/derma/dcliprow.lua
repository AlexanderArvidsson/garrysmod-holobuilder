local PANEL = {}

function PANEL:Init()
    self.icon = vgui.Create("DImage", self)
    self.icon:SetImage("icon16/bullet_delete.png")
    self.icon:SetSize(16, 16)
    self.icon:SetPos(0, -1)

    self.label = vgui.Create("DLabel", self)
    self.label:SetPos(15, -2)

    self.btnRemove = vgui.Create("DImageButton", self)
    self.btnRemove:SetImage("icon16/cross.png")
    self.btnRemove:SetSize(16, 16)

    self:SetTall(16)
end

function PANEL:SetText(text)
    self.label:SetText(text)
end

function PANEL:PerformLayout(w, h)
    self.btnRemove:SetPos(w - 16, 0)
    self.label:SetWide(w - 15 - 16)
end



vgui.Register("DHoloBuilderClipRow", PANEL, "Panel")