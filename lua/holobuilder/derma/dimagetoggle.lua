local PANEL = {}

AccessorFunc(PANEL, "m_imageOn", "ImageOn")
AccessorFunc(PANEL, "m_imageOff", "ImageOff")

function PANEL:Init()


    self:SetImageOn("icon16/tick.png")
    self:SetImageOff("icon16/cross.png")
    self:SetOn(false)
end

function PANEL:SetOn(on)
    local old = self.on

    self.on = on

    self:UpdateImage()

    if old ~= self.on then
        self:OnValueChanged(self.on)
    end
end

function PANEL:Toggle()
    self:SetOn(not self.on)
end


-- For Override
function PANEL:OnValueChanged()

end

function PANEL:DoClick()
    self:Toggle()
end

function PANEL:UpdateImage()
    if self.on then
        if self:GetImageOn() then
            self:SetImage(self:GetImageOn())
            self.m_Image:SetImageColor(self.m_imageOnCol or Color(255, 255, 255))
        end
    else
        if self:GetImageOff() then
            self:SetImage(self:GetImageOff())
            self.m_Image:SetImageColor(self.m_imageOffCol or Color(255, 255, 255))
        end
    end
end

function PANEL:SetImageOn(img, col)
    self.m_imageOn = img
    self.m_imageOnCol = col

    self:UpdateImage()
end

function PANEL:SetImageOff(img, col)
    self.m_imageOff = img
    self.m_imageOffCol = col

    self:UpdateImage()
end


vgui.Register("DHoloBuilderImageToggle", PANEL, "DImageButton")