include("holobuilder/derma/dnumberwang.lua")
include("holobuilder/derma/dimagetoggle.lua")

local PANEL = {}

AccessorFunc(PANEL, "m_bEnabled", "Enabled", FORCE_BOOL)

function PANEL:Init()
    self.label = vgui.Create("DLabel", self)
    self.label:SetText("Label")
    self.label:SetPos(5, 0)

    self.entryX = vgui.Create("DHoloBuilderNumberWang", self)
    self.entryX:SetMinMax()
    self.entryX.OnValueChanged = function() self:OnValuesChanged(self:GetValues()) end
    HoloBuilder.Skin.PaintTextEntry(self.entryX, HoloBuilder.Skin.colorDark, HoloBuilder.Skin.colorDarker)

    self.entryY = vgui.Create("DHoloBuilderNumberWang", self)
    self.entryY:SetMinMax()
    self.entryY.OnValueChanged = function() self:OnValuesChanged(self:GetValues()) end
    HoloBuilder.Skin.PaintTextEntry(self.entryY, HoloBuilder.Skin.colorDark, HoloBuilder.Skin.colorDarker)

    self.entryZ = vgui.Create("DHoloBuilderNumberWang", self)
    self.entryZ:SetMinMax()
    self.entryZ.OnValueChanged = function() self:OnValuesChanged(self:GetValues()) end
    HoloBuilder.Skin.PaintTextEntry(self.entryZ, HoloBuilder.Skin.colorDark, HoloBuilder.Skin.colorDarker)

    self.toggle = vgui.Create("DHoloBuilderImageToggle", self)
    self.toggle:SetSize(16, 16)
    self.toggle:SetImageOn("holobuilder/toggle_local.png", Color(255, 150, 0))
    self.toggle:SetImageOff("holobuilder/toggle_local.png", Color(140, 140, 140))
    self.toggle:SetTooltip("Toggle Local")
    self.toggle:SetVisible(false)

    self:SetTall(45)
    self:SetEnabled(true)
end

function PANEL:SetText(text)
    self.label:SetText(text)
end

function PANEL:SetValues(x, y, z, force)
    self.entryX:SetValue(x, force)
    self.entryY:SetValue(y, force)
    self.entryZ:SetValue(z, force)
end

function PANEL:SetEnabled(bool)
    self.entryX:SetDisabled(not bool)
    self.entryY:SetDisabled(not bool)
    self.entryZ:SetDisabled(not bool)

    self.m_bEnabled = bool
end

function PANEL:OnValuesChanged(x, y, z)

end

function PANEL:GetValues()
    return self.entryX:GetValue(), self.entryY:GetValue(), self.entryZ:GetValue()
end

function PANEL:PerformLayout(w, h)
    local spacing = 5
    local cellWidth = math.ceil((w - 10) / 3) - spacing

    self.entryX:SetPos(5, 20)
    self.entryX:SetWide(cellWidth)

    self.entryY:SetPos(5 + cellWidth + spacing, 20)
    self.entryY:SetWide(cellWidth)

    self.entryZ:SetPos(5 + (cellWidth + spacing) * 2, 20)
    self.entryZ:SetWide(cellWidth)

    self.toggle:SetPos(w - self.toggle:GetWide() - 10, 2)

end

function PANEL:Paint(w, h)
    if self:GetEnabled() then
        surface.SetDrawColor(HoloBuilder.Skin.colorPrimary)
    else
        surface.SetDrawColor(HoloBuilder.Skin.colorDark)
    end
    surface.DrawRect(0, 0, w, h)
end

vgui.Register("DHoloBuilderTriNumEntry", PANEL, "Panel")