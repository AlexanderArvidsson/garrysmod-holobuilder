local PANEL = {}

function PANEL:Init()
    self:DockPadding(0, 3, 0, 0)
end

function PANEL:AddTool(tool, tooltip, icon, func)
    local b = self:Add("DImageButton")
    b:Dock(TOP)
    b:SetTooltip(tooltip)
    b:SetImage(icon)
    b:SetTall(32)
    b.tool = tool
    b.DoClick = function(pnl)
        func(pnl.tool, pnl)
    end
    b:DockMargin(2, 0, 0, 2)
    b.Paint = self.PaintButton

    return b
end

function PANEL:AddSpacer(space)
    local p = self:Add("Panel")
    p.Paint = function(pnl, w, h)
        surface.SetDrawColor(100, 100, 100)
        surface.DrawRect(0, math.floor(h / 2), w, 1)
    end

    space = space or 4

    p:Dock(TOP)
    p:DockMargin(4, 0, 4, 2)
    p:SetTall(space)
end

function PANEL.PaintButton(pnl, w, h)

    if pnl.Depressed then
        surface.SetDrawColor(HoloBuilder.Skin.colorLighter)
    elseif pnl.Hovered then
        surface.SetDrawColor(HoloBuilder.Skin.colorLight)
    else
        surface.SetDrawColor(HoloBuilder.Skin.colorPrimary)
    end
    surface.DrawRect(0, 0, w, h)

    if pnl.ToolSelected then
        surface.SetDrawColor(HoloBuilder.Skin.colorAccent)
        surface.Util_DrawOutlinedRect(0, 0, w, h, 2)
    end
end

vgui.Register("DHoloBuilderToolBar", PANEL, "Panel")