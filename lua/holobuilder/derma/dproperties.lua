include("holobuilder/derma/dtrinumentry.lua")
include("holobuilder/derma/dparentlinker.lua")
include("holobuilder/derma/dcliprow.lua")
include("holobuilder/derma/dpartlinker.lua")
include("holobuilder/derma/dhierarchy.lua")

local PANEL = {}

local function PaintPanel(pnl, w, h)
    surface.SetDrawColor(HoloBuilder.Skin.colorLight)
    surface.DrawRect(0, 0, w, h)
end

function PANEL:Init()
    self.parts = {}
    self.clips = {}

    self.divider = vgui.Create("DVerticalDivider", self)
    self.divider:Dock(FILL)
    self.divider:SetTopMin(100)
    self.divider:SetBottomMin(100)
    self.divider:SetTopHeight(200)

    self.partTree = vgui.Create("DHoloBuilderHierarchy", self)
    self.partTree.OnNodeSelected = function(pnl, node)
        if self._selectedNode then return end
        
        self._selected = true
        HoloBuilder:SelectPart(node.part, false)

        self.partParent:SelectPart(node.part)
        self._selected = false
    end
    self.partTree.OnPartVisibilityChanged = function(pnl, node, visible)
        if not node.part then return end

        node.part:SetVisible(visible)

        if input.IsKeyDown(KEY_LSHIFT) then
            local function SetChildrenVisible(part)
                for k, v in pairs(part:GetChildren()) do
                    if not v then continue end
                    v:SetVisible(visible)

                    if IsValid(v._propertyNode) then
                        v._propertyNode:SetPartVisible(visible, true)
                    end

                    SetChildrenVisible(v)
                end
            end

            SetChildrenVisible(node.part)
        end
    end
    self.partTree.OnPartLockChanged = function(pnl, node, locked)
        if not node.part then return end

        node.part:SetLocked(locked)

        if input.IsKeyDown(KEY_LSHIFT) then
            local function SetChildrenLocked(part)
                for k, v in pairs(part:GetChildren()) do
                    if not v then continue end
                    v:SetLocked(locked)

                    if IsValid(v._propertyNode) then
                        v._propertyNode:SetPartLocked(locked, true)
                    end

                    SetChildrenLocked(v)
                end
            end

            SetChildrenLocked(node.part)
        end
    end

    self.partTreeRoot = self.partTree:AddNode("Hierarchy")
    self.partTreeRoot:SetIcon("icon16/folder.png")
    HoloBuilder.Skin.PaintTreeNode(self.partTreeRoot)

    self.propertySheet = vgui.Create("DPropertySheet", self)

    -------------
    -- Project Sheet
    -------------
    self.projectSheet = vgui.Create("DPanel", self.propertySheet)
    self.projectSheet.Paint = PaintPanel
    self.propertySheet:AddSheet("Project", self.projectSheet, "icon16/package_green.png")

    self.projectSheetInner = vgui.Create("DPanel", self.projectSheet)
    self.projectSheetInner:Dock(FILL)
    self.projectSheetInner:DockPadding(0, 5, 0, 5)
    self.projectSheetInner.Paint = function() end

    self.projectSheetScroll = vgui.Create("DScrollPanel", self.projectSheetInner)
    self.projectSheetScroll:Dock(FILL)
    -------------


    -------------
    -- Project Name
    -------------
    self.projectLabelName = vgui.Create("DLabel", self.projectSheetScroll)
    self.projectLabelName:SetText("Project Name")
    self.projectLabelName:SetPos(10, 0)

    self.projectTextName = vgui.Create("DTextEntry", self.projectSheetScroll)
    self.projectTextName:SetPos(5, 20)
    self.projectTextName.OnChange = function(pnl)
        
    end
    HoloBuilder.Skin.PaintTextEntry(self.projectTextName)
    -------------


    self.projectSheetInner.PerformLayout = function(pnl, w, h)
        local nw = w - 10

        self.projectTextName:SetWide(nw)
    end



    -------------
    -- Part Properties Sheet
    -------------
    self.partSheet = vgui.Create("DPanel", self.propertySheet)
    self.partSheet.Paint = PaintPanel
    local sheet = self.propertySheet:AddSheet("Properties", self.partSheet, "icon16/brick.png")
    self.partTab = sheet.Tab
    self.partTab:SetVisible(false)

    self.partSheetInner = vgui.Create("DPanel", self.partSheet)
    self.partSheetInner:Dock(FILL)
    self.partSheetInner:DockPadding(0, 5, 0, 5)
    self.partSheetInner.Paint = function() end
    self.partSheetInner:SetVisible(false)

    self.partSheetScroll = vgui.Create("DScrollPanel", self.partSheetInner)
    self.partSheetScroll:Dock(FILL)

    local propertiesY = 0
    -------------

    
    -------------
    -- Part Name
    -------------
    self.partLabelName = vgui.Create("DLabel", self.partSheetScroll)
    self.partLabelName:SetText("Name")
    self.partLabelName:SetPos(10, propertiesY)
    propertiesY = propertiesY + 20

    self.partTextName = vgui.Create("DTextEntry", self.partSheetScroll)
    self.partTextName:SetPos(5, propertiesY)
    self.partTextName.OnChange = function(pnl)
        if self.selectedPart and self.selectedPart:GetName() then
            self.selectedPart:SetName(pnl:GetValue())

            self:UpdatePartNode(self.selectedPart)
        end
    end
    HoloBuilder.Skin.PaintTextEntry(self.partTextName)
    propertiesY = propertiesY + 25
    -------------


    -------------
    -- Part Position
    -------------
    self.partPosEntry = vgui.Create("DHoloBuilderTriNumEntry", self.partSheetScroll)
    self.partPosEntry:SetPos(5, propertiesY)
    self.partPosEntry:SetText("Position")
    self.partPosEntry.OnValuesChanged = function(pnl, x, y, z)
        if self._selected then return end

        if self.selectedPart and self.selectedPart:GetPos() then
            self._typed = true

            if self.selectedPart:IsPosLocal() then
                self.selectedPart:SetPos(Vector(x, y, z) - self.selectedPart:GetOrigin())
            else
                self.selectedPart:SetPos(Vector(x, y, z))
            end

            self._typed = false
        end
    end

    self.partPosEntry.toggle.OnValueChanged = function(pnl, on)
        if self._selected then return end

        if self.selectedPart then
            self.selectedPart:SetPosIsLocal(on)

            self:UpdateSelectedPart()
        end
    end
    propertiesY = propertiesY + 50
    -------------

    -------------
    -- Part Angle
    -------------
    self.partAngEntry = vgui.Create("DHoloBuilderTriNumEntry", self.partSheetScroll)
    self.partAngEntry:SetPos(5, propertiesY)
    self.partAngEntry:SetText("Angle")
    self.partAngEntry.OnValuesChanged = function(pnl, x, y, z)
        if self._selected then return end

        local part = self.selectedPart
        if part and part:GetAngles() then
            self._typed = true

            local newAng = Angle(x, y, z)

            if part:IsAngleLocal() then
                local offset = -part:GetOrigin()

                offset:Rotate(newAng)

                local pos = part:LocalToWorld(part:GetOrigin())
                if part:GetParent() then
                    pos = part:GetParent():WorldToLocal(pos)
                end
                part:SetPos(pos + offset)
            end

            part:SetAngles(newAng)

            self._typed = false

            self:UpdateSelectedPartPos()
        end
    end

    self.partAngEntry.toggle.OnValueChanged = function(pnl, on)
        if self._selected then return end

        if self.selectedPart then
            self.selectedPart:SetAngleIsLocal(on)

            self:UpdateSelectedPart()
        end
    end
    propertiesY = propertiesY + 50
    -------------

    -------------
    -- Part Scale
    -------------
    self.partScaleEntry = vgui.Create("DHoloBuilderTriNumEntry", self.partSheetScroll)
    self.partScaleEntry:SetPos(5, propertiesY)
    self.partScaleEntry:SetText("Scale")
    self.partScaleEntry.OnValuesChanged = function(pnl, x, y, z)
        if self._selected then return end

        local part = self.selectedPart

        if part and part:GetScale() then
            self._typed = true

            local newScale = Vector(x, y, z)

            print("changed")

            if part:IsScaleLocal() then
                local oldScale = Vector()
                local oldOrigin = Vector()
                oldScale:Set(part:GetScale())
                oldOrigin:Set(part:GetOrigin())

                local fract = Vector(
                    math.max(newScale.x / oldScale.x, 0),
                    math.max(newScale.y / oldScale.y, 0),
                    math.max(newScale.z / oldScale.z, 0))

                local pos = part:LocalToWorld(part:GetOrigin() - part:GetOrigin() * fract)
                if part:GetParent() then
                    pos = part:GetParent():WorldToLocal(pos)
                end
                
                part:SetPos(pos)
                part:SetOrigin(oldOrigin * fract)
            end

            part:SetScale(newScale)

            self._typed = false
        end
    end

    self.partScaleEntry.toggle.OnValueChanged = function(pnl, on)
        if self._selected then return end

        if self.selectedPart then
            self.selectedPart:SetScaleIsLocal(on)

            self:UpdateSelectedPart()
        end
    end
    propertiesY = propertiesY + 50
    -------------

    -------------
    -- Part Origin
    -------------
    self.partOriginEntry = vgui.Create("DHoloBuilderTriNumEntry", self.partSheetScroll)
    self.partOriginEntry:SetPos(5, propertiesY)
    self.partOriginEntry:SetText("Origin")
    self.partOriginEntry.OnValuesChanged = function(pnl, x, y, z)
        if self._selected then return end

        if self.selectedPart and self.selectedPart:GetScale() then
            self._typed = true
            self.selectedPart:SetOrigin(Vector(x, y, z))
            self._typed = false

            self:UpdateSelectedPartPos()
        end
    end
    propertiesY = propertiesY + 50
    -------------


    -------------
    -- Part Parent
    -------------
    self.partParent = vgui.Create("DHoloBuilderParentLinker", self.partSheetScroll)
    self.partParent:SetPos(5, propertiesY)
    propertiesY = propertiesY + 25
    -------------


    -------------
    -- Part Clips
    -------------
    self.partLabelClips = vgui.Create("DLabel", self.partSheetScroll)
    self.partLabelClips:SetText("Clips")
    self.partLabelClips:SetPos(10, propertiesY)
    propertiesY = propertiesY + 20

    self.partClipsScroll = vgui.Create("DScrollPanel", self.partSheetScroll)
    self.partClipsScroll:SetPos(5, propertiesY)
    self.partClipsScroll:SetTall(80)
    HoloBuilder.Skin.PaintScrollPanel(self.partClipsScroll)
    propertiesY = propertiesY + 85

    self.partClips = vgui.Create("DListLayout", self.partClipsScroll)
    self.partClips:SetPos(0, 0)
    self.partClips:Dock(TOP)

    self.partClipLinker = vgui.Create("DHoloBuilderPartLinker", self.partSheetScroll)
    self.partClipLinker:SetText("Link Clip Plane")
    self.partClipLinker:SetPos(5, propertiesY)
    self.partClipLinker.OnPartLink = function(pnl, part)
        if not part then return true end

        if part:GetType() ~= HB_PART_TYPE_CLIP then
            pnl:SetError("Not a clip plane!")
        else
            local selected = HoloBuilder:GetSelectedPart()

            if selected then
                selected:AddClipPlane(part)
                self:UpdateSelectedPart()
            end
        end

        return true
    end
    -------------


    -------------
    -- Part Appearance Sheet
    -------------
    self.partAppearanceSheet = vgui.Create("DPanel", self.propertySheet)
    self.partAppearanceSheet.Paint = PaintPanel
    local sheet = self.propertySheet:AddSheet("Appearance", self.partAppearanceSheet, "icon16/color_wheel.png")
    self.partAppearanceTab = sheet.Tab
    self.partAppearanceTab:SetVisible(false)

    self.partAppearanceSheetInner = vgui.Create("DPanel", self.partAppearanceSheet)
    self.partAppearanceSheetInner:Dock(FILL)
    self.partAppearanceSheetInner:DockPadding(0, 5, 0, 5)
    self.partAppearanceSheetInner.Paint = function() end
    self.partAppearanceSheetInner:SetVisible(false)

    self.partAppearanceSheetScroll = vgui.Create("DScrollPanel", self.partAppearanceSheetInner)
    self.partAppearanceSheetScroll:Dock(FILL)
    -------------


    -------------
    -- Part Model
    -------------
    self.partLabelModel = vgui.Create("DLabel", self.partAppearanceSheetScroll)
    self.partLabelModel:SetText("Model")
    self.partLabelModel:SetPos(10, 0)

    self.partTextModel = vgui.Create("DTextEntry", self.partAppearanceSheetScroll)
    self.partTextModel:SetPos(5, 20)
    self.partTextModel.OnChange = function(pnl)
        if self.selectedPart and self.selectedPart:GetModel() then
            self.selectedPart:SetModel(pnl:GetValue())

            self:UpdatePartNode(self.selectedPart)
        end
    end
    HoloBuilder.Skin.PaintTextEntry(self.partTextModel)
    -------------


    -------------
    -- Part Material
    -------------
    self.partLabelMaterial = vgui.Create("DLabel", self.partAppearanceSheetScroll)
    self.partLabelMaterial:SetText("Material")
    self.partLabelMaterial:SetPos(10, 40)

    self.partTextMaterial = vgui.Create("DTextEntry", self.partAppearanceSheetScroll)
    self.partTextMaterial:SetPos(5, 60)
    self.partTextMaterial.OnChange = function(pnl)
        if self.selectedPart and self.selectedPart:GetModel() then
            self.selectedPart:SetMaterial(pnl:GetValue())
        end
    end
    HoloBuilder.Skin.PaintTextEntry(self.partTextMaterial)
    -------------


    -------------
    -- Part Color
    -------------
    self.partLabelColor = vgui.Create("DLabel", self.partAppearanceSheetScroll)
    self.partLabelColor:SetText("Color")
    self.partLabelColor:SetPos(10, 80)

    self.partMixerColor = vgui.Create("DColorMixer", self.partAppearanceSheetScroll)
    self.partMixerColor:SetPos(5, 100)
    self.partMixerColor:SetTall(92)
    self.partMixerColor:SetPalette(false)
    self.partMixerColor.ValueChanged = function(pnl, col)
        if self.selectedPart and self.selectedPart:GetColor() then
            self.selectedPart:SetColor(col)
        end
    end
    HoloBuilder.Skin.PaintColorMixer(self.partMixerColor)
    -------------



    self.propertySheet:SwitchToName("Part")


    self.partSheetInner.PerformLayout = function(pnl, w, h)
        local nw = w - 10

        if self.partSheetScroll.VBar:IsVisible() then nw = nw - 15 end

        self.partTextName:SetWide(nw)
        self.partPosEntry:SetWide(nw)
        self.partAngEntry:SetWide(nw)
        self.partScaleEntry:SetWide(nw)
        self.partOriginEntry:SetWide(nw)
        self.partParent:SetWide(nw)
        self.partClipsScroll:SetWide(nw)
        self.partClipLinker:SetWide(nw)
    end


    self.partAppearanceSheetInner.PerformLayout = function(pnl, w, h)
        local nw = w - 10

        if self.partAppearanceSheetScroll.VBar:IsVisible() then nw = nw - 15 end

        self.partTextModel:SetWide(nw)
        self.partTextMaterial:SetWide(nw)
        self.partMixerColor:SetWide(nw)
    end
    
    self.divider:SetTop(self.partTree)
    self.divider:SetBottom(self.propertySheet)
end

function PANEL:SelectedPart(part, selectNode)
    if self.partClipLinker:SelectPart(part) then return true end

    if part and part._propertyNode then
        if selectNode == nil then selectNode = true end

        if selectNode then
            self:ExpandToPart(part)
        end
    else
        if IsValid(self.partTree.m_pSelectedItem) then
            self.partTree.m_pSelectedItem:SetSelected(false)
        end
    end


    self.selectedPart = part

    self:UpdateSelectedPart()
end

function PANEL:ExpandToPart(part)
    if not part then return end

    if IsValid(part._propertyNode) then
        self._selectedNode = true
        self.partTree:SetSelectedItem(part._propertyNode)

        part._propertyNode:SetExpanded(true)

        local i = 0
        local parent = part._propertyNode:GetParent():GetParent()

        while IsValid(parent) and parent:GetName() == self.partTreeRoot:GetName() and i < 10 do
            parent:SetExpanded(true)

            parent = parent:GetParent():GetParent()
            i = i + 1
        end
        self._selectedNode = false
    end
end

function PANEL:InsertPartNode(part, partIcon, clipIcon)
    if IsValid(part._propertyNode) then return end

    local node = self.partTreeRoot

    if part:GetParent() then
        if not IsValid(part:GetParent()._propertyNode) then
            self:InsertPartNode(part:GetParent(), partIcon, clipIcon)
        end

        local n = part:GetParent()._propertyNode
        if IsValid(n) then
            node = n
        end
    end

    local icon = partIcon
    if part:GetType() == HB_PART_TYPE_CLIP then
        icon = clipIcon
    end

    part._propertyNode = node:AddNode(part:GetName())
    part._propertyNode.part = part
    part._propertyNode:SetIcon(icon)

    part._propertyNode:SetVisibilityButtonVisible(true)
    part._propertyNode:SetLockButtonVisible(true)
    part._propertyNode:SetPartVisible(part:IsVisible(), true)
    part._propertyNode:SetPartLocked(part:IsLocked(), true)


    HoloBuilder.Skin.PaintTreeNode(part._propertyNode)

end

function PANEL:ClearNodes()
    for k, v in pairs(self.parts) do
        if not v then return end

        v._propertyNode = nil
    end

    for k, v in pairs(self.clips) do
        if not v then return end

        v._propertyNode = nil
    end

    if IsValid(self.partTreeRoot.ChildNodes) then
        for k, v in pairs(self.partTreeRoot.ChildNodes:GetChildren()) do
            if IsValid(v) then
                v:Remove()
            end
        end
    end
end

function PANEL:PopulateParts(parts, clips)
    self:ClearNodes()
    local partIcon = "icon16/brick.png"
    local clipIcon = "icon16/bullet_delete.png"

    for k, v in pairs(parts) do
        if not v then return end

        self:InsertPartNode(v, partIcon, clipIcon)
    end

    for k, v in pairs(clips) do
        if not v then return end

        self:InsertPartNode(v, partIcon, clipIcon)
    end

    self.parts = parts
    self.clips = clips
end

function PANEL:UpdateSelectedPart()
    if self._typed then return end

    self._selected = true

    local part = self.selectedPart

    if part then
        local tab = self.propertySheet:GetActiveTab():GetText()

        if tab ~= "Properties" and tab ~= "Appearance" then
            self.propertySheet:SwitchToName("Properties")
        end

        self.partTab:SetVisible(true)
        self.partSheetInner:SetVisible(true)

        self.partAppearanceTab:SetVisible(true)
        self.partAppearanceSheetInner:SetVisible(true)

        self.partTextName:SetText(part:GetName())
        self.partTextModel:SetText(part:GetModel() or "")
        self.partTextMaterial:SetText(part:GetMaterial() or "")

        self.partTextModel:SetEnabled(part:GetType() ~= HB_PART_TYPE_CLIP)
        self.partTextModel:SetDisabled(part:GetType() == HB_PART_TYPE_CLIP)
        self.partTextMaterial:SetEnabled(part:GetType() ~= HB_PART_TYPE_CLIP)
        self.partTextMaterial:SetDisabled(part:GetType() == HB_PART_TYPE_CLIP)

        self:UpdateSelectedPartPos()

        local ang = part:GetLocalAngles()
        self.partAngEntry:SetValues(ang.p, ang.y, ang.r, true)
        self.partAngEntry.toggle:SetVisible(true)
        self.partAngEntry.toggle:SetOn(part:IsAngleLocal())

        local scale = part:GetScale() or Vector()
        self.partScaleEntry:SetValues(scale.x, scale.y, scale.z, true)
        self.partScaleEntry:SetEnabled(part:GetScale() ~= nil)
        self.partScaleEntry.toggle:SetVisible(true)
        self.partScaleEntry.toggle:SetOn(part:IsScaleLocal())

        local origin = part:GetOrigin()
        self.partOriginEntry:SetValues(origin.x, origin.y, origin.z, true)

        self.partParent:SetParent(part:GetParent())

        self.partMixerColor:SetColor(part:GetColor() or Color(255, 255, 255, 255))
        self.partMixerColor:SetDisabled(part:GetColor() == nil)

        self.partClips:Clear()
        local clips = part:GetClipPlanes() or {}
        for k, v in pairs(clips) do
            local row = vgui.Create("DHoloBuilderClipRow")
            row:SetText(v:GetName())
            row.btnRemove.DoClick = function()
                part:RemoveClipPlane(v)
                self:UpdateSelectedPart()
            end

            self.partClips:Add(row)
        end

        self.partClipLinker:SetDisabled(part:GetType() == HB_PART_TYPE_CLIP)
        self.partClipLinker:Reset()
    else
        self.partTab:SetVisible(false)
        self.partSheetInner:SetVisible(false)

        self.partAppearanceTab:SetVisible(false)
        self.partAppearanceSheetInner:SetVisible(false)

        self.propertySheet:SwitchToName("Project")
    end

    self._selected = false
end

function PANEL:UpdateSelectedPartPos()
    if self._typed then return end

    self._selected = true

    local part = self.selectedPart

    if part then
        local pos = part:GetLocalPos()
        if part:IsPosLocal() then
            pos = pos + part:GetOrigin()
        end
        self.partPosEntry:SetValues(pos.x, pos.y, pos.z, true)
        
        self.partPosEntry.toggle:SetVisible(true)
        self.partPosEntry.toggle:SetOn(part:IsPosLocal())
    end

    self._selected = false
end

function PANEL:UpdatePartNode(part)
    if not part or not part._propertyNode then return end

    part._propertyNode:SetText(part:GetName())
end

function PANEL:PerformLayout(w, h)

end

function PANEL:Paint(w, h)
end

vgui.Register("DHoloBuilderProperties", PANEL, "Panel")