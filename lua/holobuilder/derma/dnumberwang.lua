local PANEL = {}

function PANEL:Init()
    self.Up.DoClick = function( button, mcode )
        if self:GetDisabled() then return end
        self:SetValue( self:GetValue() + 1 )
    end

    self.Down.DoClick = function( button, mcode )
        if self:GetDisabled() then return end
        self:SetValue( self:GetValue() - 1 )
    end
end

--[[---------------------------------------------------------
   Name: SetValue
-----------------------------------------------------------]]
function PANEL:SetValue( val, force )

    if ( val == nil ) then return end
    
    local OldValue = val
    val = tonumber( val )
    val = val or 0
    
    if ( self.m_numMax != nil ) then
        val = math.min( self.m_numMax, val )
    end
    
    if ( self.m_numMin != nil ) then
        val = math.max( self.m_numMin, val )
    end
    
    if ( self.m_iDecimals == 0 ) then
    
        val = Format( "%i", val )
    
    elseif ( val != 0 ) then
    
        val = Format( "%."..self.m_iDecimals.."f", val )
            
        -- Trim trailing 0's and .'s 0 this gets rid of .00 etc
        val = string.TrimRight( val, "0" )      
        val = string.TrimRight( val, "." )
        
    end
    
    --
    -- Don't change the value while we're typing into it!
    -- It causes confusion!
    --
    if ( !self:HasFocus() or force ) then
        self:SetText( val )
        self:ConVarChanged( val )
    end
    
    self:OnValueChanged( val )

end

derma.DefineControl( "DHoloBuilderNumberWang", "Menu Option Line", PANEL, "DNumberWang" )
