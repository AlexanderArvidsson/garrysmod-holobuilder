local PANEL = {}

function PANEL:Init()

end

function PANEL:AddLine(icon, ...)
    local line = DListView.AddLine(self, ...)

    line.Icon = vgui.Create("DImage", line)
    line.Icon:SetImage(icon)
    line.Icon:SetSize(16, 16)

    line.DataLayout = function(line, listView)
        line:ApplySchemeSettings()

        local height = line:GetTall()

        local x = 0
        for i, col in ipairs(line.Columns) do
            local w = listView:ColumnWidth(i)

            if i == 1 then
                col:SetPos(x + 16, 0)
                line.Icon:SetPos(0, 0)
            else
                col:SetPos(x, 0)
            end
            col:SetSize(w, height)
            x = x + w
        end
    end

    return line
end


vgui.Register("DHoloBuilderIconListView", PANEL, "DListView")