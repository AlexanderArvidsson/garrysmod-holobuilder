local PANEL = {}

function PANEL:Init()
    self:SetTall(20)

    self.label = vgui.Create("DLabel", self)
    self.label:SetText("")
    self.label:SetPos(5, 0)
end

function PANEL:SetText(text)
    self.label:SetText(text)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(HoloBuilder.Skin.colorDark)
    surface.DrawRect(0, 0, w, h)
end

vgui.Register("DHoloBuilderStatusBar", PANEL, "Panel")