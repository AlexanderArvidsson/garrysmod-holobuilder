local PANEL = {}

AccessorFunc(PANEL, "m_bVisibilityButtonVisible", "VisibilityButtonVisible", FORCE_BOOL)
AccessorFunc(PANEL, "m_bLockButtonVisible", "LockButtonVisible", FORCE_BOOL)

function PANEL:Init()
    self.VisibilityButton = vgui.Create("DImageButton", self)
    self.VisibilityButton:SetImage("holobuilder/eye.png")
    self.VisibilityButton:SizeToContents()

    self.VisibilityButton.DoClick = function(pnl)
        self:SetPartVisible(not pnl.visible)
    end

    self.LockButton = vgui.Create("DImageButton", self)
    self.LockButton:SetImage("holobuilder/lock_off.png")
    self.LockButton:SizeToContents()

    self.LockButton.DoClick = function(pnl)
        self:SetPartLocked(not pnl.locked)
    end

    self:SetPartLocked(false)
    self:SetPartVisible(true)
end

function PANEL:SetPartVisible(visible, override)
    self.VisibilityButton.visible = visible

    if visible then
        self.VisibilityButton:SetColor(HoloBuilder.Skin.colorAccent)
    else
        self.VisibilityButton:SetColor(HoloBuilder.Skin.colorLight)
    end

    self:OnPartVisibilityChanged(visible)

    if not override and IsValid(self:GetRoot()) then
        self:GetRoot():OnPartVisibilityChanged(self, visible)
    end
end

function PANEL:SetPartLocked(locked, override)
    self.LockButton.locked = locked

    if locked then
        self.LockButton:SetImage("holobuilder/lock_on.png")
        self.LockButton:SetColor(HoloBuilder.Skin.colorAccent)
    else
        self.LockButton:SetImage("holobuilder/lock_off.png")
        self.LockButton:SetColor(HoloBuilder.Skin.colorLight)
    end

    self:OnPartLockChanged(locked)

    if not override and IsValid(self:GetRoot()) then
        self:GetRoot():OnPartLockChanged(self, locked)
    end
end


-- For Override
function PANEL:OnPartVisibilityChanged(visible)
end

function PANEL:OnPartLockChanged(locked)
end

function PANEL:AddNode(strName, strIcon)
    self:CreateChildNodes()
    
    local pNode = vgui.Create("DHoloBuilderHierarchy_Node", self)
        pNode:SetText(strName)
        pNode:SetParentNode(self)
        pNode:SetRoot(self:GetRoot())
        pNode:SetIcon(strIcon)
        pNode:SetDrawLines(not self:IsRootNode())

        self:InstallDraggable(pNode)
    
    self.ChildNodes:Add(pNode)
    self:InvalidateLayout()
    
    return pNode
end

function PANEL:PerformLayout(w, h)
    DTree_Node.PerformLayout(self, w, h)

    local LineHeight = self:GetLineHeight()
    local offset = 0

    if self:GetVisibilityButtonVisible() then
        self.VisibilityButton:SetVisible(true)
        self.VisibilityButton:SetPos(self.Icon.x + self.Icon:GetWide() + 4, self.Icon.y)

        offset = offset + self.VisibilityButton:GetWide() + 4
    else
        self.VisibilityButton:SetVisible(false)
    end

    if self:GetLockButtonVisible() then
        self.LockButton:SetVisible(true)
        self.LockButton:SetPos(self.Icon.x + self.Icon:GetWide() + offset + 4, self.Icon.y)

        offset = offset + self.LockButton:GetWide() + 4
    else
        self.LockButton:SetVisible(false)
    end

    self.Label:SetTextInset(self.Icon.x + self.Icon:GetWide() + offset + 4, 0)
end

vgui.Register("DHoloBuilderHierarchy_Node", PANEL, "DTree_Node")