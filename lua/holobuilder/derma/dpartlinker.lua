local PANEL = {}

function PANEL:Init()
    self:Reset()
    self:SetTextColor(Color(255, 255, 255))

    HoloBuilder.Skin.PaintPartLinker(self)
end

function PANEL:Reset()
    self.linking = false
    self.error = nil
    
    if self._originalText then
        self:SetText(self._originalText)
    end

    self:UpdateColor()
end

function PANEL:SetError(err)
    self.linking = false
    self.error = err

    self._originalText = self:GetText()
    self:SetText(err)

    self:UpdateColor()
end

function PANEL:UpdateColor()
    if self.error then
        self:SetTextColor(Color(255, 0, 0))
    elseif self.linking then
        self:SetTextColor(Color(255, 150, 0))
    else
        self:SetTextColor(Color(255, 255, 255))
    end
end

function PANEL:IsLinking()
    return self.linking
end

function PANEL:DoClick()
    if not HoloBuilder:GetSelectedPart() then return end

    self.linking = not self.linking

    self:UpdateColor()
end

function PANEL:SelectPart(part)
    local selectedPart = HoloBuilder:GetSelectedPart()

    if self:IsLinking() and selectedPart then
        self:Reset()

        if self:OnPartLink(part) then
            return true
        end
    end

    return false
end

function PANEL:OnPartLink(part)
    return true
end

function PANEL:Think()
    if input.IsMouseDown(MOUSE_RIGHT) and self.linking then
        self.linking = false
    end
end

vgui.Register("DHoloBuilderPartLinker", PANEL, "DButton")