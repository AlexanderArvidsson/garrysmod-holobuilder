include("diconlistview.lua")

local PANEL = {}

PANEL.TYPE_SAVE = 1
PANEL.TYPE_OPEN = 2

local function PrettifySize(size)
    if size >= 1000000 then
        return math.ceil(size / 1000000) .. " MB"
    elseif size >= 1000 then
        return math.ceil(size / 1000) .. " KB"
    end

    return size .. " B"
end

function PANEL:Init()
    self.iconFolder = "icon16/folder.png"
    self.iconUp = "icon16/folder.png"
    self.iconFile = "icon16/brick.png"

    self.basePath = ""

    self.formats = {}
    self.files = {}

    self.list = vgui.Create("DHoloBuilderIconListView", self)
    self.list:SetMultiSelect(false)
    self.list:AddColumn("Name")
    self.list:AddColumn("Size")
    self.list:SetPos(5, 28)

    self.list.DoDoubleClick = function(pnl, lineId, line)
        if self:HasUpLine() and lineId == 1 then
            self:GoUpPath()

            return
        end

        if line.type == "directory" then
            self:SetPath(line.path)
        elseif line.type == "file" then
            self:OnSelectedFileInternal(line:GetColumnText(1))
        end
    end

    self.list.OnRowSelected = function(pnl, lineId, line)
        if self:HasUpLine() and lineId == 1 then
            return
        end

        if line.type ~= "file" then return end

        self.textFile:SetText(line:GetColumnText(1))
    end

    self.list.SortByColumn = function(pnl, ColumnID, Desc)
        table.Copy(pnl.Sorted, pnl.Lines)
        
        table.sort(pnl.Sorted, function(a, b)
            if Desc then
                a, b = b, a
            end
            
            local aval = a:GetSortValue(ColumnID) and a:GetSortValue(ColumnID) or a:GetColumnText(ColumnID)
            local bval = b:GetSortValue(ColumnID) and b:GetSortValue(ColumnID) or b:GetColumnText(ColumnID)

            if aval == "..." then
                return false
            end

            local dirFlag = true

            if Desc then dirFlag = false end

            if a.type == "directory" and b.type == "file" then
                return dirFlag
            end
            if b.type == "directory" and a.type == "file" then
                return not dirFlag
            end

            return aval < bval

        end )

        pnl:SetDirty(true)
        pnl:InvalidateLayout()

    end

    --self.hierarchy = vgui.Create("DTree", self)

    --[[self.divider = vgui.Create("DHoloBuilderHorizontalDivider", self)
    self.divider:SetPos(5, 28)

    self.divider:SetRight(self.list)
    self.divider:SetLeft(self.hierarchy)]]

    self.btnCancel = vgui.Create("DButton", self)
    self.btnCancel:SetWide(70)
    self.btnCancel:SetText("Cancel")
    self.btnCancel.DoClick = function() self:Close() end

    self.btnPrimary = vgui.Create("DButton", self)
    self.btnPrimary:SetWide(70)

    local function SelectFile()
        if self.textFile:GetText() == "" then return end

        if self.type == self.TYPE_SAVE then
            self:OnSelectedFileInternal(self.textFile:GetText())
        else
            local text = self.textFile:GetText()
            local hasFile = false

            for i, fName in ipairs(self.files) do
                if fName == text then
                    hasFile = true
                    break
                end
            end

            if hasFile then
                self:OnSelectedFileInternal(text)
            end
        end
    end

    self.btnPrimary.DoClick = SelectFile

    self.labelFormat = vgui.Create("DLabel", self)
    self.labelFormat:SetText("Format: ")
    self.labelFormat:SetContentAlignment(6)
    self.labelFormat:SetVisible(false)

    self.formatBox = vgui.Create("DComboBox", self)
    self.formatBox:SetTall(22)
    self.formatBox:SetVisible(false)

    self.labelFile = vgui.Create("DLabel", self)
    self.labelFile:SetText("File Name: ")
    self.labelFile:SetContentAlignment(6)

    self.textFile = vgui.Create("DTextEntry", self)
    self.textFile:SetTall(22)
    self.textFile.OnEnter = SelectFile

    --HoloBuilder.Skin.PaintTree(self.hierarchy, HoloBuilder.Skin.colorDark)
    HoloBuilder.Skin.PaintList(self.list, HoloBuilder.Skin.colorDark)
    HoloBuilder.Skin.PaintTextEntry(self.textFile, HoloBuilder.Skin.colorDark)
    HoloBuilder.Skin.PaintComboBox(self.formatBox)
    HoloBuilder.Skin.PaintButton(self.btnCancel, HoloBuilder.Skin.colorDark)
    HoloBuilder.Skin.PaintButton(self.btnPrimary, HoloBuilder.Skin.colorDark)

    self:SetTitle("File Browser")
    self:SetSize(500, 400)
    self:SetSizable(true)
    self:SetMinimumSize(400, 200)
    self:SetType(self.TYPE_OPEN)
end

function PANEL:AddFormats(formats)
    self.formats = formats

    for k, format in ipairs(formats) do
        local name = format.text .. " (" .. format.extension .. ")"
        self.formatBox:AddChoice(name)
    end

    self.formatBox:ChooseOptionID(1)
end

function PANEL:GetFormat()
    return self.formats[self.formatBox:GetSelectedID()]
end

function PANEL:SetType(type)
    self.type = type

    if type == self.TYPE_SAVE then
        self.btnPrimary:SetText("Save")
        self.formatBox:SetVisible(true)
        self.labelFormat:SetVisible(true)

    elseif type == self.TYPE_OPEN then
        self.btnPrimary:SetText("Open")
        self.formatBox:SetVisible(false)
        self.labelFormat:SetVisible(false)
    end
end

function PANEL:AddLine(...)
    local line = self.list:AddLine(...)

    HoloBuilder.Skin.PaintListViewLine(line)

    return line
end

function PANEL:HasUpLine()
    return self.path ~= self.basePath
end

function PANEL:PerformLayout(w, h)
    DFrame.PerformLayout(self, w, h)

    local nw = w - 10
    local nh = h - 28 - 5

    local btnCw = self.btnCancel:GetWide()
    local btnPw = self.btnPrimary:GetWide()

    local offY = 0
    if self.type == self.TYPE_SAVE then
        offY = self.formatBox:GetTall() + 5
    end

    self.list:SetSize(nw, nh - self.textFile:GetTall() - 5 - offY)
    self.btnCancel:SetPos(w - 5 - btnCw, h - self.btnCancel:GetTall() - 5)

    self.btnPrimary:SetPos(w - 5 - btnCw - btnPw - 5, h - self.btnCancel:GetTall() - 5)

    self.textFile:SetPos(100, h - self.textFile:GetTall() - 5)
    self.textFile:SetWide(w - 100 - btnCw - btnPw - 5 - 5 - 5)

    self.labelFile:SetPos(35, h - self.textFile:GetTall() - 5)

    if self.type == self.TYPE_SAVE then
        self.formatBox:SetPos(100, h - self.textFile:GetTall() - 5 - self.formatBox:GetTall() - 5)
        self.formatBox:SetWide(w - 100 - btnCw - btnPw - 5 - 5 - 5)

        self.labelFormat:SetPos(35, h - self.textFile:GetTall() - 5 - self.formatBox:GetTall() - 5)
    end
end

function PANEL:GoUpPath()
    if self.path == self.basePath then return end

    local index = self.path:match("^.*()/")
    if not index and self.path ~= self.basePath then
        index = 1
    end

    local newPath = self.path:sub(1, index - 1)
    
    self:SetPath(newPath, self.dir)
end

function PANEL:SetBasePath(path)
    if string.EndsWith(path, "/") then
        path = path:sub(1, string.len(path) - 1)
    end

    self.basePath = path
end

function PANEL:GetBasePath()
    return self.basePath
end

function PANEL:SetPath(path, dir)
    dir = dir or "DATA"

    if string.EndsWith(path, "/") then
        path = path:sub(1, string.len(path) - 1)
    end

    self.list:Clear()
    self.files = {}

    self.path = path
    self.dir = dir

    local checkPath = "*"
    if path ~= "" then
        checkPath = path .. "/*"
    end

    if path ~= self.basePath then
        self:AddLine(self.iconUp, "...")
    end

    local files, directories = file.Find(checkPath, dir)

    for k, fName in ipairs(directories) do
        local line = self:AddLine(self.iconFolder, fName)
        line.type = "directory"

        if path == "" then
            line.path = fName
        else
            line.path = path .. "/" .. fName
        end
    end

    for k, fName in ipairs(files) do
        local fullPath = path .. "/" .. fName
        local size = file.Size(fullPath, dir)

        self.files[k] = fName

        local line = self:AddLine(self.iconFile, fName, PrettifySize(size or 0))
        line.path = fName
        line.type = "file"
    end
end

function PANEL:OnSelectedFileInternal(fName)
    local path = self.path
    if path ~= "" then
        path = path .. "/"
    end
    if self.basePath ~= "" then
        path = path:sub(string.len(self.basePath) + 2)
    end

    if self.type == self.TYPE_SAVE then
        local ext = self:GetFormat().extension
        if not string.EndsWith(fName, ext) then
            fName = fName .. ext
        end
    end

    if self.type == self.TYPE_SAVE then
        local fileExists = false
        for i, v in ipairs(self.files) do
            if v == fName then
                fileExists = true
                break
            end
        end

        if fileExists then
            Derma_Query("Are you sure you want to overwrite this file?\nThis cannot be undone!", "Overwrite",
                "Confirm", function()
                    self:OnSelectedFile(path, fName, self.dir, self:GetFormat())
                    self:Close()
                end,
                "Cancel", function() end)
        else
            self:OnSelectedFile(path, fName, self.dir, self:GetFormat())
            self:Close()
        end
    else
        self:OnSelectedFile(path, fName, self.dir)
        self:Close()
    end
end

-- For override
function PANEL:OnSelectedFile(path, fName)
end


vgui.Register("DHoloBuilderFileDialog", PANEL, "DFrame")

--[[if IsValid(FileDialog) then
    FileDialog:Remove()
end

FileDialog = vgui.Create("DHoloBuilderFileDialog")
FileDialog:Center()
FileDialog:MakePopup()

FileDialog:SetBasePath("holobuilder")

FileDialog.OnSelectedFile = function(pnl, path, file, dir)
    print(path, file, dir)
end]]