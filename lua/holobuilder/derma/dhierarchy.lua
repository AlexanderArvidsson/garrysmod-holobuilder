include("holobuilder/derma/dhierarchy_node.lua")

local PANEL = {}

function PANEL:Init()
    HoloBuilder.Skin.PaintTree(self, HoloBuilder.Skin.colorDark)

    self.RootNode:Remove()

    self.RootNode = self:GetCanvas():Add("DHoloBuilderHierarchy_Node");
    self.RootNode:SetRoot(self)
    self.RootNode:SetParentNode(self)
    self.RootNode:Dock(TOP)
    self.RootNode:SetText("")
    self.RootNode:SetExpanded(true, true)
    self.RootNode:DockMargin(0, 4, 0, 0)
end

-- For Override
function PANEL:OnPartVisibilityChanged(node, visible)
end

function PANEL:OnPartLockChanged(node, locked)
end



vgui.Register("DHoloBuilderHierarchy", PANEL, "DTree")