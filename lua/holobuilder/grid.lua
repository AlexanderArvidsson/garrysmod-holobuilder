HoloBuilder.Grid = HoloBuilder.Grid or {
    map = {},
    nodeDimension = Vector(64, 64, 64),
    partIds = {}
}

function HoloBuilder.Grid:ConvertPos(pos)
    local nx = math.floor(pos.x / self.nodeDimension.x)
    local ny = math.floor(pos.y / self.nodeDimension.y)
    local nz = math.floor(pos.z / self.nodeDimension.z)

    return Vector(nx, ny, nz)
end

function HoloBuilder.Grid:GetKey(x, y, z)
    return x .. "," .. y .. "," .. z
end

function HoloBuilder.Grid:AddNode(pos)
    local key = self:GetKey(pos.x, pos.y, pos.z)

    if self.map[key] then return self.map[key] end

    local node = {
        pos = pos,
        partIds = {},
        count = 0
    }

    self.map[key] = node

    return node
end

function HoloBuilder.Grid:AddPart(part)
    local startP = part:GetPos() + part:GetOBBox().min
    local endP = part:GetPos() + part:GetOBBox().max

    local nStart = self:ConvertPos(startP)
    local nEnd = self:ConvertPos(endP)

    if nStart.x == nEnd.x and nStart.y == nEnd.y and nStart.z == nEnd.z then
        local node = self:AddNode(nStart)

        if node.partIds[part:GetID()] then return end

        node.partIds[part:GetID()] = part
        node.count = node.count + 1

        part._gridNode = node
    else
        self.partIds[part:GetID()] = part
        part._gridNode = nil
    end
end

function HoloBuilder.Grid:RemovePart(node, part)
    local id = part:GetID()

    self.partIds[id] = nil
    if node == nil then return end

    node.partIds[id] = nil
    node.count = node.count - 1

    if node.count == 0 then
        self:RemoveNode(node.pos)
    end
end

function HoloBuilder.Grid:RemoveNode(pos)
    local key = self:GetKey(pos.x, pos.y, pos.z)

    self.map[key] = nil
end

function HoloBuilder.Grid:UpdatePart(part)
    if not part.added then return end

    self:RemovePart(part._gridNode, part)
    self:AddPart(part)
end

function HoloBuilder.Grid:GetNodes()
    return self.map
end

function HoloBuilder.Grid:GetNode(pos)
    local key = self:GetKey(pos.x, pos.y, pos.z)

    return self.map[key]
end

function HoloBuilder.Grid:Clear()
    self.map = {}
    self.partIds = {}
end

function HoloBuilder.Grid:TraceParts(partIds)
    local tbl = {}

    for k, v in pairs(partIds) do
        local partTrace = HoloBuilder:TracePart(v)

        if partTrace.hit then
            tbl[#tbl + 1] = v
        end
    end

    return tbl
end

function HoloBuilder.Grid:TraceNodes(pos, dir)
    local hit = false

    for k, node in pairs(self:GetNodes()) do
        local p = node.pos * self.nodeDimension + self.nodeDimension / 2

        local trace = HoloBuilder.TraceSystem.RayAABBoxIntersection(pos, dir, p, self.nodeDimension)
        local traceRes = {
            node    = nil,
            hit     = false,
        }

        node.hit = false

        if trace ~= false then
            traceRes.hit = true

            local tbl = self:TraceParts(node.partIds)
            for k, v in pairs(tbl) do
                v:OnHover()
            end

            node.hit = true

            hit = true
        end
    end

    local tbl = self:TraceParts(self.partIds)
    for k, v in pairs(tbl) do
        v:OnHover()
    end


end

function HoloBuilder.Grid:Draw()
    for k, node in pairs(self:GetNodes()) do
        local pos = node.pos * self.nodeDimension

        local col = Color(0, 255, 255, 100)
        if node.hit then
            col = Color(0, 255, 0, 100)
        end

        render.Util_Render3DAABBBoxWireframe(pos, pos + self.nodeDimension, col, true)
    end
end