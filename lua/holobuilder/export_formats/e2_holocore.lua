local FORMAT = {}
FORMAT.name = "Expression 2 HoloCore"

FORMAT.stringBase =
[[@name %NAME%

#----------#
#- GTable -#
#----------#
local Name = "%NAME%"
local HT = gTable(Name + "_model")
local CT = gTable(Name + "_clip")

HT:clear()
CT:clear()

local HN = 0
local CN = 0
#----------#

#-------------#
#- Variables -#
#-------------#
local Spawnable = 0 # Allows the model chip to spawn the model using HoloCore library
                    # HoloCore MUST be located at path "lib/holoCore.txt"

local Vec = vec()
local Ang = ang()
local Scale = vec(1,1,1)
local Col = vec4(255,255,255,255)

%VARS%
#-------------#

#---------#
#- Parts -#
#---------#
%PARTS%
#---------#

#---------#
#- Clips -#
#---------#
%CLIPS%
#---------#

#--------------#
#- Spawn Code -#
#--------------#

if (Spawnable) {
    interval(100)
    if (first()) {
        #include "lib/holoCore"

        hc_createModel(entity(), "%NAME%")
    }
    hc_generate()
}

#--------------#

]]

FORMAT.stringVectorBase = [[vec(%s,%s,%s)]]
FORMAT.stringAngleBase = [[ang(%s,%s,%s)]]
FORMAT.stringColorBase = [[vec4(%i,%i,%i,%i)]]

FORMAT.stringVar = [[local %s = %s]]

FORMAT.stringPart      = [[HN++, HT[HN, table] = table(%i,%i,%i,%s,%s,%s,%s,%s,%s) #-- %s]]
FORMAT.stringClipPlane = [[CN++, CT[CN, table] = table(%i,%i,%s,%s) #-- %s]]

function escape_magic(s)
    return s:gsub("[%^%$%(%)%%%.%[%]%*%+%-%?]", "%%%1")
end

local function formatOccurences(str, data)
    for k, v in pairs(data) do
        str = string.gsub(str, escape_magic(k), v)
    end

    return str
end



local function Round(n, dec)
    local mult = math.pow(10, dec)
    return math.Round(n * mult) / mult
end

function FORMAT:GetVectorString(vec) return string.format(self.stringVectorBase, Round(vec.x, 4), Round(vec.y, 4), Round(vec.z, 4)) end
function FORMAT:GetAngleString(ang)  return string.format(self.stringAngleBase, Round(ang.p, 4), Round(ang.y, 4), Round(ang.r, 4)) end
function FORMAT:GetColorString(col)  return string.format(self.stringColorBase, col.r, col.g, col.b, col.a) end


function FORMAT:GetVariableString(name, var)
    return string.format(self.stringVar, name, var)
end

function FORMAT:GetPartString(part, vars)
    local partPos = part:GetPos()
    local partAng = part:GetAngles()
    local partCol = part:GetColor()
    local parent = part:GetParent()

    if parent then
        partPos = parent:WorldToLocal(partPos)
        partAng = parent:WorldToLocalAngles(partAng)
    end

    local pos = self:GetVectorString(partPos)
    if partPos:IsZero() then
        pos = "Vec"
    end

    local ang = self:GetAngleString(partAng)
    if partAng:IsZero() then
        ang = "Ang"
    end

    local scale = self:GetVectorString(part:GetScale())
    if part:GetScale() == Vector(1, 1, 1) then
        scale = "Scale"
    end

    local parentId = 0
    if parent then parentId = parent:GetID() end

    local color = self:GetColorString(partCol)
    if partCol.r == 255 and partCol.g == 255 and partCol.b == 255 and partCol.a == 255 then
        color = "Col"
    else
        local varCol = vars.colors[tostring(part:GetColor())]
        if varCol and varCol.count >= 2 then
            color = varCol.name
        end
    end

    local mat = "\"" .. part:GetMaterial() .. "\""
    local varMat = vars.materials[part:GetMaterial()]
    if varMat and varMat.count >= 2 then
        mat = varMat.name
    end

    local model = "\"" .. part:GetModel() .. "\""
    --[[local varModel = vars.models[part:GetModel()]
    if varModel and varModel.count >= 2 then
        model = varModel.name
    end]]

    return string.format(self.stringPart, part:GetID(), parentId, 0, pos, ang, scale, model, mat, color, part:GetName())
end

function FORMAT:GetClipPlaneString(plane)
    

    local str = ""
    for i, v in ipairs(plane:GetClippedParts()) do
        if i ~= 1 then str = str .. "\n" end

        local norm = v:WorldToLocal(v:GetPos() + plane:GetNormal()):GetNormalized()

        local pos = v:WorldToLocal(plane:GetPos())

        local index = (self.clipIndexes[v:GetID()] or 0) + 1
        self.clipIndexes[v:GetID()] = index

        str = str .. string.format(self.stringClipPlane, v:GetID(), index, self:GetVectorString(pos), self:GetVectorString(norm), plane:GetName())

    end

    return str
end

function FORMAT:AddClipPlaneString(str, plane, written)
    if not written[plane:GetID()] then

        if plane:GetParent() then
            str = self:AddClipPlaneString(str, plane:GetParent(), written)
        end

        if plane.type == HB_PART_TYPE_CLIP then
            str = str .. self:GetClipPlaneString(plane) .. "\n"

            written[plane:GetID()] = true
        end
    end

    return str
end

function FORMAT:AddPartString(str, part, vars, written)
    if not written[part:GetID()] then

        if part:GetParent() then
            str = self:AddPartString(str, part:GetParent(), vars, written)
        end

        if part.type == HB_PART_TYPE_NORMAL then
            str = str .. self:GetPartString(part, vars) .. "\n"

            written[part:GetID()] = true
        end
    end

    return str
end



function FORMAT:GetExportedString(name, parts, clips)
    name = name or "model"
    local strVariables = ""
    local strClips = ""
    local strParts = ""

    local vars = {
        --models = {},
        materials = {},
        colors = {},
        matVars = {},
        --modelVars = {},
        colorVars = {}
    }

    -- Variables
    for k, v in pairs(parts) do
        local mat = v:GetMaterial()
        local model = v:GetModel()
        local color = v:GetColor()

        if mat ~= "" then
            if not vars.materials[mat] then
                local id = #vars.matVars + 1
                local name = "Mat" .. id
                vars.matVars[id] = {
                    name = name,
                    var = mat,
                }

                vars.materials[mat] = {
                    name = name,
                    count = 0
                }
            end

            vars.materials[mat].count = vars.materials[mat].count + 1
        end

        --[[if model ~= "" then
            if not vars.models[model] then
                local id = #vars.modelVars + 1
                local name = "Model" .. id
                vars.modelVars[id] = {
                    name = name,
                    var = model,
                }

                vars.models[model] = {
                    name = name,
                    count = 0
                }
            end

            vars.models[model].count = vars.models[model].count + 1
        end]]

        if color.r ~= 255 or color.g ~= 255 or color.b ~= 255 or color.a ~= 255 then
            local key = tostring(color)
            if not vars.colors[key] then
                local id = #vars.colorVars + 1
                local name = "Color" .. id
                vars.colorVars[id] = {
                    name = name,
                    var = color,
                }

                vars.colors[key] = {
                    name = name,
                    count = 0
                }
            end

            vars.colors[key].count = vars.colors[key].count + 1
        end
    end

    PrintTable(vars.colors)
    print("----------")

    for i, v in ipairs(vars.matVars) do
        strVariables = strVariables .. self:GetVariableString(v.name, "\"" .. v.var .. "\"") .. "\n"
    end

    --[[strVariables = strVariables .. "\n"

    for i, v in ipairs(vars.modelVars) do
        strVariables = strVariables .. self:GetVariableString(v.name, "\"" .. v.var .. "\"") .. "\n"
    end]]

    strVariables = strVariables .. "\n"

    for i, v in ipairs(vars.colorVars) do
        strVariables = strVariables .. self:GetVariableString(v.name, self:GetColorString(v.var)) .. "\n"
    end


    -- Parts
    local partsWritten = {}

    for k, v in pairs(parts) do
        strParts = self:AddPartString(strParts, v, vars, partsWritten)
    end

    -- Clips
    local clipsWritten = {}
    self.clipIndexes = {}

    for k, v in pairs(clips) do
        strClips = self:AddClipPlaneString(strClips, v, clipsWritten)
    end

    return formatOccurences(self.stringBase, {
        ["%NAME%"] = name, 
        ["%VARS%"] = strVariables,
        ["%PARTS%"] = strParts,
        ["%CLIPS%"] = strClips
    })

end


HoloBuilder.Exporter:AddFormat("e2_holocore", FORMAT)