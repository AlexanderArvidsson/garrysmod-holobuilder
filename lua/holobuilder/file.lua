HoloBuilder.File = HoloBuilder.File or {}
HoloBuilder.File.format = "holobuilder_v1_1"
HoloBuilder.File.header = "HoloBuilder Format "
HoloBuilder.File.formats = {}

HoloBuilder.File.root = "holobuilder/"

HoloBuilder.File.folders = {}
HoloBuilder.File.folders.userFiles = "files/"
HoloBuilder.File.folders.autoSave  = "autosave/"

function HoloBuilder.File:EnsureFolders()
    if not file.Exists(self.root, "DATA") then
        file.CreateDir(self.root)
    end

    for k, path in pairs(HoloBuilder.File.folders) do

        if not file.Exists(self.root .. path, "DATA") then
            file.CreateDir(self.root .. path)
        end
    end
end


function HoloBuilder.File:AddFormat(name, format)
    self.formats[name] = format
    format.code = name

    format.GetHeader = function()
        return self.header .. format.code
    end

    format.GetVectorFromString = self.GetVectorFromString
    format.GetAngleFromString = self.GetAngleFromString
    format.GetColorFromString = self.GetColorFromString
    format.GetBoolFromString = self.GetBoolFromString

    format.GetVectorString = self.GetVectorString
    format.GetAngleString = self.GetAngleString
    format.GetColorString = self.GetColorString
    format.GetBoolString = self.GetBoolString
end

function HoloBuilder.File:GetFormats()
    return self.formats
end

function HoloBuilder.File:GetFormat(name)
    return self.formats[name]
end

function HoloBuilder.File:GetDefaultFormat()
    return self:GetFormat(self.format)
end


function HoloBuilder.File:GetHeader()
    return self.header
end

function HoloBuilder.File:GetVectorString(v)
    return v.x .. "," .. v.y .. "," .. v.z
end

function HoloBuilder.File:GetAngleString(a)
    return a.p .. "," .. a.y .. "," .. a.r
end

function HoloBuilder.File:GetColorString(c)
    return c.r .. "," .. c.g .. "," .. c.b .. "," .. c.a
end

function HoloBuilder.File:GetBoolString(b)
    return b and "1" or "0"
end

function HoloBuilder.File:GetFormatFromHeader(header)
    return header:sub(string.len(self.header) + 1):Trim()
end

local function StringSeparateComma(str)
    return string.Explode(",", str)
end

function HoloBuilder.File:GetVectorFromString(str)
    local arr = StringSeparateComma(str)

    return Vector(tonumber(arr[1]), tonumber(arr[2]), tonumber(arr[3]))
end

function HoloBuilder.File:GetAngleFromString(str)
    local arr = StringSeparateComma(str)

    return Angle(tonumber(arr[1]), tonumber(arr[2]), tonumber(arr[3]))
end

function HoloBuilder.File:GetColorFromString(str)
    local arr = StringSeparateComma(str)

    return Color(tonumber(arr[1]), tonumber(arr[2]), tonumber(arr[3]), tonumber(arr[4]))
end

function HoloBuilder.File:GetBoolFromString(str)
    return str:Trim() == "1" or str:Trim() == "true"
end



function HoloBuilder.File:Save(path, parts, clips, subPath)
    subPath = subPath or self.folders.userFiles

    if not string.EndsWith(path, ".txt") then
        path = path .. ".txt"
    end

    self:EnsureFolders()

    local format = self:GetFormat(self.format)
    if not format then
        print("Unknown format")
        return
    end

    print("Saving format " .. format.name)
    
    format:Save(self.root .. subPath .. path, parts, clips)
end


function HoloBuilder.File:Load(path, subPath)
    subPath = subPath or self.folders.userFiles

    local str = file.Read(self.root .. subPath .. path)

    if not str then return end

    return self:LoadString(str)
end

function HoloBuilder.File:LoadString(str)
    str = str:gsub(";", "\n")
    local arr = string.Explode("\n", str)

    local header = arr[1]
    if not header then return end

    local formatStr = self:GetFormatFromHeader(header:sub(3))
    if formatStr == "1" then
        formatStr = "holobuilder_v1"
    end

    local format = self:GetFormat(formatStr)
    if not format then
        print("Unknown format")
        return
    end

    print("Loading format " .. format.name)
    
    return format:Load(arr)
end

local files, _ = file.Find("holobuilder/formats/*", "LUA")

for k, fName in ipairs(files) do
    include("holobuilder/formats/" .. fName)
end