HoloBuilder.Importer = HoloBuilder.Importer or {}

function HoloBuilder.Importer:Pastebin(link)
    link = link:Trim()
    
    local index = string.find(link, "/[^/]*$")
    local key = link

    if index then
        key = string.sub(link, index + 1)
    end

    local newLink = "http://pastebin.com/raw.php?i=" .. key

    http.Fetch(newLink, function(body, len, headers, code)
        local parts, clips = HoloBuilder.File:LoadString(body)
        if not parts or not clips then
            return
        end

        HoloBuilder:ClearParts()

        local count = 0
        for k, v in pairs(parts) do
            HoloBuilder:AddPart(v, v:GetID())
            count = count + 1
        end

        for k, v in pairs(clips) do
            HoloBuilder:AddClipPlane(v, v:GetID())
        end
    end)
end

function HoloBuilder.Importer:Text(text)
    local parts, clips = HoloBuilder.File:LoadString(text)
    if not parts or not clips then
        return
    end

    HoloBuilder:ClearParts()

    local count = 0
    for k, v in pairs(parts) do
        HoloBuilder:AddPart(v, v:GetID())
        count = count + 1
    end

    for k, v in pairs(clips) do
        HoloBuilder:AddClipPlane(v, v:GetID())
    end
end


--HoloBuilder.Importer:Pastebin("http://pastebin.com/3x0kz8jP")