HB_TOOL = HoloBuilder.Tools.New("rotate")

HB_TOOL.position = 2
HB_TOOL.tooltip = "Rotation Tool"
HB_TOOL.icon = "holobuilder/rotate.png"

local axes = {
    X = 1,
    Y = 2,
    Z = 3
}

HB_TOOL.fidelity          = 16
HB_TOOL.axis              = nil
HB_TOOL.hoverAxis         = nil

HB_TOOL.clickAngle                = 0
HB_TOOL.clickAngleSnapped         = 0
HB_TOOL.dragDiff                  = 0
HB_TOOL.dragDiffSnapped           = 0
HB_TOOL.isRotating                = false

function HB_TOOL:GetRadius()
    local part = self:GetSelectedPart()
    if not part then return 0 end

    local factor = part:GetPos():Distance(HoloBuilder:GetViewPos()) / 50

    return math.min(20 * factor, 20)
end

function HB_TOOL:GetAngleAroundPartAxis(pos, axis)
    local part = self:GetSelectedPart()
    if not part then return 0 end

    local localP, _ = WorldToLocal(pos, Angle(), part:LocalToWorld(part:GetOrigin()), part:GetAngles())

    local dir = localP:GetNormalized()

    if axis == axes.X then
        return math.deg(math.atan2(dir.y, -dir.z))

    elseif axis == axes.Y then
        return math.deg(math.atan2(dir.x, -dir.z))

    elseif axis == axes.Z then
        return math.deg(math.atan2(dir.x, -dir.y))
    end

    return 0
end

function HB_TOOL:OnMousePressed(mouseCode, mPos)
    if mouseCode ~= MOUSE_LEFT then return end

    local part = self:GetSelectedPart()
    if not part then return end

    local axis, trace = self:GetHoveredAxis()
    self.axis = axis

    local pos = part:LocalToWorld(part:GetOrigin())
    if trace then
        self.clickAngle = self:GetAngleAroundPartAxis(trace, axis)
        self.clickAngleSnapped = self.clickAngle
        self.dragDiff = 0

        self.partClickAngle = part:GetRenderAngles()
        self.isRotating = true
    end
end

function HB_TOOL:OnMouseReleased(mouseCode, mPos)
    if mouseCode ~= MOUSE_LEFT then return end

    if not self.isRotating then return end

    self.axis = nil
    self.isRotating = false

    local part = self:GetSelectedPart()
    if part then
        local ang = part:GetRenderAngles()
        local pos = part:GetRenderPos()
        local parent = part:GetParent()

        if parent then
            pos = parent:WorldToLocal(pos)
            ang = parent:WorldToLocalAngles(ang)
        end
        part:SetAngles(ang)
        part:SetPos(pos)
    end
end


function HB_TOOL:OnMouseDragged(mouseCode, mPos, xDelta, yDelta)
    if mouseCode ~= MOUSE_LEFT then return end

    local part = self:GetSelectedPart()
    if not part then return end

    local axis, trace = self:GetHoveredAxis(self.axis, true)
    local pos = part:LocalToWorld(part:GetOrigin())

    if trace then
        local newAng = self:GetAngleAroundPartAxis(trace, self.axis)

        local diff = (newAng - self.clickAngle + 180) % 360 - 180

        self.dragDiff = diff
    end
end

function HB_TOOL:IsHovered()
    return self:GetHoveredAxis() ~= nil
end

function HB_TOOL:GetHoveredAxis(forceAxis, noRadiusLimit)
    local rad = self:GetRadius()

    local part = self:GetSelectedPart()
    if part then
        local pos = self:GetCenter()

        local closest = nil
        local closestDist = nil
        for k, v in pairs(axes) do
            if forceAxis and forceAxis ~= v then continue end

            local norm = Vector(0, 0, 0)

            if v == axes.X then     norm = part:Forward()
            elseif v == axes.Y then norm = part:Right()
            elseif v == axes.Z then norm = part:Up() end

            local mPos = HoloBuilder:GetMousePos()
            local dir = HoloBuilder:GetScreenDir(mPos.x, mPos.y)

            local trace = HoloBuilder.TraceSystem.RayPlaneIntersection(
                HoloBuilder:GetViewPos(), dir, pos, norm)

            if trace == false then
                trace = HoloBuilder.TraceSystem.RayPlaneIntersection(
                    HoloBuilder:GetViewPos(),dir, pos, -norm)
            end

            if trace ~= false then
                if (trace:Distance(pos) <= rad and trace:Distance(pos) >= rad * 0.9) or noRadiusLimit then
                    local dist = trace:Distance(HoloBuilder:GetViewPos())
                    if not closest or dist < closestDist then
                        closest = { axis = v, pos = trace }
                        closestDist = trace:Distance(HoloBuilder:GetViewPos())
                    end
                end
            end
        end

        if closest then
            return closest.axis, closest.pos
        end
    end
end

local function SnapAngle(axis, ang, steps)
    if axis == axes.Y then
        ang = Angle(math.Round(ang.p / steps) * steps, ang.y, ang.r)

    elseif axis == axes.Z then
        ang = Angle(ang.p, math.Round(ang.y / steps) * steps, ang.r)

    elseif axis == axes.X then
        ang = Angle(ang.p, ang.y, math.Round(ang.r / steps) * steps)
    end
    
    return ang
end

function HB_TOOL:GetCenter()
    local part = self:GetSelectedPart()
    if not part then return end

    local pos = part:GetPos()
    if part:IsAngleLocal() then
        pos = part:LocalToWorld(part:GetOrigin(), true)
    end

    return pos
end

function HB_TOOL:Think()
    local part = self:GetSelectedPart()

    if self.isRotating and part then
        self.dragDiffSnapped = self.dragDiff
        self.clickAngleSnapped = self.clickAngle

        local newAng = Angle(self.partClickAngle.p, self.partClickAngle.y, self.partClickAngle.r)

        if input.IsKeyDown(KEY_LSHIFT) then
            self.dragDiffSnapped   = math.Round(self.dragDiff / 45) * 45
            self.clickAngleSnapped = math.Round(self.clickAngle / 45) * 45
            --newAng = Angle(math.Round(newAng.p / 45) * 45, math.Round(newAng.y / 45) * 45, math.Round(newAng.r / 45) * 45)

        elseif input.IsKeyDown( KEY_LCONTROL ) then
            self.dragDiffSnapped   = math.Round(self.dragDiff / 5) * 5
            self.clickAngleSnapped = math.Round(self.clickAngle / 5) * 5
            --newAng = Angle(math.Round(newAng.p / 5) * 5, math.Round(newAng.y / 5) * 5, math.Round(newAng.r / 5) * 5)
        end

        if self.axis == axes.X then
            newAng:RotateAroundAxis(part:Forward(), self.dragDiffSnapped)

        elseif self.axis == axes.Y then
            newAng:RotateAroundAxis(part:Right(), self.dragDiffSnapped)

        elseif self.axis == axes.Z then
            newAng:RotateAroundAxis(part:Up(), self.dragDiffSnapped)
        end

        if part:IsAngleLocal() then
            local offset = -part:GetOrigin()
            offset:Rotate(newAng)

            part:SetRenderPos(part:LocalToWorld(part:GetOrigin()) + offset)
        end

        part:SetRenderAngles(newAng)
    end
end

function HB_TOOL:Draw()
    local part = self:GetSelectedPart()
    if not part then return end

    local pos = self:GetCenter()
    local ang = part:GetAngles()

    local rad = self:GetRadius()
    local alpha = 255

    local hoverColor = Color(255, 150, 0, alpha)

    local xAng = part:LocalToWorldAngles(Angle(90, 0, 0))
    local yAng = part:LocalToWorldAngles(Angle(90, 0, 90))
    local zAng = part:LocalToWorldAngles(Angle(0, 0, 0))


    self.hoverAxis = self:GetHoveredAxis()

    local color = Color(255, 0, 0, alpha)
    if self.axis == axes.X or (self.hoverAxis == axes.X and not self.axis) then
        render.Util_Render3DCircle(pos, xAng, rad, self.fidelity, Color(50, 50, 50, 200, alpha), false)
        color = hoverColor
    end
    render.Util_Render3DCircleOutlined(pos, xAng, rad, self.fidelity, color, false)


    local color = Color(0, 255, 0, alpha)
    if self.axis == axes.Y or (self.hoverAxis == axes.Y and not self.axis) then
        render.Util_Render3DCircle(pos, yAng, rad, self.fidelity, Color(50, 50, 50, 200, alpha), false)
        color = hoverColor
    end
    render.Util_Render3DCircleOutlined(pos, yAng, rad, self.fidelity, color, false)


    local color = Color(0, 0, 255, alpha)
    if self.axis == axes.Z or (self.hoverAxis == axes.Z and not self.axis) then
        render.Util_Render3DCircle(pos, zAng, rad, self.fidelity, Color(50, 50, 50, 200, alpha), false)
        color = hoverColor
    end
    render.Util_Render3DCircleOutlined(pos, zAng, rad, self.fidelity, color, false)

    if self.isRotating then
        local clickAng  = self.clickAngleSnapped
        local dragAng   = self.dragDiffSnapped
        if self.axis == axes.X then
            render.DrawLine(pos, pos + part:LocalToWorldAngles(Angle(90 - clickAng, 90, 0)):Forward() * rad, Color(255, 150, 0, alpha), false)
            render.DrawLine(pos, pos + part:LocalToWorldAngles(Angle(90 - clickAng - dragAng, 90, 0)):Forward() * rad, Color(255, 150, 0, alpha), false)
            render.Util_Render3DCircleOutlined(pos, xAng, rad, self.fidelity,
                Color(255, 150, 0, alpha), false, -clickAng, -clickAng - dragAng)
        
        elseif self.axis == axes.Y then
            render.DrawLine(pos, pos + part:LocalToWorldAngles(Angle(90 - clickAng, 0, 0)):Forward() * rad, Color(255, 150, 0, alpha), false)
            render.DrawLine(pos, pos + part:LocalToWorldAngles(Angle(90 - clickAng - dragAng, 0, 90)):Forward() * rad, Color(255, 150, 0, alpha), false)
            render.Util_Render3DCircleOutlined(pos, yAng, rad, self.fidelity,
                Color(255, 150, 0, alpha), false, -clickAng, -clickAng - dragAng)
        
        elseif self.axis == axes.Z then
            render.DrawLine(pos, pos + part:LocalToWorldAngles(Angle(0, clickAng - 90, 0)):Forward() * rad, Color(255, 150, 0, alpha), false)
            render.DrawLine(pos, pos + part:LocalToWorldAngles(Angle(0, clickAng + dragAng - 90, 0)):Forward() * rad, Color(255, 150, 0, alpha), false)
            render.Util_Render3DCircleOutlined(pos, zAng, rad, self.fidelity,
                Color(255, 150, 0, alpha), false, -clickAng + 90, -clickAng - dragAng + 90)
        end
    end
end

function HB_TOOL:Draw2D()
    local part = self:GetSelectedPart()
    if not part then return end

    if not part:ShouldShowOrigin() then
        part:DrawOrigin(20)
    end
end