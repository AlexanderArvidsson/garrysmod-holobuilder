HB_TOOL = HoloBuilder.Tools.New("scale")

HB_TOOL.position = 3
HB_TOOL.tooltip = "Scale Tool"
HB_TOOL.icon = "holobuilder/scale.png"

HB_TOOL.axis      = nil
HB_TOOL.hoverAxis = nil

HB_TOOL.scale        = Vector()
HB_TOOL.scaleSnapped = HB_TOOL.scale
HB_TOOL.isScaling    = false

local axes = {
    FORWARD = 1,
    BACK    = 2,
    RIGHT   = 3,
    LEFT    = 4,
    UP      = 5,
    DOWN    = 6
}


function HB_TOOL:GetRadius()
    return 0
end


function HB_TOOL:OnMousePressed(mouseCode, mPos)
    if mouseCode ~= MOUSE_LEFT then return end

    local part = self:GetSelectedPart()
    if not part then return end

    self.axis = self.hoverAxis

    if self.axis ~= nil then
        self.isScaling = true
        self.scale = part:GetRenderScale()
        self.scaleSnapped = self.scale

        self.scaleStart = self.scale
    end
end

function HB_TOOL:OnMouseReleased(mouseCode, mPos)
    if mouseCode ~= MOUSE_LEFT then return end

    local part = self:GetSelectedPart()
    if not part then return end

    if not self.isScaling then return end

    self.axis = nil
    self.isScaling = false

    local newOrigin = part:LocalToWorld(part:GetOrigin())

    local parent = part:GetParent()
    local pos = part:GetRenderPos()

    if parent then
        pos = parent:WorldToLocal(pos)
    end

    part:SetScale(part:GetRenderScale())
    part:SetPos(pos)

    part:SetOrigin(part:WorldToLocal(newOrigin))
end

function HB_TOOL:OnMouseDragged(mouseCode, mPos, xDelta, yDelta)
    if mouseCode ~= MOUSE_LEFT then return end
    
    local part = self:GetSelectedPart()
    if not part then return end

    local norm = self:GetAxisNormal(self.axis)
    if not norm then return end

    local facePos = part:GetCenter() + norm * part:GetOBBox().size / 2
    local scrFaceX, scrFaceY = HoloBuilder:VectorToScreen(facePos)
    local scrX, scrY = HoloBuilder:VectorToScreen(part:GetCenter())

    local scrDir = Vector(scrFaceX - scrX, scrFaceY - scrY, 0):GetNormalized()
    
    local mDir = Vector(xDelta, yDelta, 0)

    local dot = (scrDir:Dot(mDir) / 35) * HoloBuilder:GetViewZoomFactor()

    local div = math.Clamp(part:GetOBBox().size:Length() / 20, 1, 10)
    local scaleNorm = self:GetAxisNormal(self.axis, true)
    scaleNorm = Vector(math.abs(scaleNorm.x), math.abs(scaleNorm.y), math.abs(scaleNorm.z))
    self.scale = self.scale + ((scaleNorm * dot) / div)

    if self.uniform then
        local s = 0
        if self.axis == axes.FORWARD or self.axis == axes.BACK then
            s = self.scale.x
        elseif self.axis == axes.RIGHT or self.axis == axes.LEFT then
            s = self.scale.y
        elseif self.axis == axes.UP or self.axis == axes.DOWN then
            s = self.scale.z
        end

        self.scale = Vector(s, s, s)
    end
end

function HB_TOOL:GetAxisNormal(axis, isLocal)
    local part = self:GetSelectedPart()
    if not part then return end

    if isLocal then
        if axis == axes.FORWARD then   return Vector(1, 0, 0)
        elseif axis == axes.BACK then  return Vector(-1, 0, 0)
        elseif axis == axes.RIGHT then return Vector(0, 1, 0)
        elseif axis == axes.LEFT then  return Vector(0, -1, 0)
        elseif axis == axes.UP then    return Vector(0, 0, 1)
        elseif axis == axes.DOWN then  return Vector(0, 0, -1) end
    else
        if axis == axes.FORWARD then   return part:Forward()
        elseif axis == axes.BACK then  return -part:Forward()
        elseif axis == axes.RIGHT then return part:Right()
        elseif axis == axes.LEFT then  return -part:Right()
        elseif axis == axes.UP then    return part:Up()
        elseif axis == axes.DOWN then  return -part:Up() end
    end
end

function HB_TOOL:IsHovered()
    if not self:GetSelectedPart() then return false end

    return self.hoverAxis ~= nil
end

local function SnapVector(axis, vec, steps)
    if axis == axes.FORWARD or axis == axes.BACK then
        vec = Vector(math.Round(vec.x / steps) * steps, vec.y, vec.z)

    elseif axis == axes.RIGHT or axis == axes.LEFT then
        vec = Vector(vec.x,  math.Round(vec.y / steps) * steps, vec.z)

    elseif axis == axes.UP or axis == axes.DOWN then
        vec = Vector(vec.x, vec.y, math.Round(vec.z / steps) * steps)
    end

    return vec
end

function HB_TOOL:Think()
    local part = self:GetSelectedPart()
    if not part then return end

    self.hoverAxis = nil

    local mPos = HoloBuilder:GetMousePos()

    local center = part:GetCenter() + part:WorldToLocal(part:GetRenderPos())
    local dir = HoloBuilder:GetScreenDir(mPos.x, mPos.y)
    local trace, norm = HoloBuilder.TraceSystem.RayOBBoxIntersection(HoloBuilder:GetViewPos(), dir, center, part:GetOBBox().size, part:GetAngles())
    if trace ~= false then
        render.Util_RenderCrossedLines(trace, 10, Color(255, 0, 0))
        if norm ==      part:Forward() then self.hoverAxis = axes.FORWARD
        elseif norm == -part:Forward() then self.hoverAxis = axes.BACK
        elseif norm ==  part:Right() then   self.hoverAxis = axes.RIGHT
        elseif norm == -part:Right() then   self.hoverAxis = axes.LEFT
        elseif norm ==  part:Up() then      self.hoverAxis = axes.UP
        elseif norm == -part:Up() then      self.hoverAxis = axes.DOWN end
    end

    self.uniform = input.IsKeyDown(KEY_LALT)

    if self.isScaling then
        self.scaleSnapped = self.scale

        local s = self.scale
        if input.IsKeyDown(KEY_LSHIFT) then
            local inc = 0.5
            self.scaleSnapped = SnapVector(self.axis, s, inc)

        elseif input.IsKeyDown(KEY_LCONTROL) then
            local inc = 0.2
            self.scaleSnapped = SnapVector(self.axis, s, inc)
        end

        local oldScale = Vector()
        local newScale = Vector()
        oldScale:Set(self.scaleStart)
        newScale:Set(self.scaleSnapped)

        part:SetRenderScale(self.scaleSnapped)

        local fract = Vector(
            math.max(newScale.x / oldScale.x, 0),
            math.max(newScale.y / oldScale.y, 0),
            math.max(newScale.z / oldScale.z, 0))

        part:SetRenderPos(part:LocalToWorld(part:GetOrigin() - part:GetOrigin() * fract))
        part:SetRenderOrigin(part:GetOrigin() * fract)
    end
end

function HB_TOOL:Draw()
    local part = self:GetSelectedPart()
    if not part then return end

    local pos = part:GetPos()

    local alpha = 1

    render.SetMaterial(render.Util_MatWhite)
    local obb = part:GetOBBox()
    local normalColor = Color(255, 200, 0, alpha * 40)
    local hoverColor = Color(255, 200, 0, alpha * 80)
    local color = normalColor

    local min = obb.min + part:WorldToLocal(part:GetRenderPos())
    local max = obb.max + part:WorldToLocal(part:GetRenderPos())

    local axis = self.hoverAxis
    if self.axis then axis = self.axis end

    if axis == axes.UP or self.uniform then color = hoverColor end
    render.DrawQuad(
        part:LocalToWorld(Vector(min.x, max.y, max.z)),
        part:LocalToWorld(Vector(max.x, max.y, max.z)),
        part:LocalToWorld(Vector(max.x, min.y, max.z)),
        part:LocalToWorld(Vector(min.x, min.y, max.z)),
        color
    )
    color = normalColor

    if axis == axes.DOWN or self.uniform then color = hoverColor end
    render.DrawQuad(
        part:LocalToWorld(Vector(min.x, min.y, min.z)),
        part:LocalToWorld(Vector(max.x, min.y, min.z)),
        part:LocalToWorld(Vector(max.x, max.y, min.z)),
        part:LocalToWorld(Vector(min.x, max.y, min.z)),
        color
    )
    color = normalColor

    if axis == axes.BACK or self.uniform then color = hoverColor end
    render.DrawQuad(
        part:LocalToWorld(Vector(min.x, min.y, min.z)),
        part:LocalToWorld(Vector(min.x, max.y, min.z)),
        part:LocalToWorld(Vector(min.x, max.y, max.z)),
        part:LocalToWorld(Vector(min.x, min.y, max.z)),
        color
    )
    color = normalColor

    if axis == axes.FORWARD or self.uniform then color = hoverColor end
    render.DrawQuad(
        part:LocalToWorld(Vector(max.x, min.y, max.z)),
        part:LocalToWorld(Vector(max.x, max.y, max.z)),
        part:LocalToWorld(Vector(max.x, max.y, min.z)),
        part:LocalToWorld(Vector(max.x, min.y, min.z)),
        color
    )
    color = normalColor

    if axis == axes.RIGHT or self.uniform then color = hoverColor end
    render.DrawQuad(
        part:LocalToWorld(Vector(min.x, min.y, max.z)),
        part:LocalToWorld(Vector(max.x, min.y, max.z)),
        part:LocalToWorld(Vector(max.x, min.y, min.z)),
        part:LocalToWorld(Vector(min.x, min.y, min.z)),
        color
    )
    color = normalColor

    if axis == axes.LEFT or self.uniform then color = hoverColor end
    render.DrawQuad(
        part:LocalToWorld(Vector(min.x, max.y, min.z)),
        part:LocalToWorld(Vector(max.x, max.y, min.z)),
        part:LocalToWorld(Vector(max.x, max.y, max.z)),
        part:LocalToWorld(Vector(min.x, max.y, max.z)),
        color
    )

    render.SetColorModulation(1, 1, 1)
end

function HB_TOOL:Draw2D()
    local part = self:GetSelectedPart()
    if not part then return end
    
    if not part:ShouldShowOrigin() then
        part:DrawOrigin(20)
    end
end