HB_TOOL = HoloBuilder.Tools.New("translate")

HB_TOOL.position = 1
HB_TOOL.tooltip = "Translation Tool"
HB_TOOL.icon = "holobuilder/move.png"

local matCross  = Material( "spaceage/editor/cross" )
local matArrow = Material( "holobuilder/arrowup_solid" )

local directions = {
    Vector(1, 0, 0),
    Vector(0, 1, 0), 
    Vector(0, 0, 1), 
}

function HB_TOOL:GetArrowRadius()
    return 12
end

function HB_TOOL:GetRadius()
    local part = self:GetSelectedPart()
    if not part then return 0 end

    local factor = part:GetPos():Distance(HoloBuilder:GetViewPos()) / 50

    return math.min(20 * factor, 20)
end

function HB_TOOL:GetArrowPosition(dir)
    local part = self:GetSelectedPart()
    if not part then return false end


    if part:IsPosLocal() then
        return part:LocalToWorld(dir * self:GetRadius() + part:GetOrigin(), true)
    else
        return part:LocalToWorld(dir * self:GetRadius(), true)
    end

    
end

function HB_TOOL:GetHoveredArrow()
    local part = self:GetSelectedPart()
    if not part then return end

    local closest = nil
    local closestDist = nil

    local arrowRad = self:GetArrowRadius()

    for i = 1, #directions do
        local dir = directions[ i ]

        local pos = self:GetArrowPosition(dir)
        local x, y = HoloBuilder:VectorToScreen(pos)
        
        local mPos = HoloBuilder:GetMousePos()

        if mPos:Distance(Vector(x, y, 0)) <= arrowRad + 4 then
            local dist = pos:Distance(HoloBuilder:GetViewPos())
            if not closest or dist < closestDist then
                closest = i
                closestDist = dist
            end
        end
    end

    return closest
end

function HB_TOOL:OnMousePressed(mouseCode, mPos)
    if mouseCode ~= MOUSE_LEFT then return end

    local arrow = self:GetHoveredArrow()

    if arrow then
        self.hoveredArrow = arrow
        self.dot = 0
        self.snappedDot = 0
        self.isDuplicate = false
    end
end

function HB_TOOL:OnMouseReleased(mouseCode, mPos)
    if mouseCode ~= MOUSE_LEFT then return end
    if not self.hoveredArrow then return end

    self.hoveredArrow = nil

    local part = self:GetSelectedPart()
    if not part then return end

    local parent = part:GetParent()
    local pos = part:GetRenderPos()


    if parent then
        pos = parent:WorldToLocal(pos)
    end

    part:SetPos(pos)

    self.isDuplicate = false
end


function HB_TOOL:OnMouseDragged(mouseCode, mPos, xDelta, yDelta)
    if mouseCode ~= MOUSE_LEFT then return end

    if self.hoveredArrow then
        local zoom = HoloBuilder:GetViewZoomFactor()

        local part = self:GetSelectedPart()
        if not part then return end

        local pos = part:GetRenderPos()
        if part:IsPosLocal() then
            pos = part:LocalToWorld(part:GetOrigin(), true)
        end

        local dir = directions[self.hoveredArrow]
        local pX, pY = HoloBuilder:VectorToScreen(pos)
        local pPos = Vector(pX, pY, 0)

        local arrowPos = self:GetArrowPosition(dir)
        local x, y = HoloBuilder:VectorToScreen(arrowPos)
        local scrAPos = Vector(x, y, 0)

        local scrDir = (scrAPos - pPos):GetNormalized()

        local delta = Vector(xDelta, yDelta, 0)
        self.dot = self.dot + (scrDir:Dot(delta) / 6) * zoom
    end
end

function HB_TOOL:IsHovered()
    if not self:GetSelectedPart() then return false end

    local arrow = self:GetHoveredArrow()
    return arrow ~= nil
end

local function SnapVector(dirid, vec, steps)
    if dirid == 1 then
        vec = Vector(math.Round(vec.x / steps) * steps, vec.y, vec.z)

    elseif dirid == 2 then
        vec = Vector(vec.x, math.Round(vec.y / steps) * steps, vec.z)

    elseif dirid == 3 then
        vec = Vector(vec.x, vec.y, math.Round(vec.z / steps) * steps)
    end

    return vec
end

function HB_TOOL:Think()
    if self.hoveredArrow then
        local part = self:GetSelectedPart()
        if not part then return end

        local dir = directions[self.hoveredArrow]
        
        local pos = part:LocalToWorld(dir * self.dot)

        if input.IsKeyDown(KEY_LSHIFT) then
            pos = SnapVector(self.hoveredArrow, pos, 1)

        elseif input.IsKeyDown(KEY_LCONTROL) then
            pos = SnapVector(self.hoveredArrow, pos, 5)

        end

        if input.IsKeyDown(KEY_LALT) and not self.isDuplicate then
            part:SetRenderPos(part:GetPos())

            local newPart = part:Copy()

            if string.sub(part:GetName(), 1, 5) ~= "copy_" then
                newPart:SetName("copy_" .. part:GetName())
            end

            HoloBuilder:SetGhostPart(newPart)
            HoloBuilder:SelectPart(newPart)

            self.isDuplicate = true

            return
        end


        part:SetRenderPos(pos)
    end
end

function HB_TOOL:Draw2D()
    local part = self:GetSelectedPart()
    if not part then return end

    local pos = part:GetRenderPos()
    if part:IsPosLocal() then
        pos = part:LocalToWorld(part:GetOrigin(), true)
    end

    local pX, pY = HoloBuilder:VectorToScreen(pos)
    local pPos = Vector(pX, pY, 0)

    local arrowRad = self:GetArrowRadius()

    for i = 1, #directions do
        local dir = directions[i]

        local pos = self:GetArrowPosition(dir)
        local x, y = HoloBuilder:VectorToScreen(pos)

        local scrDir = (Vector(x, y, 0) - pPos):GetNormalized()
        local ang = -math.deg(math.atan2(scrDir.x, -scrDir.y))

        surface.SetDrawColor(Color(dir.x * 255, dir.y * 255, dir.z * 255))
        surface.Util_DrawLine( x, y, pX, pY, 2 )

        surface.SetMaterial(matArrow)
        surface.DrawTexturedRectRotated(x, y, arrowRad * 2, arrowRad * 2, ang)
    end
end