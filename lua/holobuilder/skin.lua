HoloBuilder.Skin = {}

HoloBuilder.Skin.colorPrimary = Color(100, 100, 100)
HoloBuilder.Skin.colorDark = Color(80, 80, 80)
HoloBuilder.Skin.colorDarker = Color(60, 60, 60)
HoloBuilder.Skin.colorLight = Color(120, 120, 120)
HoloBuilder.Skin.colorLighter = Color(140, 140, 140)
HoloBuilder.Skin.colorAccent = Color(255, 150, 0)
HoloBuilder.Skin.colorText = Color(255, 255, 255)

function HoloBuilder.Skin.PaintPanel(pnl, bgColor, cornerRadius)
    bgColor = bgColor or HoloBuilder.Skin.colorPrimary
    
    pnl.Paint = function(pnl, w, h)
        if cornerRadius then
            draw.RoundedBox(cornerRadius, 0, 0, w, h, bgColor)
        else
            surface.SetDrawColor(bgColor)
            surface.DrawRect(0, 0, w, h)
        end
    end
end

function HoloBuilder.Skin.PaintTextEntry(pnl, bgColor, bgDisabled)
    bgColor = bgColor or HoloBuilder.Skin.colorPrimary
    bgDisabled = bgDisabled or HoloBuilder.Skin.colorDark
    pnl:SetTextColor(HoloBuilder.Skin.colorText)
    pnl:SetCursorColor(HoloBuilder.Skin.colorText)
    pnl:SetHighlightColor(HoloBuilder.Skin.colorAccent)

    pnl.Paint = function(pnl, w, h)
        if pnl.m_bBackground then
            local borderColor = bgColor
            if pnl:GetDisabled() then
                borderColor = bgDisabled
            elseif pnl:HasFocus() then
                borderColor = HoloBuilder.Skin.colorAccent
            end

            local thickness = 2
            draw.RoundedBox(6, 0, 0, w, h, borderColor)
            draw.RoundedBox(4, thickness, thickness, w - thickness * 2, h - thickness * 2, bgColor)
        end

        pnl:DrawTextEntryText(pnl.m_colText, pnl.m_colHighlight, pnl.m_colCursor)
    end
end

function HoloBuilder.Skin.PaintComboBox(pnl)
    pnl:SetTextColor(HoloBuilder.Skin.colorText)

    pnl.Paint = function(pnl, w, h)
        if pnl.m_bBackground then
            local borderColor = HoloBuilder.Skin.colorDark
            if pnl:GetDisabled() then
                borderColor = HoloBuilder.Skin.colorDark
            elseif pnl:IsMenuOpen() then
                borderColor = HoloBuilder.Skin.colorAccent
            end

            local thickness = 2
            draw.RoundedBox(6, 0, 0, w, h, borderColor)
            draw.RoundedBox(4, thickness, thickness, w - thickness * 2, h - thickness * 2, HoloBuilder.Skin.colorDark)
        end
    end

    pnl.DropButton.Paint = function(arrow, w, h)
        local triangle = {
            { x = 4, y = 5 },
            { x = w - 4, y = 5 },
            { x = w / 2, y = h - 5 }
        }

        if pnl:GetDisabled() then
            surface.SetDrawColor(HoloBuilder.Skin.colorDarker)
        elseif pnl.Depressed || pnl:IsMenuOpen() then
            surface.SetDrawColor(HoloBuilder.Skin.colorLighter)
        elseif pnl.Hovered then
            surface.SetDrawColor(HoloBuilder.Skin.colorLight)
        else
            surface.SetDrawColor(HoloBuilder.Skin.colorPrimary)
        end

        draw.NoTexture()
        surface.DrawPoly(triangle)
    end
end

function HoloBuilder.Skin.PaintButton(pnl, bgColor)
    bgColor = bgColor or HoloBuilder.Skin.colorPrimary
    pnl:SetTextColor(HoloBuilder.Skin.colorText)

    pnl.Paint = function(pnl, w, h)
        if pnl.Depressed then
            surface.SetDrawColor(HoloBuilder.Skin.colorLight)
        else
            surface.SetDrawColor(bgColor)
        end
        surface.DrawRect(0, 0, w, h)

        local outlineColor = bgColor
        if pnl:GetDisabled() then
            outlineColor = HoloBuilder.Skin.colorDarker
        end

        surface.SetDrawColor(outlineColor)
        surface.Util_DrawOutlinedRect(0, 0, w, h, 2)
    end
end

function HoloBuilder.Skin.PaintTree(pnl, bgColor)
    bgColor = bgColor or HoloBuilder.Skin.colorPrimary

    pnl.Paint = function(pnl, w, h)
        draw.RoundedBox(4, 0, 0, w, h, bgColor)
    end
end

function HoloBuilder.Skin.PaintTreeNode(pnl)
    pnl.Label:SetTextColor(HoloBuilder.Skin.colorText)

    pnl.Label.Paint = function(pnl, w, h)
        if not pnl.m_bSelected then return end
        
        -- Don't worry this isn't working out the size every render
        -- it just gets the cached value from inside the Label
        local w, _ = pnl:GetTextSize() 
        local x = 38

        if pnl:GetParent():GetVisibilityButtonVisible() then
            x = x + 20
        end

        if pnl:GetParent():GetLockButtonVisible() then
            x = x + 20
        end

        draw.RoundedBox(6, x, 0, w + 6, h, HoloBuilder.Skin.colorAccent)
    end
end

function HoloBuilder.Skin.PaintList(pnl, bgColor)
    bgColor = bgColor or HoloBuilder.Skin.colorPrimary

    pnl.Paint = function(pnl, w, h)
        draw.RoundedBox(4, 0, 0, w, h, bgColor)
    end

    for i = 1, #pnl.Columns do
        local column = pnl.Columns[i]

        column.Header.Paint = function(pnl, w, h)
            if pnl.Depressed then
                surface.SetDrawColor(HoloBuilder.Skin.colorLight)
            else
                surface.SetDrawColor(HoloBuilder.Skin.colorPrimary)
            end
            surface.DrawRect(0, 0, w, h)

            surface.SetDrawColor(HoloBuilder.Skin.colorAccent)
            surface.DrawRect(0, h - 1, w, 1)
        end

        column.Header:SetTextColor(HoloBuilder.Skin.colorText)
    end
end

function HoloBuilder.Skin.PaintListViewLine(pnl)
    pnl.Paint = function(pnl, w, h)
        if pnl:IsSelected() then
            surface.SetDrawColor(HoloBuilder.Skin.colorAccent)
         
        elseif pnl.Hovered then
            surface.SetDrawColor(HoloBuilder.Skin.colorLight)
         
        elseif pnl.m_bAlt then
            surface.SetDrawColor(HoloBuilder.Skin.colorDark)
        else
            surface.SetDrawColor(HoloBuilder.Skin.colorPrimary)
        end

        surface.DrawRect(0, 0, w, h)
    end

    for i = 1, #pnl.Columns do
        local label = pnl.Columns[i]

        label.UpdateColours = function(pnl, skin)
            return pnl:SetTextStyleColor(HoloBuilder.Skin.colorText)
        end
    end
end

function HoloBuilder.Skin.PaintParentLinker(pnl)
    pnl.Paint = function(pnl, w, h)
        if pnl.Depressed then
            surface.SetDrawColor(Color(110, 110, 110))
        else
            surface.SetDrawColor(Color(100, 100, 100))
        end
        surface.DrawRect(0, 0, w, h)

        local outlineColor = Color(110, 110, 110)
        if pnl.error then
            outlineColor = Color(255, 0, 0)
        elseif pnl.parent then
            outlineColor = Color(255, 150, 0)
        end

        surface.SetDrawColor(outlineColor)
        surface.Util_DrawOutlinedRect(0, 0, w, h, 2)
    end
end

function HoloBuilder.Skin.PaintPartLinker(pnl)
    pnl.Paint = function(pnl, w, h)
        if pnl.Depressed then
            surface.SetDrawColor(Color(110, 110, 110))
        else
            surface.SetDrawColor(Color(100, 100, 100))
        end
        surface.DrawRect(0, 0, w, h)

        local outlineColor = Color(110, 110, 110)
        if pnl:GetDisabled() then
            outlineColor = HoloBuilder.Skin.colorDark
        elseif pnl.error then
            outlineColor = Color(255, 0, 0)
        elseif pnl:IsLinking() then
            outlineColor = Color(255, 150, 0)
        end

        surface.SetDrawColor(outlineColor)
        surface.Util_DrawOutlinedRect(0, 0, w, h, 2)
    end
end

function HoloBuilder.Skin.PaintScrollPanel(pnl)
    pnl.Paint = function(pnl, w, h)
        surface.SetDrawColor(HoloBuilder.Skin.colorDark)
        surface.DrawRect(0, 0, w, h)
    end
end

function HoloBuilder.Skin.PaintColorMixer(pnl)
    HoloBuilder.Skin.PaintTextEntry(pnl.txtR)
    HoloBuilder.Skin.PaintTextEntry(pnl.txtG)
    HoloBuilder.Skin.PaintTextEntry(pnl.txtB)
    HoloBuilder.Skin.PaintTextEntry(pnl.txtA)
end