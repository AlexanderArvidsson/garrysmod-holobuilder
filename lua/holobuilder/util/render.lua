 

local sin, cos, rad = math.sin, math.cos, math.rad

 function render.Util_RenderCrossedLines(pos, size, col, bDepthTest)
	size = size or 10

	render.DrawLine(pos - Vector(size, 0, 0), pos + Vector(size, 0, 0), col or Color(0,255,0), false)
	render.DrawLine(pos - Vector(0, size, 0), pos + Vector(0, size, 0), col or Color(0,255,0), false)
	render.DrawLine(pos - Vector(0, 0, size), pos + Vector(0, 0, size), col or Color(0,255,0), false)
end 


function render.Util_Render3DCircleOnSphere( pos, angle, radius, hAng, vertexCount, col, bDepthTest, angMin, angMax )
	angMin = math.Round( ( angMin or 0 ) )
	angMax = math.Round( ( angMax or 359 ) )

	--[[if angMax < angMin then
		local a = angMin
		angMin = angMax
		angMax = a
	end]]


	hAng = 90 - hAng
	local step = 1 / vertexCount
	local prev = nil
	for i = 0, 1, step do
		local ang = rad( angMin + i * ( angMax - angMin ) )
		local vertexPos = pos +
			( angle:Forward( ) * cos( ang ) * sin( rad( hAng ) ) ) * radius +
			( angle:Right( ) * sin( ang ) * sin( rad( hAng ) ) ) * radius +
			( angle:Up( ) * cos( rad( hAng ) ) ) * radius
		
		if prev ~= nil then
			render.DrawLine( vertexPos, prev, col or Color( 0, 255, 0 ), bDepthTest )
		end

		prev = vertexPos
	end
end

local matWhite = CreateMaterial( "WhiteMaterial", "UnlitGeneric", {	-- Necessary to assign color by vertex
	["$basetexture"] = "color/white",
	["$vertexcolor"] = 1,	-- Necessary to assign color by vertex
	["$vertexalpha"] = 1,	-- Necessary to assign alpha to vertex
	["$model"] = 1
} )
render.Util_MatWhite = matWhite
function render.Util_Render3DCircle( pos, angle, radius, vertexCount, col )
	local step = 360 / vertexCount
	local prev = nil
	col = col or Color( 255, 255, 255 )

	render.SetMaterial( matWhite )
	render.SuppressEngineLighting( true )
	mesh.Begin( MATERIAL_TRIANGLES, vertexCount * 2 )

		for i = 0, 360, step do
			local ang = rad( i )
			local vertexPos = pos +
				( angle:Forward() * sin( ang ) ) * radius +
				( angle:Right() * cos( ang ) ) * radius

			if prev ~= nil then
				--render.DrawLine( vertexPos, prev, col or Color( 0, 255, 0 ), true )

				mesh.Color( col.r, col.g, col.b, col.a )
				mesh.Position( pos )
				mesh.AdvanceVertex()

				mesh.Color( col.r, col.g, col.b, col.a )
				mesh.Position( prev )
				mesh.AdvanceVertex()

				mesh.Color( col.r, col.g, col.b, col.a )
				mesh.Position( vertexPos )
				mesh.AdvanceVertex()



				mesh.Color( col.r, col.g, col.b, col.a )
				mesh.Position( vertexPos )
				mesh.AdvanceVertex()

				mesh.Color( col.r, col.g, col.b, col.a )
				mesh.Position( prev )
				mesh.AdvanceVertex()

				mesh.Color( col.r, col.g, col.b, col.a )
				mesh.Position( pos )
				mesh.AdvanceVertex()
			end

			prev = vertexPos
		end

	mesh.End()
	render.SuppressEngineLighting( false )
end

function render.Util_Render3DCircleOutlined( pos, angle, radius, vertexCount, col, bDepthTest, angMin, angMax )
	render.Util_Render3DCircleOnSphere(pos, angle, radius, 0, vertexCount, col, bDepthTest, angMin, angMax )
end

function render.Util_Render3DCylinderWireframe( pos, angle, radius, height, vertexCount, col, bDepthTest )
	render.Util_Render3DCircleOutlined( pos, angle, radius, vertexCount, col, bDepthTest )
	if height == 0 then return end

	render.Util_Render3DCircleOutlined( pos + angle:Up() * height, angle, radius, vertexCount, col, bDepthTest )

	local step = 360 / vertexCount

	for i = 0, 360, step do
		local ang = rad(i)
		local vertexPos = pos +
			( angle:Right( ) * sin( ang ) ) * radius + 
			( angle:Forward( ) * cos( ang ) ) * radius
		
		
		render.DrawLine( vertexPos, vertexPos + angle:Up() * height, col, bDepthTest )
		
		prev = vertexPos
	end
end

function render.Util_Render3DSphereWireframe(pos, radius, vertexCount, col, bDepthTest)
	
	local step = 360 / vertexCount
	local prev = nil
	local prevVert = nil

	for k = 0, vertexCount do
		prevVert = nil
		for j = 0, 180, step do
			for i = 0, (360 / vertexCount), step do
				local ang = rad(i + (k * (360 / vertexCount)))
				local t = rad(j)
				local vertexPos = pos + Vector(
					cos( ang ) * sin( t ) * radius,
					sin( ang ) * sin( t ) * radius,
					cos( t ) * radius )
				
				if prev ~= nil then
					if i == 0 then
						if prevVert ~= nil then
							render.DrawLine(vertexPos, prevVert, col or Color(0,255,0), bDepthTest)
						end
					else
						render.DrawLine(vertexPos, prev, col or Color(0,255,0), bDepthTest)
					end
				end

				prev = vertexPos

				if i == 0 then prevVert = vertexPos end
			end
		end
	end
end

function render.Util_Render3DAABBBoxWireframe( min, max, col, bDepthTest )
	render.Util_Render3DOBBBoxWireframe( min, Angle( 0, 0, 0 ),
		max.x - min.x,
		max.y - min.y,
		max.z - min.z, col, bDepthTest )
end


function render.Util_Render3DOBBBoxWireframe( p, ang, xSize, ySize, zSize, col, bDepthTest )
	local f = ang:Forward()
	local r = -ang:Right()
	local u = ang:Up()

	-- Bottom Edges
	render.DrawLine(p, p + f * xSize, col, bDepthTest) -- 0x > x
	render.DrawLine(p, p + r * ySize, col, bDepthTest) -- 0y > y
	render.DrawLine(p + f * xSize, p + f * xSize + r * ySize, col, bDepthTest) -- x > xy
	render.DrawLine(p + r * ySize, p + f * xSize + r * ySize, col, bDepthTest) -- y > xy

	-- Middle Edges
	render.DrawLine(p, p + u * zSize, col, bDepthTest) -- 0, 0
	render.DrawLine(p + f * xSize + r * ySize, p + f * xSize + r * ySize + u * zSize, col, bDepthTest) -- x, y

	render.DrawLine(p + f * xSize, p + f * xSize + u * zSize, col, bDepthTest) -- x, 0
	render.DrawLine(p + r * ySize, p + r * ySize + u * zSize, col, bDepthTest) -- 0, y

	-- Top Edges
	render.DrawLine(p + u * zSize, p + f * xSize + u * zSize, col, bDepthTest) -- 0x > x
	render.DrawLine(p + u * zSize, p + r * ySize + u * zSize, col, bDepthTest) -- 0y > y
	render.DrawLine(p + f * xSize + u * zSize, p + f * xSize + r * ySize + u * zSize, col, bDepthTest) -- x > xy
	render.DrawLine(p + r * ySize + u * zSize, p + f * xSize + r * ySize + u * zSize, col, bDepthTest) -- y > xy





	--render.DrawLine(p, p + u * zSize, col, bDepthTest)

end