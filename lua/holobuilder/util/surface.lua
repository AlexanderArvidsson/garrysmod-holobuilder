local cos, sin, rad, floor = math.cos, math.sin, math.rad, math.floor
local setDrawColor = surface.SetDrawColor

local texOutlinedCorner = surface.GetTextureID( "gui/corner16" )

function surface.Util_SetDrawColor(r, g, b, a)
	setDrawColor(r, g, b, a)
end


local DrawRect = surface.DrawRect
function surface.Util_DrawOutlinedRect( x, y, w, h, thickness )
	DrawRect( x + thickness, y, w - thickness * 2, thickness )
	DrawRect( x + thickness, y + h - thickness, w - thickness * 2, thickness )

	DrawRect( x, y, thickness, h )
	DrawRect( x + w - thickness, y, thickness, h )
end

function surface.Util_DrawCircle(sx, sy, sw, sh, vertexCount, bOutline, thickness, angle)

	local vertices = {}
	local ang = -rad(angle or 0)
	local c = cos(ang)
	local s = sin(ang)
	for i=0,360,360/vertexCount do
		local radd = rad(i)
		local x = cos(radd)
		local y = sin(radd)

		local tempx = x * sw * c - y * sh * s + sx
		y = x * sw * s + y * sh * c + sy
		x = tempx

		vertices[#vertices+1] = { x = x, y = y, u = u, v = v }
	end
	if bOutline == true then
		local n = #vertices
		for i=1, n do
			local v = vertices[i]
			if (i+1<=n) then
				local x, y = v.x, v.y
				local x2, y2 = vertices[i+1].x, vertices[i+1].y
				surface.Util_DrawLine( x, y, x2, y2, thickness )
			end
		end

		surface.Util_DrawLine( vertices[n].x, vertices[n].y, vertices[1].x, vertices[1].y, thickness )
	else
		if (vertices and #vertices>0) then
			draw.NoTexture()
			surface.DrawPoly( vertices )
		end
	end
end

function surface.Util_DrawWedge( xpos, ypos, w, h, ang, degrees, vertexCount )
	if w > 0 and h > 0 and degrees ~= 360 then
		local vertices = {}

		vertices[1] = { x = xpos, y = ypos, u = 0, v = 0 }
		local ang = -rad( ang)
		local c = cos( ang )
		local s = sin( ang )
		for ii = 0, vertexCount do
			local i = ii * ( 360 - degrees ) / vertexCount
			local radd = rad( i )
			local x = cos( radd )
			local u = ( x + 1) / 2
			local y = sin( radd )
			local v = ( y + 1) / 2

			--radd = -rad(self.angle)
			local tempx = x * w * c - y * h * s + xpos
			y = x * w * s + y * h * c + ypos
			x = tempx

			vertices[ii+2] = { x = x, y = y, u = u, v = v }
		end

		if (vertices and #vertices>0) then
			surface.DrawPoly( vertices )
		end
	end
end

function surface.Util_DrawOutlinedWedge( xpos, ypos, w, h, ang, degrees, vertexCount, thickness )
	if w > 0 and h > 0 and degrees ~= 360 then
		thickness = thickness or 1
		
		local oldPos = { x = xpos, y = ypos }

		local ang = -rad( ang)
		local c = cos( ang )
		local s = sin( ang )
		for ii = 0, vertexCount do
			local i = ii * ( 360 - degrees ) / vertexCount
			local radd = rad( i )
			local x = cos( radd )
			local u = ( x + 1) / 2
			local y = sin( radd )
			local v = ( y + 1) / 2

			--radd = -rad(self.angle)
			local tempx = x * w * c - y * h * s + xpos
			y = x * w * s + y * h * c + ypos
			x = tempx

			surface.Util_DrawLine(oldPos.x, oldPos.y, x, y, thickness)
			
			oldPos = {x = x, y = y}
		end
		surface.Util_DrawLine(xpos, ypos, oldPos.x, oldPos.y, thickness)
	end
end

function surface.Util_DrawLine( x, y, x2, y2, size )
	if not size or size < 1 then size = 1 end
	if size == 1 then
		surface.DrawLine( x, y, x2, y2 )
	else
		-- Calculate position
		local x3 = (x + x2) / 2
		local y3 = (y + y2) / 2

		-- calculate height
		local w = math.sqrt( (x2-x) ^ 2 + (y2-y) ^ 2 )

		-- Calculate angle (Thanks to Fizyk)
		local angle = math.deg(math.atan2(y-y2,x2-x))
		
		draw.NoTexture()
		surface.DrawTexturedRectRotated( x3, y3, w, size, angle )
	end
end


function surface.Util_DrawParallelogram( x, y, width, height, nudge, outlined, thickness )
	nudge = nudge or 20
	local vertexData = {
		{ -- 1
			x = x - nudge,
			y = y,
		},
		{ -- 2
			x = x + width + nudge,
			y = y + height,
		},
		{ -- 3
			x = x,
			y = y + height,
		},

		{ -- 4
			x = x + width,
			y = y,
		},

		{ -- 5
			x = x + width + nudge,
			y = y + height,
		},
		--
	}
	local outline = { 1, 4, 2, 3 }

	draw.NoTexture()
	if outlined == true then
		for i = 1, #outline do
			local p1, p2 = vertexData[outline[i]], vertexData[outline[1 + i % #outline]]
			surface.Util_DrawLine( p1.x, p1.y, p2.x, p2.y, thickness )
		end
	else
		surface.DrawPoly(vertexData)
	end
end

function surface.Util_DrawPolyOutlined( data, thickness )
	for i = 1, #data do
		local p1, p2 = data[i], data[1 + i % #data]
		surface.Util_DrawLine( p1.x, p1.y, p2.x, p2.y, thickness or 1 )
	end
end

function surface.Util_DrawParallelogramCentered( x, y, width, height, nudge, outlined, thickness )
	x = x - width / 2
	y = y - height / 2

	surface.Util_DrawParallelogram(x, y, width, height, nudge, outlined, thickness)
end



function surface.Util_RoundedBoxOutlined( bordersize, x, y, w, h, color )
	x = math.Round( x )
	y = math.Round( y )
	w = math.Round( w )
	h = math.Round( h )

	surface.SetDrawColor( color.r, color.g, color.b, color.a )
	
	-- Draw as much of the rect as we can without textures
	surface.DrawRect( x+bordersize, y, w-bordersize*2, bordersize )
	surface.DrawRect( x+bordersize, h-y-bordersize, w-bordersize*2, bordersize )
	surface.DrawRect( x, y+bordersize, bordersize, h-bordersize*2 )
	surface.DrawRect( x+w-bordersize, y+bordersize, bordersize, h-bordersize*2 )
	
	surface.SetTexture( texOutlinedCorner )
	
	surface.DrawTexturedRectUV( x, y, bordersize, bordersize, 0, 0, 1, 1 )
	surface.DrawTexturedRectUV( x +w -bordersize, y, bordersize, bordersize, 1, 0, 0, 1 )
	surface.DrawTexturedRectUV( x, y +h -bordersize, bordersize, bordersize, 0, 1, 1, 0 )
	surface.DrawTexturedRectUV( x +w -bordersize, y +h -bordersize, bordersize, bordersize, 1, 1, 0, 0 )
end





function surface.Util_DrawBox( x, y, width, height, ang )
	local sin = math.Round( math.sin( math.rad( ang ) ), 6 )
	local cos = math.Round( math.cos( math.rad( ang ) ), 6 )

	surface.DrawTexturedRectRotated( x + width / 2, y + height / 2, width, height, ang )

end