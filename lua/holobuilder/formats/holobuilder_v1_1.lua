local FORMAT = {}
FORMAT.name = "HoloBuilder Version 1.1"

function FORMAT:WriteComment(str, comment)
    return str .. "# " .. comment .. "\n"
end

function FORMAT:WritePart(str, part, written, minified)
    if written[part:GetUniqueID()] then return end

    local separator = "\n"
    if minified then
        separator = ";"
    end

    --if part:GetParent() then
    --    self:WritePart(f, part:GetParent(), written, minified)
    --end

    local pos = part:GetPos()
    local ang = part:GetAngles()

    if part:GetType() == HB_PART_TYPE_CLIP then
        str = str .. "cp " .. part:GetID() .. " "
        str = str .. "\"" .. part:GetName() .. "\"" .. separator
        
        str = str .. "d "
        str = str .. self:GetVectorString(pos) .. " "
        str = str .. self:GetVectorString(part:GetOrigin()) .. " "
        str = str .. self:GetAngleString(ang) .. separator

    else
        str = str .. "p " .. part:GetID() .. " "
        str = str .. "\"" .. part:GetName() .. "\" "
        str = str .. "\"" .. part:GetModel() .. "\" "
        str = str .. "\"" .. part:GetMaterial() .. "\"" .. separator
        
        str = str .. "d "
        str = str .. self:GetVectorString(pos) .. " "
        str = str .. self:GetVectorString(part:GetOrigin()) .. " "
        str = str .. self:GetAngleString(ang) .. " "
        str = str .. self:GetVectorString(part:GetScale()) .. " "
        str = str .. self:GetColorString(part:GetColor()) .. separator

        print("-")
        local str = ""
        for k, v in pairs(part:GetClipPlanes()) do
            if str == "" then
                str = str .. "c " .. v:GetID()
                continue
            end

            str = str .. "," .. v:GetID()


            print("clip " .. v:GetID())
        end

        if str ~= "" then
            str = str .. str .. separator
        end
    end

    if part:GetParent() then
        str = str .. "pa " .. part:GetParent():GetID() .. separator
    end

    if not part:IsVisible() or part:IsLocked() then
        str = str .. "pi " .. self:GetBoolString(part:IsVisible()) .. " " .. self:GetBoolString(part:IsLocked()) .. separator
    end

    written[part:GetUniqueID()] = true

    return str
end

function FORMAT:GetData(parts, clips, minified)
    minified = minified or false

    local written = {}
    local minified = false

    local saveStr = ""

    saveStr = self:WriteComment(saveStr, self:GetHeader())

    if not minified then
        saveStr = saveStr .. "\n"
        saveStr = self:WriteComment(saveStr, "Clip Planes")
    end
    for k, plane in pairs(clips) do
        saveStr = self:WritePart(saveStr, plane, written, minified)
    end

    if not minified then
        saveStr = saveStr .. "\n"
        saveStr = self:WriteComment(saveStr, "Parts")
    end
    for k, part in pairs(parts) do
        saveStr = self:WritePart(saveStr, part, written, minified)
    end

    return saveStr
end

function FORMAT:Save(path, parts, clips)
    local f = file.Open(path, "w", "DATA")
    if f == nil then return end

    f:Write(self:GetData(parts, clips))

    f:Close()
end

function FORMAT:Load(lines)
    local parts = {}
    local clips = {}

    local parents = {}

    local part
    for k, line in pairs(lines) do
        if line == "" then continue end
        if line:sub(1, 1) == "#" then continue end

        local arr = string.Explode(" ", line)
        local op = arr[1]

        if op == "p" then
            local id = tonumber(arr[2])
            local name = arr[3]:Trim():Trim("\"")
            local model = arr[4]:Trim():Trim("\"")
            local mat = arr[5]:Trim():Trim("\"")

            part = HoloBuilder.Parts.New(name, model)
            part.id = id
            part:SetMaterial(mat)
            parts[id] = part


        elseif op == "cp" then
            local id = tonumber(arr[2])
            local name = arr[3]:Trim("\"")

            part = HoloBuilder.Parts.NewClipPlane(name)
            part.id = id
            clips[id] = part
        end

        if part then
            if op == "pa" then
                local id = tonumber(arr[2])
                parents[#parents + 1] = {
                    partId   = part:GetID(),
                    partType = part.type,
                    parentId = id
                }
            end

            if op == "pi" then
                part:SetVisible(self:GetBoolFromString(arr[2]))
                part:SetLocked(self:GetBoolFromString(arr[3]))
            end

            if part:GetType() == HB_PART_TYPE_CLIP then
                if op == "d" then
                    local pos    = self:GetVectorFromString(arr[2])
                    local origin = self:GetVectorFromString(arr[3])
                    local angle  = self:GetAngleFromString(arr[4])

                    part:SetPos(pos)
                    part:SetOrigin(origin)
                    part:SetAngles(angle)
                end

            elseif part:GetType() == HB_PART_TYPE_NORMAL then
                if op == "d" then
                    local pos    = self:GetVectorFromString(arr[2])
                    local origin = self:GetVectorFromString(arr[3])
                    local angle  = self:GetAngleFromString(arr[4])
                    local scale  = self:GetVectorFromString(arr[5])
                    local color  = self:GetColorFromString(arr[6])

                    part:SetPos(pos)
                    part:SetOrigin(origin)
                    part:SetAngles(angle)
                    part:SetScale(scale)
                    part:SetColor(color)

                elseif op == "c" then
                    local ids = string.Explode(",", arr[2])

                    for i = 1, #ids do
                        local id = tonumber(ids[i])
                        
                        local plane = clips[id]
                        if plane then
                            part:AddClipPlane(plane)
                        end
                    end
                end
            end
        end
    end

    for k, v in pairs(parents) do
        local part = nil
        if v.partType == HB_PART_TYPE_CLIP then
            part = clips[v.partId]
        else
            part = parts[v.partId]
        end

        local parent = parts[v.parentId]
        if part and parent then
            part:SetParent(parent)
        end
    end

    return parts, clips
end

HoloBuilder.File:AddFormat("holobuilder_v1_1", FORMAT)