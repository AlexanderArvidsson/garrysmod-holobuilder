print("running")

local render_DrawLine = render.DrawLine
local surface_SetDrawColor = surface.SetDrawColor
local surface_DrawRect = surface.DrawRect

HoloBuilder = HoloBuilder or {
    count = 0,
    mouseX = 0,
    mouseY = 0,
    traceRes = {
        pos      = Vector(0, 0, 0),
        part     = nil,
        hit      = false,
    },
    options = {
        showClipPlanes = true
    },
}

HoloBuilder.Camera = HoloBuilder.Camera or {
    pos      = Vector(0, 0, 0),
    origin   = Vector(0, 0, 0),
    zoom     = 100,
    destZoom = 100,
    ang      = Angle(),
    destAng  = Angle(),
    fov      = 90,
    zNear    = 1,
    zFar     = 1000,
}

HoloBuilder.version = 1.0

HoloBuilder.ghostPart = nil
HoloBuilder.defaultModel = "models/Combine_Helicopter/helicopter_bomb01.mdl"

HoloBuilder.hierarchyDirty = false


include("holobuilder/util/render.lua")
include("holobuilder/util/surface.lua")

include("holobuilder/tracesystem.lua")
include("holobuilder/parts.lua")
include("holobuilder/tools.lua")
include("holobuilder/grid.lua")
include("holobuilder/skin.lua")
include("holobuilder/file.lua")
include("holobuilder/importer.lua")
include("holobuilder/exporter.lua")
include("holobuilder/spawnmenu.lua")

include("holobuilder/derma/dtoolbar.lua")
include("holobuilder/derma/dproperties.lua")
include("holobuilder/derma/dhorizontaldivider.lua")
include("holobuilder/derma/dviewcube.lua")
include("holobuilder/derma/dstatusbar.lua")
include("holobuilder/derma/dfiledialog.lua")

hook.Add("SpawnMenuOpen", "HoloBuilder_SpawnMenuIntegration", function()
    timer.Simple(0.5, function()
        HoloBuilder.SpawnMenu:Integrate()
    end)
end)

hook.Add("HoloBuilderSpawnMenuIconMenu", "HoloBuilderIconMenu", function(menu, icon)
    menu:AddSpacer()
    menu:AddOption("Send to HoloBuilder", function()
        HoloBuilder.OpenEditor()

        local part = HoloBuilder.Parts.New("model", icon:GetModelName())
        HoloBuilder:AddPart(part)
    end)
end)


if IsValid(HoloBuilder.model) then
    HoloBuilder.model:Remove()
end

local model = ClientsideModel("models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
model:SetNoDraw(true)

HoloBuilder.model = model

local function urlEncode(str)
    local ndata = string.gsub(str, "[^%w _~%.%-]", function(str)
        local nstr = string.format("%X", string.byte(str))

        return "%" .. ((string.len(nstr) == 1) and "0" or "") .. nstr
    end)

    return string.gsub(ndata, " ", "+")
end

function HoloBuilder.OpenEditor()
    if IsValid(HoloBuilder.frame) then
        if HoloBuilder.frame:IsVisible() then
            HoloBuilder.frame:Close()
        else
            HoloBuilder.frame:Show()
            return
        end
    end

    local frame = vgui.Create("DFrame")
    frame:SetSize(1000, 800)
    frame:SetSizable(true)
    frame:SetMinimumSize(500,300)
    frame:MakePopup()

    frame.btnClose.DoClick = function(pnl)
        Derma_Query("Are you sure you want to close?\nAll unsaved progress will be lost!", "Close",
            "Confirm", function()
                HoloBuilder:Close()
            end,
            "Cancel", function() end)
    end

    frame.btnMinim:SetDisabled(false)
    frame.btnMinim.DoClick = function(pnl)
        frame:Hide()
    end

    local toolbarWidth = 32 + 2
    local propertyWidth = 250

    local divider = vgui.Create("DHoloBuilderHorizontalDivider", frame)
    divider:Dock(FILL)
    divider:DockMargin(3, 3, 0, 3)
    divider:SetLeftWidth(frame:GetWide() - propertyWidth - toolbarWidth)
    divider:SetRightMin(200)

    local statusBar = vgui.Create("DHoloBuilderStatusBar", frame)
    statusBar:Dock(BOTTOM)
    statusBar:DockMargin(-4, 0, -4, -4)



    local viewport = vgui.Create("Panel", frame)
    viewport:SetPos(5 + toolbarWidth, 50)

    viewport.Paint = function(pnl, w, h)
        local x, y = pnl:LocalToScreen()
        HoloBuilder:PaintViewport(x, y, w, h)
    end

    local mouseInfo = {}
    local oldMousePos = Vector()

    viewport.OnMousePressed = function(pnl, btn)
        local mX, mY = pnl:CursorPos()
        mouseInfo[btn] = { isDown = true, pos = Vector(mX, mY, 0) }

        HoloBuilder:OnMousePressed(btn)
    end

    viewport.OnMouseReleased = function(pnl, btn)
        mouseInfo[btn] = { isDown = false }

        HoloBuilder:OnMouseReleased(btn)
    end

    viewport.OnMouseWheeled = function(pnl, delta)
        HoloBuilder:OnMouseWheeled(delta)
    end

    frame.OnKeyCodePressed = function(pnl, keyCode)
        HoloBuilder:OnKeyCodePressed(keyCode)
    end

    viewport.Think = function(pnl)
        local mPos = Vector(gui.MouseX(), gui.MouseY(), 0)

        local xDelta = mPos.x - oldMousePos.x
        local yDelta = mPos.y - oldMousePos.y

        if xDelta ~= 0 or yDelta ~= 0 then
            for k, v in pairs(mouseInfo) do
                if v.isDown then
                    HoloBuilder:OnMouseDragged(k, xDelta, yDelta)

                    if not input.IsMouseDown(k) then
                        viewport:OnMouseReleased(k)
                    end
                end
            end
        end

        oldMousePos = mPos

        local mx, my = viewport:CursorPos()
        HoloBuilder:Think(mx, my)
    end

    local menuBar = vgui.Create("DMenuBar", frame)
    menuBar:DockMargin(-3, -6, -3, 0)

    local m1 = menuBar:AddMenu("File")
    m1:AddOption("New", function() HoloBuilder:ClearParts() end):SetIcon("icon16/page_white_go.png")
    m1:AddOption("Open", function()
        if IsValid(HoloBuilder.fileDialog) then
            HoloBuilder.fileDialog:Remove()
        end

        HoloBuilder.fileDialog = vgui.Create("DHoloBuilderFileDialog")
        HoloBuilder.fileDialog:Center()
        HoloBuilder.fileDialog:MakePopup()
        HoloBuilder.fileDialog:SetType(HoloBuilder.fileDialog.TYPE_OPEN)
        HoloBuilder.fileDialog:SetBasePath("holobuilder")
        HoloBuilder.fileDialog:SetPath("holobuilder/files")

        HoloBuilder.fileDialog.OnSelectedFile = function(pnl, path, file, dir)
            HoloBuilder:LoadFile(file, path)
        end
    end):SetIcon("icon16/folder.png")
    m1:AddOption("Save", function()
        if IsValid(HoloBuilder.fileDialog) then
            HoloBuilder.fileDialog:Remove()
        end

        local formats = {
            {
                text = "Model File",
                extension = ".txt"
            }
        }

        HoloBuilder.fileDialog = vgui.Create("DHoloBuilderFileDialog")
        HoloBuilder.fileDialog:Center()
        HoloBuilder.fileDialog:MakePopup()
        HoloBuilder.fileDialog:SetType(HoloBuilder.fileDialog.TYPE_SAVE)
        HoloBuilder.fileDialog:SetBasePath("holobuilder")
        HoloBuilder.fileDialog:SetPath("holobuilder/files")
        HoloBuilder.fileDialog:AddFormats(formats)

        HoloBuilder.fileDialog.OnSelectedFile = function(pnl, path, file, dir)
            HoloBuilder:SaveFile(file, path)
        end
    end):SetIcon("icon16/disk.png")

    -- File Menu > Import
    local menuImport, menuImportPnl = m1:AddSubMenu("Import")
    menuImportPnl:SetIcon("icon16/basket_put.png")
    menuImport:SetDeleteSelf(false)
    menuImport:AddOption("From Pastebin", function()
        HoloBuilder.panelImportPastebin.url:SetText("")
        HoloBuilder:SetExpandedPanel(HoloBuilder.panelImportPastebin)
    end):SetIcon("icon16/script.png")
    menuImport:AddOption("From Text", function() 
        HoloBuilder.panelImportText.text:SetText("")
        HoloBuilder:SetExpandedPanel(HoloBuilder.panelImportText)
    end):SetIcon("icon16/textfield.png")

    -- File Menu > Export
    local menuExport, menuExportPnl = m1:AddSubMenu("Export")
    menuExportPnl:SetIcon("icon16/basket_remove.png")
    menuExport:SetDeleteSelf(false)
    menuExport:AddOption("To Pastebin", function() 
        local format = HoloBuilder.File:GetDefaultFormat()
        if not format then return end

        HoloBuilder.panelExportPastebin.url:SetText("Processing...")
        HoloBuilder.panelExportPastebin.copy:SetEnabled(false)
        HoloBuilder:SetExpandedPanel(HoloBuilder.panelExportPastebin)

        http.Post("http://metamist.kuubstudios.com/pastebin.php", {
            data = format:GetData(HoloBuilder:GetParts(), HoloBuilder:GetClipPlanes(), true),
        }, function(body, contentLength, responseHeader, statusCode) 
            HoloBuilder.panelExportPastebin.url:SetText(body)
            HoloBuilder.panelExportPastebin.copy:SetEnabled(true)
        end)
    end):SetIcon("icon16/script.png")

    local exportE2 = menuExport:AddOption("To Expression 2", function() 
        if IsValid(HoloBuilder.fileDialog) then
            HoloBuilder.fileDialog:Remove()
        end

        local formats = {
            {
                text = "HoloCore",
                code = "e2_holocore",
                extension = ".txt"
            },   
        }

        HoloBuilder.fileDialog = vgui.Create("DHoloBuilderFileDialog")
        HoloBuilder.fileDialog:Center()
        HoloBuilder.fileDialog:MakePopup()
        HoloBuilder.fileDialog:SetType(HoloBuilder.fileDialog.TYPE_SAVE)
        HoloBuilder.fileDialog:SetBasePath("Expression2")
        HoloBuilder.fileDialog:SetPath("Expression2")
        HoloBuilder.fileDialog:AddFormats(formats)

        HoloBuilder.fileDialog.OnSelectedFile = function(pnl, path, fName, dir, format)
            local name = string.sub(fName, 1, string.len(fName) - 4)
            local exportFormat = HoloBuilder.Exporter:GetFormat(format.code)

            if not exportFormat then return end

            local str = exportFormat:GetExportedString(name, HoloBuilder:GetParts(), HoloBuilder:GetClipPlanes())

            file.Write(HoloBuilder.fileDialog.basePath .. "/" .. path .. fName, str)
        end
    end)
    exportE2:SetIcon("icon16/cog.png")
    exportE2.m_Image:SetImageColor(Color(255, 100, 100))



    HoloBuilder.menuView = menuBar:AddMenu("View")

    HoloBuilder.optionShowClipPlanes = HoloBuilder.menuView:AddOption("Show Clip Planes", function()
        HoloBuilder.options.showClipPlanes = not HoloBuilder.options.showClipPlanes

        if not HoloBuilder.options.showClipPlanes then
            local part = HoloBuilder:GetSelectedPart()
            if part and part.type == HB_PART_TYPE_CLIP then
                HoloBuilder:SelectPart(nil)
            end
        end
    end)

    local menuViewOpen = HoloBuilder.menuView.Open
    HoloBuilder.menuView.Open = function(pnl, x, y, skipanimation, owner)
        menuViewOpen(pnl, x, y, skipanimation, owner)

        if HoloBuilder.options.showClipPlanes then
            HoloBuilder.optionShowClipPlanes:SetIcon("icon16/tick.png")
        else
            HoloBuilder.optionShowClipPlanes:SetIcon()
        end
    end


    local toolBar = vgui.Create("DHoloBuilderToolBar", frame)
    toolBar:Dock(LEFT)
    toolBar:DockMargin(-3, 0, 0, -3)
    toolBar:SetWide(toolbarWidth)

    local viewCube = vgui.Create("DHoloBuilderViewCube", viewport)
    viewCube:SetSize(100, 100)
    viewCube.OnMousePressedFace = function(pnl, mCode, axis, normal)
        if mCode ~= MOUSE_LEFT then return end

        local angles = {
            Angle(0, 180, 0),
            Angle(0, 0, 0),
            Angle(0, -90, 0),
            Angle(0, 90, 0),
            Angle(90, 180, 0),
            Angle(-90, 180, 0),
        }
        
        HoloBuilder.Camera.destAng = angles[axis]
    end

    local expandPanel = vgui.Create("DPanel", viewport)
    expandPanel:Dock(BOTTOM)
    expandPanel:SetTall(0)
    HoloBuilder.Skin.PaintPanel(expandPanel, HoloBuilder.Skin.colorDark, 8)

    expandPanel.PerformLayout = function(pnl, w, h)
        if not IsValid(pnl.container) then return end

        pnl.container:SetPos(5, 5)
        pnl.container:SetWide(w - 10)
    end


    -- Pastebin Import Panel
    local panelImportPastebin = vgui.Create("Panel")
    panelImportPastebin:SetTall(40)
    panelImportPastebin:SetVisible(false)

    panelImportPastebin.ExpandedPerformLayout = function(pnl, w, h)
        pnl.url:SetWide(w - 50 - 5 - 50 - 5)
        pnl.cancel:SetPos(w - 50, 20)
        pnl.confirm:SetPos(w - 50 - 50 - 5, 20)
    end


    panelImportPastebin.label = vgui.Create("DLabel", panelImportPastebin)
    panelImportPastebin.label:SetText("Pastebin URL")
    panelImportPastebin.label:SetPos(10, 0)

    panelImportPastebin.url = vgui.Create("DTextEntry", panelImportPastebin)
    panelImportPastebin.url:SetPos(0, 20)
    HoloBuilder.Skin.PaintTextEntry(panelImportPastebin.url, HoloBuilder.Skin.colorDarker)

    panelImportPastebin.confirm = vgui.Create("DButton", panelImportPastebin)
    panelImportPastebin.confirm:SetSize(50, 20)
    panelImportPastebin.confirm:SetText("Confirm")
    panelImportPastebin.confirm.DoClick = function()
        local text = panelImportPastebin.url:GetText()
        if text:Trim() == "" then return end

        Derma_Query("Are you sure you want to import this?\nAll unsaved progress will be lost!", "Import",
            "Confirm", function()
                HoloBuilder:SetExpandedPanel(nil)
                HoloBuilder.Importer:Pastebin(text)
            end,
            "Cancel", function() end)
    end
    HoloBuilder.Skin.PaintButton(panelImportPastebin.confirm, HoloBuilder.Skin.colorPrimary)

    panelImportPastebin.cancel = vgui.Create("DButton", panelImportPastebin)
    panelImportPastebin.cancel:SetSize(50, 20)
    panelImportPastebin.cancel:SetText("Cancel")
    panelImportPastebin.cancel.DoClick = function() HoloBuilder:SetExpandedPanel(nil) end
    HoloBuilder.Skin.PaintButton(panelImportPastebin.cancel, HoloBuilder.Skin.colorPrimary)



    -- Text Import Panel
    local panelImportText = vgui.Create("Panel")
    panelImportText:SetTall(200)
    panelImportText:SetVisible(false)

    panelImportText.ExpandedPerformLayout = function(pnl, w, h)
        pnl.text:SetSize(w, h - pnl.cancel:GetTall() - 20 - 5)
        pnl.cancel:SetPos(w - 50, h - pnl.cancel:GetTall())
        pnl.confirm:SetPos(w - 50 - 50 - 5, h - pnl.confirm:GetTall())
    end

    panelImportText.label = vgui.Create("DLabel", panelImportText)
    panelImportText.label:SetText("Text")
    panelImportText.label:SetPos(10, 0)

    panelImportText.text = vgui.Create("DTextEntry", panelImportText)
    panelImportText.text:SetPos(0, 20)
    panelImportText.text:SetMultiline(true)
    HoloBuilder.Skin.PaintTextEntry(panelImportText.text, HoloBuilder.Skin.colorDarker)

    panelImportText.confirm = vgui.Create("DButton", panelImportText)
    panelImportText.confirm:SetSize(50, 20)
    panelImportText.confirm:SetText("Confirm")
    panelImportText.confirm.DoClick = function()
        local text = panelImportText.text:GetText()
        if text:Trim() == "" then return end

        Derma_Query("Are you sure you want to import this?\nAll unsaved progress will be lost!", "Import",
            "Confirm", function()
                HoloBuilder.Importer:Text(text)
                HoloBuilder:SetExpandedPanel(nil)
            end,
            "Cancel", function() end)
    end
    HoloBuilder.Skin.PaintButton(panelImportText.confirm, HoloBuilder.Skin.colorPrimary)

    panelImportText.cancel = vgui.Create("DButton", panelImportText)
    panelImportText.cancel:SetSize(50, 20)
    panelImportText.cancel:SetText("Cancel")
    panelImportText.cancel.DoClick = function() HoloBuilder:SetExpandedPanel(nil) end
    HoloBuilder.Skin.PaintButton(panelImportText.cancel, HoloBuilder.Skin.colorPrimary)

    -- Pastebin Export Result Panel
    local panelExportPastebin = vgui.Create("Panel")
    panelExportPastebin:SetTall(40)
    panelExportPastebin:SetVisible(false)

    panelExportPastebin.ExpandedPerformLayout = function(pnl, w, h)
        pnl.url:SetWide(w - 50 - 5 - 50 - 5)
        pnl.close:SetPos(w - 50, 20)
        pnl.copy:SetPos(w - 50 - 50 - 5, 20)
    end


    panelExportPastebin.label = vgui.Create("DLabel", panelExportPastebin)
    panelExportPastebin.label:SetText("Pastebin Result URL")
    panelExportPastebin.label:SizeToContents()
    panelExportPastebin.label:SetPos(10, 0)

    panelExportPastebin.url = vgui.Create("DTextEntry", panelExportPastebin)
    panelExportPastebin.url:SetPos(0, 20)
    panelExportPastebin.url:SetEnabled(false)
    HoloBuilder.Skin.PaintTextEntry(panelExportPastebin.url, HoloBuilder.Skin.colorDarker)

    panelExportPastebin.copy = vgui.Create("DButton", panelExportPastebin)
    panelExportPastebin.copy:SetSize(50, 20)
    panelExportPastebin.copy:SetText("Copy")
    panelExportPastebin.copy.DoClick = function() SetClipboardText(HoloBuilder.panelExportPastebin.url:GetText()) end
    HoloBuilder.Skin.PaintButton(panelExportPastebin.copy, HoloBuilder.Skin.colorPrimary)

    panelExportPastebin.close = vgui.Create("DButton", panelExportPastebin)
    panelExportPastebin.close:SetSize(50, 20)
    panelExportPastebin.close:SetText("Close")
    panelExportPastebin.close.DoClick = function() HoloBuilder:SetExpandedPanel(nil) end
    HoloBuilder.Skin.PaintButton(panelExportPastebin.close, HoloBuilder.Skin.colorPrimary)




    local propertyPanel = vgui.Create("DHoloBuilderProperties", frame)

    HoloBuilder.frame = frame
    HoloBuilder.toolBar = toolBar
    HoloBuilder.propertyPanel = propertyPanel
    HoloBuilder.viewCube = viewCube
    HoloBuilder.expandPanel = expandPanel
    HoloBuilder.statusBar = statusBar

    HoloBuilder.panelImportPastebin = panelImportPastebin
    HoloBuilder.panelExportPastebin = panelExportPastebin
    HoloBuilder.panelImportText = panelImportText

    toolBar:AddTool("add_part", "Add part", "holobuilder/add_part", function()
        local ghost = HoloBuilder.Parts.New("model", HoloBuilder.defaultModel)
        HoloBuilder:SetGhostPart(ghost)
        HoloBuilder.placingNewPart = true
    end)

    toolBar:AddTool("add_clip_plane", "Add clip plane", "holobuilder/add_clip_plane", function()
        local ghost = HoloBuilder.Parts.NewClipPlane()
        HoloBuilder:SetGhostPart(ghost)
        HoloBuilder.placingNewPart = true
    end)

    toolBar:AddSpacer(8)

    HoloBuilder:LoadTools()

    toolBar:AddSpacer(8)

    toolBar:AddTool("center_origin", "Center view on origin", "holobuilder/center_origin", function()
        HoloBuilder:CenterView()
    end)
    toolBar:AddTool("center_part", "Center view on part", "holobuilder/center_part", function()
        HoloBuilder:CenterViewOnPart(HoloBuilder:GetSelectedPart())
    end)

    divider:SetLeft(viewport)
    divider:SetRight(propertyPanel)
    divider:SetLeftWidth(divider:GetLeftWidth() - 50)

    local dividerPerformLayout = divider.PerformLayout
    divider.PerformLayout = function(pnl, w, h)
        dividerPerformLayout(pnl, w, h)

        HoloBuilder.viewportWidth = viewport:GetWide()
        HoloBuilder.viewportHeight = viewport:GetTall()

        viewCube:SetPos(0, h - viewCube:GetTall())
    end


    HoloBuilder:ClearParts()

    --[[
    local part1 = HoloBuilder.Parts.New("model1", "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
    HoloBuilder:AddPart(part1)

    local part2 = HoloBuilder.Parts.New("model1", "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
    part2:SetPos(Vector(0, 0, 12))
    part2:SetOrigin(Vector(6, 6, 0))
    part2:SetParent(part1)
    HoloBuilder:AddPart(part2)

    local part3 = HoloBuilder.Parts.New("model1", "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
    part3:SetParent(part2)
    part3:SetPos(Vector(0, 0, 12))
    HoloBuilder:AddPart(part3)

    local part4 = HoloBuilder.Parts.New("model1", "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
    part4:SetParent(part3)
    part4:SetPos(Vector(0, 0, 12))
    HoloBuilder:AddPart(part4)

    HoloBuilder:SelectPart(part1)

    HoloBuilder:LoadFile("test.txt", "files/")

    local exportFormat = HoloBuilder.Exporter:GetFormat("e2_holocore")
    if not exportFormat then return end

    local str = exportFormat:GetExportedString("export", HoloBuilder:GetParts(), HoloBuilder:GetClipPlanes())

    file.Write("Expression2/mechs/models/export2.txt", str)
    ]]


    --HoloBuilder:Close()
end

function HoloBuilder:Close()
    self:ClearParts()
    self:SelectTool(nil)
    self.frame:Close()
end

function HoloBuilder:SetExpandedPanel(pnl, performLayoutFunc)
    if not IsValid(pnl) then
        self.expandPanel:SizeTo(-1, 0, 0.2)
        return
    end

    if IsValid(self.expandPanel.container) then
        self.expandPanel.container:SetParent(nil)
        self.expandPanel.container:SetVisible(false)
    end

    performLayoutFunc = performLayoutFunc or pnl.ExpandedPerformLayout

    if performLayoutFunc then
        pnl.PerformLayout = performLayoutFunc
    end

    pnl:SetVisible(true)
    pnl:SetParent(self.expandPanel)

    self.expandPanel.container = pnl

    if pnl.Reset then
        pnl:Reset()
    end

    self.expandPanel:SizeTo(-1, pnl:GetTall() + 10, 0.2)
end

function HoloBuilder:LoadFile(file, subPath)
    local parts, clips = HoloBuilder.File:Load(file, subPath)
    if not parts or not clips then
        return
    end

    self:ClearParts()

    local count = 0
    for k, v in pairs(parts) do
        HoloBuilder:AddPart(v, v:GetID())
        count = count + 1
    end

    for k, v in pairs(clips) do
        HoloBuilder:AddClipPlane(v, v:GetID())
    end
end

function HoloBuilder:SaveFile(file, subPath)

    HoloBuilder.File:Save(file, self:GetParts(), self:GetClipPlanes(), subPath)
end

function HoloBuilder:AddTool(tool)
    local b = self.toolBar:AddTool(tool.name, tool.tooltip, tool.icon, function(tool, pnl) self:SelectTool(tool, pnl) end)
    tool:SetPanel(b)

    self.Tools.table[tool.name] = tool
end

function HoloBuilder:SelectTool(tool, pnl)
    if not tool then
        self.selectedTool = nil
        return
    end

    for k, v in pairs(self:GetTools()) do
        v:GetPanel().ToolSelected = false
    end

    if tool == self.selectedTool then
        self.selectedTool = nil
        return
    end

    pnl.ToolSelected = true

    self.selectedTool = tool
end

function HoloBuilder:GetTools()
    return self.Tools.table
end

function HoloBuilder:GetSelectedTool()
    if not self.selectedTool then return end

    return self:GetTools()[self.selectedTool]
end

function HoloBuilder:LoadTools()
    local files, directories = file.Find("holobuilder/tools/*", "LUA")
    local tools = {}
    
    print("loading tools")
    PrintTable(files)

    for k, fName in ipairs(files) do
        include("holobuilder/tools/" .. fName)
        
        tools[#tools + 1] = HB_TOOL
    end

    table.sort(tools, function(a, b)
        return a.position < b.position
    end)

    for k, tool in ipairs(tools) do
        self:AddTool(tool)
    end
end

function HoloBuilder:OnPartAdded(id, part)
     part.notInGrid = true
end

function HoloBuilder:OnPartRemoved(id, part)
    self:PopulateParts()
end

function HoloBuilder:OnPartPositionChanged(part, newPos, oldPos)
    if self:GetSelectedPart() == part then
        self.propertyPanel:UpdateSelectedPart()
    end

    self.Grid:UpdatePart(part)
end

function HoloBuilder:OnPartAngleChanged(part, newAng, oldAng)
    if self:GetSelectedPart() == part then
        self.propertyPanel:UpdateSelectedPart()
    end
end

function HoloBuilder:OnPartScaleChanged(part, newScale, oldScale)
    if self:GetSelectedPart() == part then
        self.propertyPanel:UpdateSelectedPart()
    end

    self.Grid:UpdatePart(part)
end

function HoloBuilder:OnPartParentChanged(part, newParent, oldParent)
    if self:GetSelectedPart() == part then
        self.propertyPanel:UpdateSelectedPart()
    end
end

function HoloBuilder:OnPartOriginChanged(part, newPos, oldPos)
    if self:GetSelectedPart() == part then
        self.propertyPanel:UpdateSelectedPart()
    end
end

function HoloBuilder:RotateView(ang)
    self.Camera.destAng.p =  self.Camera.destAng.p % 360
    self.Camera.destAng.y =  self.Camera.destAng.y % 360
    self.Camera.destAng.r =  self.Camera.destAng.r % 360

    if self.Camera.destAng.p >= 90 and self.Camera.destAng.p < 270 then ang.y = -ang.y end
    self.Camera.destAng = self.Camera.destAng + ang
end

function HoloBuilder:TranslateView(x, y)
    local ang   = self:GetViewAng()
    local right = ang:Right()
    local up    = ang:Up()

    self.Camera.origin = self.Camera.origin + right * -x + up * y
end

function HoloBuilder:ZoomView(delta)
    self.Camera.destZoom = math.Clamp(self.Camera.destZoom + delta, 0, 2000)
end

function HoloBuilder:CenterView()
    self.Camera.origin = Vector()
end

function HoloBuilder:CenterViewOnPart(part)
    if part == nil then return end

    self.Camera.origin = part:GetPos()
end

local function GetOrbitCamPos(origin, ang, dist)
    local dir = ang:Forward()

    return origin - dir * dist
end

function HoloBuilder:OnMousePressed(btn)
    local tool = self:GetSelectedTool()
    if tool then
        tool:OnMousePressed(btn, self:GetMousePos())
    end

    if btn == MOUSE_LEFT then
        local tool = self:GetSelectedTool()
        if not tool or (tool and tool:CanSelectParts() and not tool:IsHovered()) then
            self:SelectPart(self:GetClosestTracedPart())
        end
    end
end

function HoloBuilder:OnMouseReleased(btn)
    local tool = self:GetSelectedTool()
    if tool then
        tool:OnMouseReleased(btn, self:GetMousePos())
    end

    if btn == MOUSE_LEFT then
        if self.ghostPart then
            self:PlaceGhostPart()
            self.placingNewPart = false
        end
    elseif btn == MOUSE_RIGHT then
        if self.ghostPart then
            self:SetGhostPart(nil)
            self.placingNewPart = false
        end
    end
end


function HoloBuilder:OnMouseDragged(btn, dx, dy)
    local tool = self:GetSelectedTool()
    if tool then
        tool:OnMouseDragged(btn, self:GetMousePos(), dx, dy)
    end

    if btn == MOUSE_MIDDLE then
        if input.IsKeyDown(KEY_LSHIFT) then
            local mult = self:GetViewZoom() / 380
            self:TranslateView(dx * mult, dy * mult)
        else
            self:RotateView(Angle(dy, -dx, 0))
        end
    end
end

function HoloBuilder:OnMouseWheeled(delta)
    self:ZoomView(-delta * self.Camera.destZoom / 20)
end

function HoloBuilder:VectorToScreen(pos)
    local dir = pos - self:GetViewPos()
    dir:Normalize()

    local angCamRot = self:GetViewAng()
    local w = self.viewportWidth
    local h = self.viewportHeight

    --local ratio = w / h
    --local fovFix = (math.atan(math.tan((self.Camera.fov * math.pi) / 360) * (ratio / (4/3) )) * 360) / math.pi

    --Same as we did above, we found distance the camera to a rectangular slice of the camera's frustrum, whose width equals the "4:3" width corresponding to the given screen height.
    local d = (0.5 * w) / math.tan(math.rad(self.Camera.fov) * 0.5)
    local fdp = angCamRot:Forward():Dot(dir)
 
    --fdp must be nonzero ( in other words, dir must not be perpendicular to angCamRot:Forward() )
    --or we will get a divide by zero error when calculating vProj below.
    if fdp == 0 then
        return 0, 0, -1
    end
 
    --Using linear projection, project this vector onto the plane of the slice
    local vProj = (d / fdp) * dir
 
    --Dotting the projected vector onto the right and up vectors gives us screen positions relative to the center of the screen.
    --We add half-widths / half-heights to these coordinates to give us screen positions relative to the upper-left corner of the screen.
    --We have to subtract from the "up" instead of adding, since screen coordinates decrease as they go upwards.
    local x = 0.5 * w + angCamRot:Right():Dot(vProj)
    local y = 0.5 * h - angCamRot:Up():Dot(vProj)
 
    --Lastly we have to ensure these screen positions are actually on the screen.
    local iVisibility
    if fdp < 0 then         --Simple check to see if the object is in front of the camera
        iVisibility = -1
    elseif x < 0 || x > w || y < 0 || y > h then    --We've already determined the object is in front of us, but it may be lurking just outside our field of vision.
        iVisibility = 0
    else
        iVisibility = 1
    end
 
    return x, y, iVisibility
end

function HoloBuilder:OnKeyCodePressed(keyCode)
    if keyCode == KEY_DELETE then
        local selected = self:GetSelectedPart()
        if selected then
            --self:RemovePart(selected)

            local menu = DermaMenu()
            menu:SetDrawColumn( true )

            menu:AddOption("Delete part?", function() end).OnMouseReleased = function(pnl, mouseCode)
                DButton.OnMouseReleased(pnl, mouseCode)
            end

            menu:AddSpacer()
            menu:AddOption("Confirm", function()
                if selected:GetType() == HB_PART_TYPE_CLIP then
                    self:RemoveClipPlane(selected)
                else
                    self:RemovePart(selected)
                end
            end):SetIcon("icon16/tick.png")
            menu:AddOption("Cancel", function() end):SetIcon("icon16/cross.png")
            menu:Open()
        end
    end
end

function HoloBuilder:GetScreenDir(x, y)
    local w = self.viewportWidth
    local h = self.viewportHeight

    --local ratio = w / h
    --local fovFix = (math.atan(math.tan((self.Camera.fov * math.pi) / 360) * (ratio / (4/3) )) * 360) / math.pi

    --This code works by basically treating the camera like a frustrum of a pyramid.
    --We slice this frustrum at a distance "d" from the camera, where the slice will be a rectangle whose width equals the "4:3" width corresponding to the given screen height.
    local d = (0.5 * w) / math.tan(math.rad( self.Camera.fov ) * 0.5)
 
    --Forward, right, and up vectors (need these to convert from local to world coordinates
    local angCamRot = self:GetViewAng()
    local vForward = angCamRot:Forward()
    local vRight   = angCamRot:Right()
    local vUp      = angCamRot:Up()
 
    --Then convert vec to proper world coordinates and return it 
    local v = (d * vForward + (x - 0.5 * w) * vRight + (0.5 * h - y) * vUp)
    v:Normalize()
    return v
end

function HoloBuilder:GetMousePos()
    return Vector(self.mouseX, self.mouseY, 0)
end

function HoloBuilder:GetMouseOn3DPlane(origin, normal)
    origin = origin or Vector()
    normal = normal or -self:GetViewAng():Forward()

    local dir = self:GetScreenDir(self.mouseX, self.mouseY)
    return self.TraceSystem.RayPlaneIntersection(self:GetViewPos(), dir, origin, normal)
end

function HoloBuilder:GetViewPos() return self.Camera.pos end
function HoloBuilder:GetViewAng() return self.Camera.ang end
function HoloBuilder:GetViewOrigin() return self.Camera.origin end
function HoloBuilder:GetViewZoom() return self.Camera.zoom end

function HoloBuilder:GetViewZoomFactor()
    return self:GetViewZoom() / 100
end

function HoloBuilder:ClearParts()
    self:SelectPart(nil)

    self.propertyPanel:ClearNodes()
    self.Grid:Clear()

    for k in pairs(self:GetParts()) do
        self:GetParts()[k] = nil
    end

    for k in pairs(self:GetClipPlanes()) do
        self:GetClipPlanes()[k] = nil
    end

    self.count = 0
end

function HoloBuilder:PopulateParts(now)
    if now then
        self:PopulatePartsInternal()
        self.hierarchyDirty = false
        return
    end

    self.hierarchyDirty = true
end

function HoloBuilder:PopulatePartsInternal()
    if not IsValid(self.propertyPanel) then return end

    self.propertyPanel:PopulateParts(self:GetParts(), self:GetClipPlanes())
end

function HoloBuilder:GetParts()
    return self.Parts.table
end


function HoloBuilder:SelectPart(part, selectNode)
    if self.selectedPart == part then return end

    local action = self.propertyPanel.partParent:SelectPart(part)
    if action == 1 then
        return
    elseif action == 2 then
        self:PopulateParts(true)
        self.propertyPanel:ExpandToPart(self.selectedPart)

        return
    end

    if self.propertyPanel:SelectedPart(part, selectNode) then return end

    self.selectedPart = part
end

function HoloBuilder:GetSelectedPart()
    return self.selectedPart
end

function HoloBuilder:AddPart(part, id)
    if not id then
        id = table.insert(self:GetParts(), part)
    else
        self:GetParts()[id] = part
    end

    part.id = id
    part.added = true

    self.count = self.count + 1

    self:PopulateParts()

    self:OnPartAdded(id, part)
end

function HoloBuilder:RemovePart(part)
    if not part then return end

    part:ClearClipPlanes()

    self:GetParts()[part:GetID()] = nil

    self.Grid:RemovePart(part._gridNode, part)

    self.count = self.count - 1

    if self:GetSelectedPart() == part then
        self:SelectPart(nil)
    end

    for k, v in pairs(part.children) do
        if v then v:SetParent(nil) end
    end

    self:PopulateParts()

    self:OnPartRemoved(part:GetID(), part)
end

function HoloBuilder:SetGhostPart(part)
    if not part and self:GetSelectedPart() == self.ghostPart then
        self:SelectPart(nil)
    end

    self.ghostPart = part

    if self.ghostPart then
        self.ghostPart:SetGhost(true)
    end
end

function HoloBuilder:PlaceGhostPart()
    if not self.ghostPart then return end

    local part = self.ghostPart
    self:SetGhostPart(nil)

    part:SetGhost(false)

    if part:GetType() == HB_PART_TYPE_CLIP then
        self:AddClipPlane(part)
    else
        self:AddPart(part)
    end

    self:PopulateParts(true)

    self:SelectPart(part)
end

function HoloBuilder:GetClipPlanes()
    return self.Parts.clipTable
end

function HoloBuilder:AddClipPlane(plane, id)
    if not id then
        id = table.insert(self:GetClipPlanes(), plane)
    else
        self:GetClipPlanes()[id] = plane
    end

    plane.id = id
    plane.added = true
end

function HoloBuilder:RemoveClipPlane(plane)
    if not plane then return end

    local parts = plane:GetClippedParts()
    for k, v in pairs(parts) do
        v:RemoveClipPlane(plane)
    end

    self:GetClipPlanes()[plane:GetID()] = nil

    if self:GetSelectedPart() == plane then
        self:SelectPart(nil)
    end

    self:PopulateParts()
end


function HoloBuilder:GetTools()
    return self.Tools.table
end


function HoloBuilder:TracePart(part)
    local dir = self:GetScreenDir(self.mouseX, self.mouseY)
    local trace = part:Trace(self:GetViewPos(), dir)
    local traceRes = {
        pos     = Vector(0, 0, 0),
        part    = nil,
        hit     = false,
    }
    if trace ~= false then
        traceRes.pos = trace
        traceRes.hit = true
        traceRes.part = part
    end

    return traceRes
end

function HoloBuilder:GetClosestTracedPart()
    local closest = nil
    local closestDist = 0
    for k, v in pairs(self:GetParts()) do
        if not v:IsVisible() or v:IsLocked() then continue end

        local trace = self:TracePart(v)
        if trace.hit then
            local dist = self:GetViewPos():Distance(trace.pos)

            if closest == nil or dist < closestDist then
                closest = v
                closestDist = dist
            end
        end
    end

    if self.options.showClipPlanes then
        for k, v in pairs(self:GetClipPlanes()) do
            if not v:IsVisible() or v:IsLocked() then continue end

            local trace = self:TracePart(v)
            if trace.hit then
                local dist = self:GetViewPos():Distance(trace.pos)

                if closest == nil or dist < closestDist then
                    closest = v
                    closestDist = dist
                end
            end
        end
    end



    return closest
end

function HoloBuilder:InBounds(x, y)
    return x >= 0 and x < self.viewportWidth and y >= 0 and y < self.viewportHeight
end

function HoloBuilder:Think(mx, my)
    self.mouseX = mx
    self.mouseY = my

    if self.hierarchyDirty then
        self.hierarchyDirty = false

        self:PopulatePartsInternal()
    end

    --[[if self:InBounds(self.mouseX, self.mouseY) then
        local dir = self:GetScreenDir(self.mouseX, self.mouseY)
        self.Grid:TraceNodes(self:GetViewPos(), dir)
    end]]

    if self.ghostPart and self.placingNewPart then
        local trace = self:GetMouseOn3DPlane(self:GetViewOrigin())
        local pos = Vector()

        if trace then
            pos = trace
        end

        
        self.ghostPart:SetPos(pos)
    end

    local tool = self:GetSelectedTool()
    if tool then
        tool:Think()
    end

    self.statusBar:SetText("Parts: " .. self.count)
end


function HoloBuilder:DrawGrid()
    local gridSize   = 50
    local gridAmount = 9
    local color      = Color(150, 150, 150)
    local amt = gridAmount
    for i = 1, gridAmount do
        local j = i - (gridAmount / 2) - 0.5

        local posX1 = Vector(((amt / 2) - 0.5) * gridSize, j * gridSize, 0)
        local posX2 = Vector(-((amt / 2) - 0.5) * gridSize, j * gridSize, 0)
        render_DrawLine(posX1, posX2, color, true)

        local posY1 = Vector(j * gridSize, ((amt / 2) - 0.5) * gridSize, 0)
        local posY2 = Vector(j * gridSize, -((amt / 2) - 0.5) * gridSize, 0)
        render_DrawLine(posY1, posY2, color, true)
    end
end

function HoloBuilder:DrawParts()
    local model = self.model

    for k, v in pairs(self:GetParts()) do
        if v.hovering then
            v:DrawBoundingBox(Color(0, 255, 255, 60))
        end

        --v:DrawBoundingBox(Color(255, 0, 0, 60))

        if v:IsVisible() then
            v:Draw(model)
        end
    end

    if self.options.showClipPlanes then
        for k, v in pairs(self:GetClipPlanes()) do
            if not v:IsVisible() then continue end

            v:Draw(model)
        end
    end

    if self.selectedPart ~= nil then
        self.selectedPart:DrawBoundingBox(Color(255, 150, 0, 100))
        self.selectedPart:DrawLinkedClips(Color(255, 150, 0, 100))
    end

    if self.ghostPart then
        self.ghostPart:Draw(model)
    end
end

function HoloBuilder:PaintViewport(x, y, w, h)
    surface_SetDrawColor(HoloBuilder.Skin.colorPrimary)
    surface_DrawRect(0, 0, w, h)


    local camera = self.Camera
    camera.ang = LerpAngle(0.2, camera.ang, camera.destAng)
    camera.zoom = Lerp(0.2, camera.zoom, camera.destZoom)
    camera.pos = GetOrbitCamPos(camera.origin, camera.ang, camera.zoom)

    self.viewCube.angle = camera.ang

    local tool = self:GetSelectedTool()

    cam.Start3D(camera.pos, camera.ang, camera.fov, x, y, w, h, camera.zNear, camera.zFar)

        self:DrawGrid()
        self:DrawParts()

        if tool then
            tool:Draw(self.model)
        end

        if camera.origin ~= Vector() then
            render_DrawLine(camera.origin, camera.origin + Vector(2, 0, 0), Color(255, 0, 0), true)
            render_DrawLine(camera.origin, camera.origin + Vector(0, 2, 0), Color(0, 255, 0), true)
            render_DrawLine(camera.origin, camera.origin + Vector(0, 0, 2), Color(0, 0, 255), true)
        end    

        render_DrawLine(Vector(0, 0, 0), Vector(50, 0, 0), Color(255, 0, 0), true)
        render_DrawLine(Vector(0, 0, 0), Vector(0, 50, 0), Color(0, 255, 0), true)
        render_DrawLine(Vector(0, 0, 0), Vector(0, 0, 50), Color(0, 0, 255), true)

        local part = self:GetSelectedPart()
        if self.propertyPanel.partParent:IsLinking() or self.propertyPanel.partClipLinker:IsLinking() and part then
            local trace = self:GetMouseOn3DPlane()

            if trace then
                render_DrawLine(part:GetPos(), trace, Color(255, 150, 0), false)
            end
        end

        --self.Grid:Draw()

    cam.End3D()

    if tool then
        tool:Draw2D()
    end

    local selected = self:GetSelectedPart()
    if selected and selected:ShouldShowOrigin() then
        selected:DrawOrigin(20)
    end

end


concommand.Add("holobuilder", function(ply)
    HoloBuilder.OpenEditor()
end)