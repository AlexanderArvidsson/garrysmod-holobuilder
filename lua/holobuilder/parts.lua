HoloBuilder.Parts = HoloBuilder.Parts or {
    table = {},
    clipTable = {},
    mirrorTable = {},
    meta = {}
}
HoloBuilder.Parts.meta.__index = HoloBuilder.Parts.meta

HB_PART_TYPE_NORMAL = 0
HB_PART_TYPE_CLIP   = 1
HB_PART_TYPE_MIRROR = 2



local matOrigin = Material("holobuilder/origin")
local matClipPlane = Material("holobuilder/planes/clip")

local partMeta = HoloBuilder.Parts.meta

local function SetPartBaseValues(part)
    part.id            = 0
    part.ghost         = false
    part.pos           = Vector(0, 0, 0)
    part.renderPos     = Vector(0, 0, 0)
    part.origin        = Vector(0, 0, 0)
    part.renderOrigin  = Vector(0, 0, 0)
    part.angle         = Angle(0, 0, 0)
    part.renderAngle   = Angle(0, 0, 0)
    part.scale         = Vector(1, 1, 1)
    part.renderScale   = Vector(1, 1, 1)

    part.posIsLocal    = false
    part.angleIsLocal  = false
    part.scaleIsLocal  = false
    
    part.visible       = true
    part.locked        = false

    part.obb           = {
        max     = Vector(0, 0, 0),
        min     = Vector(0, 0, 0),
        size    = Vector(0, 0, 0),
        center  = Vector(0, 0, 0),
    }
    part.children      = {}
end


local function CopyVector(vec) return Vector(vec.x, vec.y, vec.z) end
local function CopyAngle(ang) return Angle(ang.p, ang.y, ang.z) end

local function CopyPartBase(part)
    local newTbl = table.Copy(part)

    newTbl.parent = nil
    newTbl.children = {}

    newTbl.pos          = CopyVector(part.pos)
    newTbl.renderPos    = CopyVector(part.renderPos)

    newTbl.origin       = CopyVector(part.origin)
    newTbl.renderOrigin = CopyVector(part.renderOrigin)

    newTbl.angle        = CopyAngle(part.angle)
    newTbl.renderAngle  = CopyAngle(part.renderAngle)

    newTbl.scale        = CopyVector(part.scale)
    newTbl.renderScale  = CopyVector(part.renderScale)

    newTbl.obb          = {
        max     = CopyVector(part.obb.max),
        min     = CopyVector(part.obb.min),
        size    = CopyVector(part.obb.size),
        center  = CopyVector(part.obb.center),
    }

    return newTbl
end

function HoloBuilder.Parts.New(name, model)
    if not name or not model then return end
    local part = setmetatable({}, partMeta)

    SetPartBaseValues(part)

    part.type          = HB_PART_TYPE_NORMAL
    part.name          = name
    part.model         = ""
    part.material      = ""
    part.locked        = false
    part.color         = Color(255, 255, 255, 255)
    part.clipPlanes    = {}

    part:SetModel(model)

    return part
end

function HoloBuilder.Parts.NewClipPlane(pos, dir)
    local part = setmetatable({}, partMeta)
    dir = dir or Vector(0, 0, 1)

    SetPartBaseValues(part)
    part.type         = HB_PART_TYPE_CLIP
    part.name         = "clip"
    part.clippedParts = {}

    local size = Vector(48, 48, 0)

    part:SetOBBox(-size / 2, size / 2, Vector())

    function part:Copy()
        local newPart = CopyPartBase(self)

        part.clippedParts = {}

        return newPart
    end

    function part:SetNormal(norm)
        self.normal = norm

        self:SetAngles(norm:Angle() + Angle(90, 0, 0))
    end

    function part:GetNormal() return self:GetAngles():Up() end
    function part:GetRenderNormal() return self:GetRenderAngles():Up() end

    function part:GetClippedParts() return self.clippedParts end

    function part:Draw()
        render.SetMaterial(matClipPlane)
        
        local col = HoloBuilder.Skin.colorAccent
        if #self.clippedParts == 0 then
            col = Color(255, 50, 50)
        end

        local obb = self:GetOBBox()
        render.DrawQuad(
            self:LocalToWorld(Vector(obb.min.x, obb.max.y, obb.max.z), true),
            self:LocalToWorld(Vector(obb.max.x, obb.max.y, obb.max.z), true),
            self:LocalToWorld(Vector(obb.max.x, obb.min.y, obb.max.z), true),
            self:LocalToWorld(Vector(obb.min.x, obb.min.y, obb.max.z), true),
            ColorAlpha(col, 150))
    end

    function part:CanToolManipulate(tool)
        if tool == "scale" then return false end

        return true
    end

    part:SetNormal(dir)

    return part
end

function partMeta:Copy()
    local newTbl = CopyPartBase(self)

    newTbl.clipPlanes = {}

    newTbl.color      = Color(self.color.r, self.color.g, self.color.b, self.color.a)

    return newTbl
end

function partMeta:SetType(type) self.type = type end
function partMeta:SetName(name) self.name = name end
function partMeta:SetOBBox(min, max, center)
    self.obb.min = min
    self.obb.max = max
    self.obb.size = max - min
    self.obb.center = center
end

function partMeta:SetModel(model)  self.model = string.Trim(model) end
function partMeta:SetMaterial(mat)
    mat = string.Trim(mat)
    if mat == "" then
        self.material = nil
    else
        self.material = mat
    end
end
function partMeta:SetColor(color)  self.color = color end
function partMeta:SetVisible(visible) self.visible = visible end
function partMeta:SetLocked(locked) self.locked = locked end

function partMeta:SetPos(pos, oldAng)
    if self:GetParent() then
        pos = self:GetParent():LocalToWorld(pos)
    end

    local oldPos = self.pos
    if pos == oldPos then return end

    self.pos = pos
    self.renderPos = pos

    self:UpdateChildPositions(oldPos, oldAng)

    HoloBuilder:OnPartPositionChanged(self, pos, oldPos)
end
function partMeta:SetRenderPos(pos) self.renderPos = pos end

function partMeta:SetOrigin(pos) 
    local old = self.origin

    if pos == old then return end
    self.origin = pos
    self.renderOrigin = pos

    HoloBuilder:OnPartOriginChanged(self, origin, oldOrigin)
end
function partMeta:SetRenderOrigin(pos) self.renderOrigin = pos end

function partMeta:SetAngles(ang)
    if self:GetParent() then
        ang = self:GetParent():LocalToWorldAngles(ang)
    end

    ang = Angle(ang.p % 360, ang.y % 360, ang.r % 360)

    local oldAng = self.angle
    if ang == oldAng then return end

    self.angle         = Angle(ang.p, ang.y, ang.r)
    self.renderAngle   = Angle(ang.p, ang.y, ang.r)

    self:UpdateChildAngles(oldAng)

    HoloBuilder:OnPartAngleChanged(self, ang, oldAng)
end
function partMeta:SetRenderAngles(ang)
    ang = Angle(ang.p % 360, ang.y % 360, ang.r % 360)

    self.renderAngle = Angle(ang.p, ang.y, ang.r)
end

local function ClampScale(scale) return Vector(math.max(0.01, scale.x), math.max(0.01, scale.y), math.max(0.01, scale.z)) end
function partMeta:SetScale(scale)
    scale = ClampScale(scale)

    local oldScale = self.scale
    if scale == oldScale then return end

    self.scale       = Vector(scale.x, scale.y, scale.z)
    self.renderScale = Vector(scale.x, scale.y, scale.z)

    HoloBuilder:OnPartScaleChanged(self, scale, oldScale)
end
function partMeta:SetRenderScale(scale) 
    scale = ClampScale(scale)
    self.renderScale = Vector(scale.x, scale.y, scale.z)
end

function partMeta:SetPosIsLocal(isLocal) self.posIsLocal = isLocal end
function partMeta:SetAngleIsLocal(isLocal) self.angleIsLocal = isLocal end
function partMeta:SetScaleIsLocal(isLocal) self.scaleIsLocal = isLocal end

function partMeta:SetGhost(ghost) self.ghost = ghost end

function partMeta:SetParent(part)
    if part == self or part and part:GetParent() == self then
        return
    end
    if part and self.type == HB_PART_TYPE_NORMAL and part.type == HB_PART_TYPE_CLIP then return end

    local oldParent = self.parent

    self.parent = part

    if part then
        part:AddChild(self)
    end

    if oldParent then
        oldParent:RemoveChild(self)
    end

    HoloBuilder:OnPartParentChanged(self, part, oldParent)
end

function partMeta:GetChildren() return self.children end

function partMeta:HasChild(part)
    for k, v in pairs(self:GetChildren()) do
        if v == part then return true, k end
    end

    return false
end

function partMeta:AddChild(part)
    local hasChild, i = self:HasChild(part)
    if hasChild then return end

    self.children[#self.children + 1] = part
end

function partMeta:RemoveChild(part)
    local hasChild, i = self:HasChild(part)
    if hasChild then
        table.remove(self.children, i)
    end
end

function partMeta:UpdateChildPositions(oldPos, oldAng)
    if not self.children then return end

    oldAng = oldAng or self:GetAngles()

    for k, v in pairs(self.children) do
        local localPos = WorldToLocal(v:GetPos(), Angle(), oldPos, oldAng)
        local oldPos = v:GetPos()

        v:SetPos(localPos)
    end
end

function partMeta:UpdateChildAngles(oldAng)
    if not self.children then return end

    for k, v in pairs(self.children) do
        local localPos, localAng = WorldToLocal(v:GetPos(), v:GetAngles(), self:GetPos(), oldAng)
        local oldAng = v:GetAngles()

        v:SetAngles(localAng)
        v:SetPos(localPos)
    end
end

function partMeta:GetID()           return self.id end
function partMeta:GetType()         return self.type end
function partMeta:GetUniqueID()     return self:GetType() .. "_" .. self:GetID() end
function partMeta:GetName()         return self.name end
function partMeta:GetOBBox()        return self.obb end
function partMeta:GetModel()        return self.model or "" end
function partMeta:GetMaterial()     return self.material or "" end
function partMeta:GetColor()        return self.color end
function partMeta:IsVisible()       return self.visible end
function partMeta:IsLocked()        return self.locked end

function partMeta:GetPos()          return self.pos end
function partMeta:GetRenderPos()    return self.renderPos end
function partMeta:GetOrigin()       return self.origin end
function partMeta:GetRenderOrigin() return self.renderOrigin end
function partMeta:GetAngles()       return self.angle end
function partMeta:GetRenderAngles() return self.renderAngle end
function partMeta:GetScale()        return self.scale end
function partMeta:GetRenderScale()  return self.renderScale end

function partMeta:IsPosLocal()      return self.posIsLocal end
function partMeta:IsAngleLocal()    return self.angleIsLocal end
function partMeta:IsScaleLocal()    return self.scaleIsLocal end

function partMeta:IsGhost()         return self.ghost end

function partMeta:ShouldShowOrigin() return self:IsPosLocal() or self:IsAngleLocal() or self:IsScaleLocal() end

function partMeta:GetParent()       return self.parent end
function partMeta:Forward()         return self:GetAngles():Forward() end
function partMeta:Right()           return self:GetAngles():Right() end
function partMeta:Up()              return self:GetAngles():Up() end

function partMeta:GetForward() return self.angle:Forward() end
function partMeta:GetRight() return self.angle:Right() end
function partMeta:GetUp() return self.angle:Up() end

function partMeta:GetLocalPos()
    if self:GetParent() then
        return self:GetParent():WorldToLocal(self:GetPos())
    end

    return self:GetPos()
end

function partMeta:GetLocalAngles()
    if self:GetParent() then
        return self:GetParent():WorldToLocalAngles(self:GetAngles())
    end

    return self:GetAngles()
end

function partMeta:CanToolManipulate(tool)
    return true
end

function partMeta:GetClipPlanes() return self.clipPlanes end
function partMeta:HasClipPlane(part)
    for k, v in pairs(self:GetClipPlanes()) do
        if v == part then return true, k end
    end

    return false
end

function partMeta:AddClipPlane(plane)
    if not plane or plane:GetType() ~= HB_PART_TYPE_CLIP then return end

    local hasClipPlane, i = self:HasClipPlane(plane)
    if hasClipPlane then return end

    self.clipPlanes[#self.clipPlanes + 1] = plane
    plane.clippedParts[#plane.clippedParts + 1] = self
end

function partMeta:RemoveClipPlane(plane)
    if not plane or plane:GetType() ~= HB_PART_TYPE_CLIP then return end

    local hasClipPlane, i = self:HasClipPlane(plane)
    if not hasClipPlane then return end

    table.remove(self.clipPlanes, i)

    for i, v in ipairs(plane.clippedParts) do
        if v == self then
            table.remove(plane.clippedParts, i)
            break
        end
    end
end

function partMeta:ClearClipPlanes()
    for k, v in pairs(self.clipPlanes) do
        self:RemoveClipPlane(v)
    end
end

function partMeta:LocalToWorld(offset, useRender)
    local pos, _ = nil, nil
    if useRender then
        pos, _ = LocalToWorld(offset, Angle(), self:GetRenderPos(), self:GetRenderAngles())
    else
        pos, _ = LocalToWorld(offset, Angle(), self:GetPos(), self:GetAngles())
    end

    return pos
end
function partMeta:LocalToWorldAngles(offset)
    local pos, ang = LocalToWorld(Vector(), offset, self:GetPos(), self:GetAngles())
    return ang
end
function partMeta:WorldToLocal(pos)
    return WorldToLocal(pos, Angle(), self:GetPos(), self:GetAngles())
end
function partMeta:WorldToLocalAngles(ang)
    local pos, ang = WorldToLocal(Vector(), ang, self:GetPos(), self:GetAngles())
    return ang
end

function partMeta:GetCenter()
    return self:LocalToWorld(self.obb.center)
end

function partMeta:Trace(origin, dir)
    local size = self:GetOBBox().size
    if self:GetType() == HB_PART_TYPE_NORMAL then
        size = size / 1.1
    end

    return HoloBuilder.TraceSystem.RayOBBoxIntersection(origin, dir, self:GetCenter(), size, self:GetAngles())
end


function partMeta:ToSaveableTable()
    local tbl = {}
    tbl.model   = self:GetModel()
    tbl.id      = self.id
    tbl.color   = self:GetColor()
    tbl.name    = self:GetName()
    tbl.pos     = self:GetPos()
    tbl.scale   = self:GetScale()
    tbl.angle   = self:GetAngles()

    return tbl
end

function partMeta:LeftClick(mPos) end
function partMeta:LeftClickDragged(mPos, xDelta, yDelta) end
function partMeta:LeftClickReleased(mPos) end
function partMeta:RightClick(mPos) end
function partMeta:RightClickDragged(mPos) end
function partMeta:RightClickReleased(mPos) end

function partMeta:OnHover()
    self.hovering = true
end

function partMeta:Think() end

function partMeta:DrawBoundingBox(color)
    render.Util_Render3DOBBBoxWireframe(self:LocalToWorld(self.obb.min, true), self:GetRenderAngles(), self.obb.size.x, self.obb.size.y, self.obb.size.z, color or Color(255,0,0, 50), false)
end

function partMeta:DrawLinkedClips(color)
    if self:GetType() ~= HB_PART_TYPE_NORMAL then return end

    for k, v in pairs(self:GetClipPlanes()) do
        local pos = v:GetRenderPos()

        render.DrawLine(self:GetRenderPos(), pos, color or Color(255,0,0, 50), false)
    end
end

local _faces = {
    [ BOX_FRONT ] = Vector( 1, 0, 0 ),
    [ BOX_BACK ] = Vector( -1, 0, 0 ),
    [ BOX_RIGHT ] = Vector( 0, 1, 0 ),
    [ BOX_LEFT ] = Vector( 0, -1, 0 ),
    [ BOX_TOP ] = Vector( 0, 0, 1 ),
    [ BOX_BOTTOM ] = Vector( 0, 0, -1 ),
}

local function DoLighting(lightDir, drawfunc, ambient, lightColor)
    ambient = ambient or Color(0.2, 0.2, 0.2)
    lightColor = lightColor or Color(1, 1, 1)

    render.SuppressEngineLighting(true)
    render.ResetModelLighting(0, 0, 0)

    local total = {}
    local dir = lightDir:GetNormalized()

    for k, v in pairs(_faces) do
        local br = math.max(-v:Dot(dir), 0)

        local col = Color(
            math.min(ambient.r + br * lightColor.r, 1),
            math.min(ambient.g + br * lightColor.g, 1),
            math.min(ambient.b + br * lightColor.b, 1))

        local r, g, b = col.r, col.g, col.b
        render.SetModelLighting(k, r, g, b)
    end

    if not drawfunc then return end

    drawfunc()

    render.SuppressEngineLighting( false )
end

function partMeta:Draw(clientModel)
    if not IsValid(clientModel) then return end

    local col = self:GetColor()
    col = { r = col.r, g = col.g, b = col.b, a = col.a }
    
    if self.hovering then
        col.r = col.r + 255
        col.g = col.g + 255
        col.b = col.b + 255
    end
    if self.ghost then
        col.a = 255 / 2
    end

    local prevClipState = render.EnableClipping(true)

    for k, v in pairs(self:GetClipPlanes()) do
        local normal = v:GetRenderNormal()
        local origin = v:GetRenderPos()

        render.PushCustomClipPlane(normal, normal:Dot(origin))
    end

    render.SetColorModulation(col.r / 255, col.g / 255, col.b / 255)
    render.SetBlend(col.a / 255)

    clientModel:SetModel(self:GetModel())

    if self:GetMaterial() == "" then
        clientModel:SetMaterial(nil)
    else
        if clientModel:GetMaterial() ~= self:GetMaterial() then
            clientModel:SetMaterial(self:GetMaterial())
        end
    end

    local m = Matrix()
    local scale = self:GetRenderScale()
    m:Scale(scale)
    clientModel:EnableMatrix("RenderMultiply", m)

    clientModel:SetRenderOrigin(self:GetRenderPos())
    clientModel:SetRenderAngles(self:GetRenderAngles())
    clientModel:SetupBones()

    local lightDir = Vector(-0.666667, -0.333333, -0.666667)
    --render.DrawLine(self:GetCenter(), lightPos, Color(255, 255, 0, 2), false)

    --DoLighting(lightDir, function()
        clientModel:DrawModel()
    --end, Color(0.1, 0.1, 0.1))

    local m = Matrix()
    m:Scale(Vector(1, 1, 1))
    clientModel:EnableMatrix("RenderMultiply", m)

    for k, v in pairs(self:GetClipPlanes()) do
        render.PopCustomClipPlane()
    end

    render.EnableClipping(prevClipState)

    self:SetOBBox(clientModel:OBBMins() * scale, clientModel:OBBMaxs() * scale, clientModel:OBBCenter() * scale)
    
    if self.obb.size.x ~= 0 and self.obb.size.y ~= 0 and self.obb.size.z ~= 0 then
        self.obbSet = true
    end

    self.hovering = false
end

function partMeta:DrawOrigin(radius, optOrigin)
    surface.SetMaterial(matOrigin)
    local oX, oY = HoloBuilder:VectorToScreen(self:LocalToWorld(optOrigin or self:GetRenderOrigin(), true))

    surface.SetDrawColor(Color(255, 255, 255))
    surface.DrawTexturedRectRotated(oX, oY, radius, radius, 0)
end