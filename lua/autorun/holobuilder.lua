
if SERVER then
    AddCSLuaFile()

    local function AddDir(dir) // recursively adds everything in a directory to be downloaded by client
        local files, folders = file.Find(dir.."/*", "GAME")

        for _, fdir in pairs(folders) do
            if fdir != ".svn" then // don't spam people with useless .svn folders
                AddDir(dir.."/"..fdir)
            end
        end

        for k,v in pairs(files) do
            resource.AddSingleFile(dir.."/"..v)
        end
    end

    AddDir("materials/holobuilder")

    AddCSLuaFile("holobuilder/util/render.lua")
    AddCSLuaFile("holobuilder/util/surface.lua")

    AddCSLuaFile("holobuilder/tracesystem.lua")
    AddCSLuaFile("holobuilder/parts.lua")
    AddCSLuaFile("holobuilder/tools.lua")
    AddCSLuaFile("holobuilder/grid.lua")
    AddCSLuaFile("holobuilder/skin.lua")
    AddCSLuaFile("holobuilder/file.lua")
    AddCSLuaFile("holobuilder/importer.lua")
    AddCSLuaFile("holobuilder/exporter.lua")
    AddCSLuaFile("holobuilder/spawnmenu.lua")

    local function AddCSFolder(folder)
        local files, _ = file.Find(folder .. "/*", "lsv")

        for k, fName in ipairs(files) do
            AddCSLuaFile(folder .. "/" .. fName)
            --print("Adding file", folder.."/"..fName)
        end
    end

    AddCSFolder("holobuilder/tools")
    AddCSFolder("holobuilder/derma")
    AddCSFolder("holobuilder/export_formats")
    AddCSFolder("holobuilder/formats")

    AddCSLuaFile("holobuilder/core.lua")
else
    include("holobuilder/core.lua")
end
