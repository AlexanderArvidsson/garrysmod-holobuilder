local function includeFolder(folder)
    local files, _ = file.Find(folder .. "/*", "LUA")

    for k, fName in ipairs(files) do
        include(folder .. "/" .. fName)
        print("Including file", folder.."/"..fName)
    end
end

if SERVER then
    AddCSLuaFile()

    local function AddDir(dir) // recursively adds everything in a directory to be downloaded by client
        local files, folders = file.Find(dir.."/*", "GAME")

        for _, fdir in pairs(folders) do
            if fdir != ".svn" then // don't spam people with useless .svn folders
                AddDir(dir.."/"..fdir)
            end
        end
     
        for k,v in pairs(files) do
            resource.AddSingleFile(dir.."/"..v)
        end
    end

    AddDir("materials/material")
    AddDir("resource/fonts")

    local function AddCSFolder(folder)
        local files, _ = file.Find(folder .. "/*", "lsv")

        for k, fName in ipairs(files) do
            AddCSLuaFile(folder .. "/" .. fName)
            print("Adding file", folder.."/"..fName)
        end
    end

    AddCSFolder("material/derma")

    AddCSLuaFile("material/core.lua")
else
    include("material/core.lua")

    includeFolder("material/derma")

    --if IsValid(TEST_FRAME) then
    --    TEST_FRAME:Remove()
    --end
    
    --TEST_FRAME = vgui.Create("DMaterialFrame")
    --TEST_FRAME:SetSize(800, 400)
    --TEST_FRAME:SetPos(20, 40)
    --TEST_FRAME:SetSizable(true)
    --TEST_FRAME:MakePopup()

    --local toolbar = vgui.Create("DMaterialToolbar", TEST_FRAME)
end