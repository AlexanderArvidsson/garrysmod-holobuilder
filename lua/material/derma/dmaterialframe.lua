local PANEL = {}

AccessorFunc(PANEL, "_matBarColor", "BarColor")
AccessorFunc(PANEL, "_matBackColor", "BackColor")
AccessorFunc(PANEL, "_matMinimized", "Minimized", FORCE_BOOL)
AccessorFunc(PANEL, "_matMaximized", "Maximized", FORCE_BOOL)

function PANEL:Init()
    self.btnClose:Remove()
    self.btnClose = vgui.Create("DMaterialIconButton", self)
    self.btnClose:SetClipEnabled(true)
    self.btnClose.DoClick = function(button) timer.Simple(0.2, function() self:CloseInternal() end) end
    self.btnClose:SetIcon("material/window_close.png")
    self.btnClose.icon:DockMargin(4, 4, 4, 4)

    self.btnMaxim:Remove()
    self.btnMaxim = vgui.Create("DMaterialIconButton", self)
    self.btnMaxim:SetClipEnabled(true)
    self.btnMaxim.DoClick = function(button) timer.Simple(0.2, function() self:SetMaximized(true) end) end
    self.btnMaxim:SetIcon("material/window_maximize.png")
    self.btnMaxim.icon:DockMargin(4, 4, 4, 4)

    self.btnRestore = vgui.Create("DMaterialIconButton", self)
    self.btnRestore:SetClipEnabled(true)
    self.btnRestore.DoClick = function(button) timer.Simple(0.2, function() self:SetMaximized(false) end) end
    self.btnRestore:SetIcon("material/window_restore.png")
    self.btnRestore.icon:DockMargin(4, 4, 4, 4)

    self.btnMinim:Remove()
    --[[self.btnMinim = vgui.Create("DMaterialIconButton", self)
    self.btnClose:SetClipEnabled(true)
    self.btnMinim.DoClick = function(button) self:SetMinimized(true) end
    self.btnMinim.icon:SetImage("material/window_minimize.png")
    self.btnMinim.icon:DockMargin(4, 7, 4, 1)]]

    self:SetBarColor(MaterialLib.VGUI:Hex2RGB("#455A64"))
    self:SetBackColor(MaterialLib.VGUI:Hex2RGB("#FFFFFFF"))

    self.btnRestore:SetVisible(false)

    self:SetTitle("")
end

function PANEL:SetAnimating(animating)
    self._matAnimating = animating
end

function PANEL:IsAnimating()
    return self._matAnimating
end

function PANEL:CloseInternal()
    if self:IsAnimating() then return end

    local x, y = self:GetPos()
    self:MoveTo(0, ScrH(), 0.2)
    self:SizeTo(0, 0, 0.2)

    self:SetAnimating(true)

    timer.Simple(0.2, function()
        self:Close()
        self:SetAnimating(false)
    end)
end

function PANEL:SetMaximized(maximized)
    if self:IsAnimating() then return end

    self._matMaximized = maximized

    if maximized then
        self._matRestoreWidth, self._matRestoreHeight = self:GetSize()
        self._matRestoreX, self._matRestoreY = self:GetPos()

        self:SizeTo(ScrW(), ScrH(), 0.2)
        self:MoveTo(0, 0, 0.2)
    else
        self:MoveTo(self._matRestoreX, self._matRestoreY, 0.2)
        self:SizeTo(self._matRestoreWidth, self._matRestoreHeight, 0.2)
    end

    self:SetDraggable(not maximized)

    self.btnMaxim:SetVisible(not maximized)
    self.btnRestore:SetVisible(maximized)

    self:SetAnimating(true)

    timer.Simple(0.2, function()
        self:SetAnimating(false)
    end)
end

function PANEL:SetMinimized(minimized)
    self._matMinimized = minimized

    if minimized then
        self._matRestoreWidth, self._matRestoreHeight = self:GetSize()
        self._matRestoreX, self._matRestoreY = self:GetPos()

        local x = 0
        local i = 0
        local max = math.floor(ScrW() / 100) - 1
        while i < max do
            if not IsValid(MaterialLib.VGUI.minimizedWindows[i]) then
                x = i * 102

                break
            end

            i = i + 1
        end

        if i == max then return end


        self:MoveTo(x, ScrH() - 22, 0.2)
        self:SizeTo(100, 22, 0.2)

        MaterialLib.VGUI.minimizedWindows[i] = self
    else
        self:MoveTo(self._matRestoreX, self._matRestoreY, 0.2)
        self:SizeTo(self._matRestoreWidth, self._matRestoreHeight, 0.2)

        for k, v in pairs(MaterialLib.VGUI.minimizedWindows) do
            if v == self then
                MaterialLib.VGUI.minimizedWindows[k] = nil
            end
        end
    end

    self.btnMaxim:SetVisible(not minimized)
    self.btnRestore:SetVisible(minimized)
    self.btnMinim:SetVisible(not minimized)
end

function PANEL:PerformLayout(w, h)
    self.btnClose:SetSize(24, 24)
    self.btnClose:SetPos(w - 24, 0)

    self.btnMaxim:SetSize(24, 24)
    self.btnMaxim:SetPos(w - 24 * 2, 0)

    self.btnRestore:SetSize(24, 24)
    self.btnRestore:SetPos(w - 24 * 2, 0)

    --self.btnMinim:SetSize(24, 24)
    --self.btnMinim:SetPos(w - 24 * 3, 0)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(self:GetBarColor())
    surface.DrawRect(0, 0, w, 24)

    surface.SetDrawColor(self:GetBackColor())
    surface.DrawRect(0, 24, w, h - 24)
end

vgui.Register("DMaterialFrame", PANEL, "DFrame")