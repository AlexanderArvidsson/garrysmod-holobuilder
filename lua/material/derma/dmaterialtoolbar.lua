surface.CreateFont("MaterialToolbarTitle", {
    font = "Roboto",
    size = 20,
    antialias = true,
    weight  = 500
})

local PANEL = {}

AccessorFunc(PANEL, "_matColor", "Color")
AccessorFunc(PANEL, "_matTitle", "Title", FORCE_STRING)
AccessorFunc(PANEL, "_matAutoTitleColor", "AutoTitleColor", FORCE_BOOL)

function PANEL:Init()
    self.title = vgui.Create("DLabel", self)
    self.title:SetFont("MaterialToolbarTitle")
    self.title:SetPos(16, 14)
    self.title:SetTextColor(Color(255, 255, 255))

    self:Dock(TOP)
    self:DockMargin(-5, -5, -5, -5)
    self:SetTall(48)

    self:SetAutoTitleColor(true)
    self:SetColor(MaterialLib.VGUI:Hex2RGB("#607D8B"))

    self:SetTitle("Herp di Derp")
end

function PANEL:SetColor(color)
    self._matColor = color

    if self:GetAutoTitleColor() then
        local res = color.r + color.g + color.b
        local res = math.Round(((color.r * 299) + (color.g * 587) + (color.b * 114)) / 1000);

        local col = Color(255, 255, 255)
        if res >= 136 then
            col = Color(0, 0, 0)
        end

        self:SetTitleColor(col)
    end
end

function PANEL:SetTitle(title)
    self._matTitle = title

    self.title:SetText(title)
    self.title:SizeToContents()
end

function PANEL:SetTitleColor(color)
    self.title:SetTextColor(color)
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(self:GetColor())
    surface.DrawRect(0, 0, w, h)
end

vgui.Register("DMaterialToolbar", PANEL, "DPanel")