local PANEL = {}

AccessorFunc(PANEL, "_matIcon", "Icon")
AccessorFunc(PANEL, "_matClipEnabled", "ClipEnabled", FORCE_BOOL)

MaterialLib.VGUI:AddRipple(PANEL)

function PANEL:Init()
    self.icon = vgui.Create("DImage", self)
    self.icon:Dock(FILL)

    self:SetClipEnabled(false)
    self:SetIcon("material/window_close.png")
end

function PANEL:SetClipEnabled(clip)
    self._matClipEnabled = clip

    self:NoClipping(not clip)
end

function PANEL:SetIcon(icon)
    self._matIcon = icon

    self.icon:SetImage(icon)
end

function PANEL:OnMousePressed(code)
    self:TriggerRipple(self:LocalCursorPos())

    if code == MOUSE_RIGHT then
        self:DoRightClick()
    end
    
    if code == MOUSE_LEFT then
        self:DoClick()
    end

    if code == MOUSE_MIDDLE then
        self:DoMiddleClick()
    end
end

function PANEL:DoClick() end
function PANEL:DoRightClick() end
function PANEL:DoMiddleClick() end

function PANEL:Paint() end

function PANEL:PaintOver(w, h)
    self:PaintRipple(w, h, Color(0, 0, 0), not self:GetClipEnabled())
end

vgui.Register("DMaterialIconButton", PANEL, "DPanel")