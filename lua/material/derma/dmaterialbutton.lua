local PANEL = {}
MaterialLib.VGUI:AddRipple(PANEL)

function PANEL:Init()

end

function PANEL:OnMousePressed(code)
    self:TriggerRipple(self:LocalCursorPos())
end

function PANEL:Paint(w, h)
    surface.SetDrawColor(255, 150, 150)
    surface.DrawRect(0, 0, w, h)
end

function PANEL:PaintOver(w, h)
    self:PaintRipple(w, h, Color(0, 0, 0))
end

vgui.Register("DMaterialButton", PANEL, "DButton")