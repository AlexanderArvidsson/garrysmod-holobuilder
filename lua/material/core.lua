MaterialLib = MaterialLib or {
    Surface = {},
    VGUI = {
        minimizedWindows = {}
    }
}

local math_rad = math.rad
local math_sin = math.sin
local math_cos = math.cos
local math_sqrt = math.sqrt
local math_pow = math.pow
local math_Clamp = math.Clamp


function MaterialLib.VGUI:Hex2RGB(hex)
    hex = hex:gsub("#", "")
    return Color(tonumber("0x" .. hex:sub(1, 2)), tonumber("0x" .. hex:sub(3, 4)), tonumber("0x" .. hex:sub(5, 6)))
end

function MaterialLib.Surface:DrawCircle(sx, sy, sw, sh, vertexCount, angle)
    local vertices = {}
    local ang = -math_rad(angle or 0)
    local c = math_cos(ang)
    local s = math_sin(ang)
    for i = 0, 360, 360 / vertexCount do
        local radd = math_rad(i)
        local x = math_cos(radd)
        local y = math_sin(radd)

        local tempx = x * sw * c - y * sh * s + sx
        y = x * sw * s + y * sh * c + sy
        x = tempx

        vertices[#vertices + 1] = { x = x, y = y, u = u, v = v }
    end

    if vertices and #vertices > 0 then
        draw.NoTexture()
        surface.DrawPoly(vertices)
    end
end

function MaterialLib.Surface:CalculateRippleSize(localX, localY, pnlWidth, pnlHeight)
    local destX = 0
    local destY = 0
    if localX < pnlWidth / 2 then -- Left Half
        if localY < pnlHeight / 2 then -- Top Left Quadrant
            destX, destY = pnlWidth, pnlHeight
        else -- Bottom Left Quadrant
            destX, destY = pnlWidth, 0
        end
    else -- Right Half
        if localY < pnlHeight / 2 then -- Top Right Quadrant
            destX, destY = 0, pnlHeight
        else -- Bottom Right Quadrant
            destX, destY = 0, 0
        end
    end

    return math_sqrt(math_pow(destX - localX, 2) + math_pow(destY - localY, 2))
end

function MaterialLib.Surface:DrawRipple(x, y, size, t, color)
    local tExpand = math_Clamp(t, 0, 0.6) / 0.6
    local tFade = (1 - math_Clamp(t, 0.6, 1)) / 0.4
    local a = 40

    local color = color and ColorAlpha(color, a * tFade) or Color(255, 255, 255, a * tFade)
    surface.SetDrawColor(color)
    self:DrawCircle(x, y, size * tExpand, size * tExpand, 32)
end

function MaterialLib.Surface:DrawRippleNoClip(x, y, pnlW, pnlH, t, color)
    local tExpand = math_Clamp(t, 0, 0.6) / 0.6
    local tFade = (1 - math_Clamp(t, 0.6, 1)) / 0.4
    local a = 40

    local size = math_sqrt(math_pow(pnlW / 2, 2) + math_pow(pnlH / 2, 2))
    local x = Lerp(tExpand, x, pnlW / 2)
    local y = Lerp(tExpand, y, pnlH / 2)

    local color = color and ColorAlpha(color, a * tFade) or Color(255, 255, 255, a * tFade)
    surface.SetDrawColor(color)
    self:DrawCircle(x, y, size * tExpand, size * tExpand, 32)
end

function MaterialLib.Surface:DrawRippleBack(pnlW, pnlH, t, color)
    local tFade = (1 - math_Clamp(t, 0.6, 1)) / 0.4
    local a = 20
    local size = math_sqrt(math_pow(pnlW / 2, 2) + math_pow(pnlH / 2, 2))

    local color = color and ColorAlpha(color, a * tFade) or Color(255, 255, 255, a * tFade)
    surface.SetDrawColor(color)
    self:DrawCircle(pnlW / 2, pnlH / 2, size, size, 32)
end

function MaterialLib.VGUI:AddRipple(pnl)
    pnl._matRippleTime = 1
    pnl._matRippleX = 0
    pnl._matRippleY = 0
    pnl._matRippleSpeed = 1

    function pnl:TriggerRipple(x, y, speed)
        if x < 0 or y < 0 or x >= self:GetWide() or y >= self:GetTall() then return end

        self._matRippleTime = 0
        self._matRippleX = x
        self._matRippleY = y

        if speed ~= nil then self._matRippleSpeed = speed end
    end

    function pnl:PaintRipple(w, h, color, noClip)
        if self._matRippleTime < 1 then
            self._matRippleTime = math_Clamp(self._matRippleTime + FrameTime() * 2 * self._matRippleSpeed, 0, 1)
        end

        local x = self._matRippleX
        local y = self._matRippleY
        local size = MaterialLib.Surface:CalculateRippleSize(x, y, w, h)

        MaterialLib.Surface:DrawRippleBack(w, h, self._matRippleTime, color)

        if noClip then
            MaterialLib.Surface:DrawRippleNoClip(x, y, w, h, self._matRippleTime, color)
        else
            MaterialLib.Surface:DrawRipple(x, y, size, self._matRippleTime, color)
        end
    end
end